import {MessageBox} from 'element-ui'
import { nodeEnv } from '@/utils'
var imgHostUrl = nodeEnv() ? 'https://images.imags-google.com' : 'http://testimageupload.img-google.com'
// let chatImgHostUrl = nodeEnv ? 'http://47.91.218.62:23005/' : 'http://47.91.218.62:23005/'
export function filNum (v) {
  if (v && v.length > 6) {
    return v.slice(0, 2) + '***' + v.slice(v.length - 2)
  }
  return v
}
export function imgHost (val) {
  return imgHostUrl + val
}
export function chatImgHost (val) {
  return location.origin + '/' + val
}
export function imgCommon (val) {
  return imgHostUrl + '/system/common/im' + val
}

export function imgLive (val) {
  return imgHostUrl + '/system/live/' + val
}
export function imgAvator (val) {
  return imgHostUrl + '/system/common/headimg/' + val // http://47.52.109.168:8080
}
export function stringToArray (stringObj) {
  stringObj = stringObj.replace(/\[([\w, ]*)\]/, '$1')
  if (stringObj.indexOf('[') === 0) {
    stringObj = stringObj.substring(1, stringObj.length - 1)
  }
  let arr = stringObj.split(',')
  let newArray = []
  for (let i = 0; i < arr.length; i++) {
    let arrOne = arr[i]
    newArray.push(arrOne)
  }
  return newArray
}
export function formatMoney (s, n) {
  n = n > 0 && n <= 20 ? n : 2
  s = parseFloat((s + '').replace(/[^\d\\.-]/g, '')).toFixed(n + 2) + ''
  s = s.substring(0, s.length - 2)
  let l = s.split('.')[0].split('').reverse()
  let r = s.split('.')[1]
  let t = ''
  for (let i = 0; i < l.length; i++) {
    t += l[i] + ((i + 1) % 3 === 0 && (i + 1) !== l.length ? '' : '')
  }
  return t.split('').reverse().join('') + '.' + r
}
export function formatPercentage (s) {
  if (s === undefined || s === '') {
    return '数据格式错误'
  }
  let number = formatMoney(s * 100, 2)
  return number + '%'
}
export function formatTime (s) {
  if (s === undefined || s === '') {
    return '数据格式错误'
  }
  let timeData = ''
  let hour = Math.floor(Number(s) / 3600)
  let min = Math.floor(Number(s) / 60) % 60
  let sec = Number(s) % 60
  let day = parseInt(hour / 24)
  if (day > 0) {
    hour = hour - 24 * day
    timeData += day + '天' + hour + '小时'
  }
  if (hour > 0) {
    timeData += hour + '小时'
  }
  if (min > 0) {
    timeData += min + '分'
  }
  timeData += sec + '秒'
  return timeData
}
export function formatDateTime (str, format) {
  if (!str) {
    return ''
  }
  let time = new Date(Date.parse(str.replace(/-/g, '/')))
  let date = {
    'M+': time.getMonth() + 1,
    'd+': time.getDate(),
    'h+': time.getHours(),
    'm+': time.getMinutes(),
    's+': time.getSeconds(),
    'q+': Math.floor((time.getMonth() + 3) / 3),
    'S+': time.getMilliseconds()
  }
  if (/(y+)/i.test(format)) {
    format = format.replace(RegExp.$1, (time.getFullYear() + '').substr(4 - RegExp.$1.length))
  }
  for (var k in date) {
    if (new RegExp('(' + k + ')').test(format)) {
      format = format.replace(RegExp.$1, RegExp.$1.length === 1 ? date[k] : ('00' + date[k]).substr(('' + date[k]).length))
    }
  }
  return format
}
export function openContentForm (title, v) {
  MessageBox.confirm(v, title, {
    confirmButtonText: '确定',
    type: 'info',
    closeOnClickModal: false,
    closeOnPressEscape: false,
    showClose: false,
    showCancelButton: false
  })
}

export function textHtml (v, l) {
  if (v === undefined || v === null) {
    return ''
  }
  if (v.length < Number(l)) {
    return v
  }
  return `${v.substr(0, l)}<i class="color-blue">...</i>`
}
export function textNumber (v) {
  if (v === null || v === undefined) {
    return 0
  }
  let text = v.length
  return text
}
export function formatMoneyTwo (s) {
  s = parseFloat((s + '').replace(/[^\d\\.-]/g, '')).toFixed(2 + 2) + ''
  let tempResult = s.substring(0, s.length - 5)
  s = s.substring(0, s.length - 2)
  if (parseFloat(tempResult) !== parseFloat(s)) {
    let l = s.split('.')[0].split('').reverse()
    let r = s.split('.')[1]
    let t = ''
    for (let i = 0; i < l.length; i++) {
      t += l[i] + ((i + 1) % 3 === 0 && (i + 1) !== l.length ? '' : '')
    }
    return t.split('').reverse().join('') + '.' + r
  }
  return tempResult
}
export function getSummaries (summaryColumns, data, type) {
  let summaryData = []
  for (let column in summaryColumns) {
    const values = data.map(item => Number(item[column]))
    if (!values.every(value => isNaN(value))) {
      summaryData[column] = values.reduce((prev, curr) => {
        const value = Number(curr)
        if (!isNaN(value)) {
          return prev + curr
        } else {
          return prev
        }
      }, 0)
      if (summaryColumns[column] === '1') {
        summaryData[column] = formatMoney(summaryData[column])
      }
    } else {
      summaryData[column] = 0
    }
  }
  return summaryData
}
// 其它银行名称过滤
export function setBankName (val) {
  if (!val) return ''
  let obj = val.indexOf('其它银行') > -1 ? '其它银行' : val
  return obj
}
