function stopDefault (e) {
  if (e && e.preventDefault) {
    e.preventDefault()
  } else {
    window.event.returnValue = false
  }
  return false
}

const ctx = '@@ScrollLock'

export default {
  bind (el, binding, vnode, oldVnode) {
    var eventType = 'mousewheel'
    if (document.mozHidden !== undefined) {
      eventType = 'DOMMouseScroll'
    }
    var scroll = (event) => {
      var scrollTop = el.scrollTop
      var scrollHeight = el.scrollHeight
      var height = el.clientHeight
      var delta = (event.wheelDelta) ? event.wheelDelta : -(event.detail || 0)
      if ((delta > 0 && scrollTop <= 10) || (delta < 0 && scrollHeight - height - scrollTop <= 10)) {
        // IE浏览器下滚动会跨越边界直接影响父级滚动，因此，临界时候手动边界滚动定位
        el.scrollTop = delta > 0 ? 0 : scrollHeight
        // 向上滚 || 向下滚
        stopDefault(event)
        // 避免事件触发太快，时间间隔200毫秒
        if (!el[ctx].last || new Date().getTime() - el[ctx].last > 200) {
          vnode.context.$emit(delta > 0 ? 'scrollToTop' : 'scrollToButtom')
          el[ctx].last = new Date().getTime()
        }
      }
    }
    el[ctx] = {
      eventType,
      scroll
    }
    el.addEventListener(eventType, scroll, true)
  },
  unbind (el) {
    if (el && el[ctx]) {
      el.removeEventListener('scroll', el[ctx].scroll)
    }
  }
}
