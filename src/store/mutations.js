export default {
  SET_SIDE_NAV: (state, sideNav) => {
    state.sideNav = sideNav
  },
  SET_MEDIA_SCREEN: (state, screenObject) => {
    state.media = screenObject
  },
  SET_NAV_STATUES: (state, status) => {
    state.sideNavStatus = status
  },
  SET_PC_NAV_STATUES: (state, status) => {
    state.pcSideNavStatus = status
  },
  SET_APP_BAR_STATUES: (state, status) => {
    state.appNavBarStatus = status
  },
  SET_UNREADCOUNT: (state, unreadCount) => {
    state.unreadCount = unreadCount
  },
  SET_MESSAGE_UNREADCOUNT: (state, messageUnreadCount) => {
    state.messageUnreadCount = messageUnreadCount
  },
  SET_SITE_INFO: (state, res) => {
    state.siteInfo = res
  }
}
