export default {
  /**
   * 重新请求接口的数据
   * */
  setFetchSideNav ({ commit }, sideNav) {
    commit('SET_SIDE_NAV', sideNav)
  },
  /**
   * 请求localStotage的数据
   * */
  setSideNav ({ commit }) {
    commit('SET_SIDE_NAV', JSON.parse(localStorage.getItem('sideNav')))
  },
  /**
   * 判断屏幕宽度
   * */
  setMediaScreen ({ commit }, screenObject) {
    commit('SET_MEDIA_SCREEN', screenObject)
  },
  /**
   * 小屏幕一级导航状态
   * */
  setAppBarStatus ({ commit }, status) {
    commit('SET_APP_BAR_STATUES', status)
  },
  /**
   * 小屏幕二级导航状态
   * */
  setNavStatus ({ commit }, status) {
    commit('SET_NAV_STATUES', status)
  },
  setPcNavStatus ({ commit }, status) {
    localStorage.setItem('pc-close', JSON.stringify(status))
    commit('SET_PC_NAV_STATUES', status)
  },
  /**
   * 小屏幕二级导航状态
   * */
  setSiteInfo ({ commit }, status) {
    commit('SET_SITE_INFO', status)
  },
  /**
   * 平台消息未读总数量
   * */
  setUnreadCount ({ commit }, messageUnreadCount) {
    commit('SET_UNREADCOUNT', messageUnreadCount)
  },
  /**
   * 平台消息未读数量（分类）
   * */
  setMessageUnreadCount ({ commit }, messageUnreadCount) {
    commit('SET_MESSAGE_UNREADCOUNT', messageUnreadCount)
  },
  /**
   * 当前是否平台
   * */
  setIsPlatform ({ commit }, isPlatform) {
    commit('SET_IS_PLATFORM', isPlatform)
  }
}
