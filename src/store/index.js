import Vue from 'vue'
import Vuex from 'vuex'
import actions from './actions'
import mutations from './mutations'

import { user } from './modules/user'
import { helper } from './modules/helper'
import { nodeEnv } from '@/utils'
Vue.use(Vuex)

let imgHostUrl = nodeEnv() ? 'https://images.imags-google.com' : 'http://testimageupload.img-google.com'
let pcSideNavClose = false
if (document.documentElement.clientWidth > 1023) {
  pcSideNavClose = localStorage.getItem('pc-close') && JSON.parse(localStorage.getItem('pc-close'))
} else {
  pcSideNavClose = false
}
export default new Vuex.Store({
  state: {
    sideNav: null || localStorage,
    sitNavTimestamp: '',
    siteInfo: null,
    currentUser: '',
    IsPlatform: '',
    buttonState: false,
    appNavBarStatus: false,
    sideNavStatus: false,
    pcSideNavStatus: pcSideNavClose, // 导航最小化
    imgHost: imgHostUrl, // http://47.96.68.87
    unreadCount: 0,
    messageUnreadCount: {1: 0, 2: 0, 3: 0, 4: 0},
    permissionData: {},
    isToken: 0,
    resetList: [],
    media: {
      screen: 'large',
      screenWidth: 1920
    }
  },
  actions,
  mutations,
  modules: {
    mutations,
    user,
    helper
  }
})
