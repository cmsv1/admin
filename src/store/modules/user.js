import * as types from '../types'

export const user = {
  state: {
    // 用户登录状态
    loginStatus: 0,
    isFlatfrom: '',
    // 用户登录信息
    userInfo: ''

  },
  getters: {
    loginStatus: state => state.userInfo,
    userInfo: state => state.userInfo
  },
  actions: {
    /**
     * 用户登录
     */
    setUserInfo ({ commit }, res) {
      let data = JSON.stringify(res)
      sessionStorage.setItem('userInfo', data)
      commit(types.SET_USER_INFO, res.username)
      commit(types.SET_LOGIN_STATUS, res.loginStatus)
    },

    /**
     * 退出登录
     */
    setSignOut ({ commit }) {
      sessionStorage.removeItem('userInfo')
      localStorage.setItem('isLogin', 0)
      commit(types.SET_USER_INFO, {})
      commit(types.SET_LOGIN_STATUS, 0)
    },
    /**
     * 刷新页面更新,初始化用户信息
     * */
    setUserState ({ commit }, b) {
      commit(types.SET_USER_INFO, b.username)
      commit(types.SET_LOGIN_STATUS, b.loginStatus)
    }
  },
  mutations: {
    [types.SET_USER_INFO] (state, res) {
      state.userInfo = res
    },

    [types.SET_LOGIN_STATUS] (state, status) {
      state.loginStatus = status
    }
  }
}
