import * as types from '../types'

export const helper = {
  state: {
    // 右侧查看滑块
    rightSlideShow: false,
    lotteryList: []
  },
  getters: {
    rightSlideShow: state => state.rightSlideShow
  },
  actions: {
    /**
     * 右侧滑块
     */
    setSlideShow ({ commit }, res) {
      commit(types.SET_SLIDE_STATUS, res)
    }
  },
  mutations: {
    [types.SET_SLIDE_STATUS] (state, res) {
      state.rightSlideShow = res
    }
  }
}
