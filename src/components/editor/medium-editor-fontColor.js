var currFontColor = null
window.ExtensionFontColor = window.MediumEditor.extensions.anchor.extend({
    name: 'fontColor',
    action: 'createFontColor',
    aria: 'fontColor',
    tagNames: ['fontColor'],
    contentDefault: '<b>Aa</b>',
    contentFA: '<i class="fa fa-font" style="color:#ff2323;"></i>',

    doFormSave: function (i) {
        // Restore Medium Editor's selection before pasting HTML
        this.base.restoreSelection();

        // Paste newly created table.
        document.execCommand('foreColor',false, currFontColor)
        // this.base.pasteHTML(resultHtml.innerHTML);

        // Update toolbar -> hide this form
        this.base.checkSelection();
    },

    // Called when the button the toolbar is clicked
    // Overrides DefaultButton.handleClick
    handleClick: function (evt) {
        evt.preventDefault();
        evt.stopPropagation();

        let currentTarget = this.base
        let dataActive = currentTarget.isColorActive

        if (!dataActive) {
            this.showForm()
           currentTarget.isColorActive = true
           currentTarget.isBgColorActive = false
           currentTarget.isFontActive = false
            let fontSize = document.getElementById(`medium-editor-toolbar-form-fontSize-${currentTarget.id}`)
            if (fontSize) {
              fontSize.style.display = 'none'
            }
            let fontBgColor = document.getElementById(`medium-editor-toolbar-form-fontBgColor-${currentTarget.id}`)
            if (fontBgColor) {
              fontBgColor.style.display = 'none'
            }
        } else {
            this.hideForm()
           currentTarget.isColorActive = false
        }

        return false;
    },

    hideForm: function () {
        this.getForm().style.display = 'none';
    },

    showForm: function () {
        this.base.saveSelection();
        // this.hideToolbarDefaultActions();
        let obj = this.getForm()
        obj.style.left = this.button.offsetLeft + 'px'
        obj.style.display = 'block';
        this.setToolbarPosition();
    },

    createForm: function () {
        var doc = this.base.options.ownerDocument,
            form = doc.createElement('div'),
            save = doc.createElement('ul')

        form.className = 'medium-editor-toolbar-fontColor';
        form.id = 'medium-editor-toolbar-form-fontColor-' + this.base.id;

        // Handle clicks on the form itself
        this.base.on(form, 'click', this.handleFormClick.bind(this));

        // Add save buton
        save.className = 'medium-editor-toolbar-fontColor1 clearfix';
        let str =
            `<li data-font-color="#ff2323"><i data-font-color="#ff2323" class="fa fa-font" style="color:#ff2323;"></i></li>
            <li data-font-color="#ffa659"><i data-font-color="#ffa659" class="fa fa-font" style="color:#ffa659;"></i></li>
            <li data-font-color="#3366cc"><i data-font-color="#3366cc" class="fa fa-font" style="color:#3366cc;"></i></li>
            <li data-font-color="#1abc65"><i data-font-color="#1abc65" class="fa fa-font" style="color:#1abc65;"></i></li>
            <li data-font-color="#666"><i data-font-color="#666" class="fa fa-font" style="color:#666666;"></i></li>
            <li data-font-color="#ffffff" title="白色"><i data-font-color="#fff" class="fa fa-font" style="color:#fff;"></i></li>`
        save.innerHTML = str
        form.appendChild(save);

        // Handle save button clicks (capture)
        let btn = save.children
        for (let i = 0; i < btn.length; i++) {
          this.base.on(btn[i], 'click', this.handleSaveClick.bind(this), true);
          btn[i].addEventListener('mousedown', function (e) {
            currFontColor = e.target.getAttribute('data-font-color')
          })
        }
        return form;
    }
})
