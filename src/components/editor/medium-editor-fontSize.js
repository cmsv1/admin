var currFontSize = null
window.ExtensionFontSize = window.MediumEditor.extensions.anchor.extend({
    name: 'fontSize',
    action: 'createFontSize',
    aria: 'fontSize',
    tagNames: ['fontSize'],
    contentDefault: '<b>Aa</b>',
    contentFA: '<i class="fa">Aa</i>',

    doFormSave: function (i) {
        // Restore Medium Editor's selection before pasting HTML
        this.base.restoreSelection();

        // Paste newly created table.
        document.execCommand('FontSize',false, currFontSize)
        // this.base.pasteHTML(resultHtml.innerHTML);

        // Update toolbar -> hide this form
        this.base.checkSelection();
    },

    // Called when the button the toolbar is clicked
    // Overrides DefaultButton.handleClick
    handleClick: function (evt) {
        evt.preventDefault();
        evt.stopPropagation();

        let currentTarget = this.base
        let dataActive = currentTarget.isFontActive
        if (!dataActive) {
            this.showForm()
            currentTarget.isFontActive = true
            currentTarget.isBgColorActive = false
            currentTarget.isColorActive = false
            let fontColor = document.getElementById(`medium-editor-toolbar-form-fontColor-${currentTarget.id}`)
            if (fontColor) {
              fontColor.style.display = 'none'
            }
            let fontBgColor = document.getElementById(`medium-editor-toolbar-form-fontBgColor-${currentTarget.id}`)
            if (fontBgColor) {
              fontBgColor.style.display = 'none'
            }
        } else {
            this.hideForm()
           currentTarget.isFontActive = false
        }

        return false;
    },

    hideForm: function () {
        this.getForm().style.display = 'none';
    },

    showForm: function () {
        this.base.saveSelection();
        // this.hideToolbarDefaultActions();
        let obj = this.getForm()
        obj.style.left = this.button.offsetLeft + 'px'
        obj.style.display = 'block';
        this.setToolbarPosition();
    },

    createForm: function () {
        var doc = this.base.options.ownerDocument,
            form = doc.createElement('div'),
            save = doc.createElement('ul')

        form.className = 'medium-editor-toolbar-fontSize';
        form.id = 'medium-editor-toolbar-form-fontSize-' + this.base.id;

        // Handle clicks on the form itself
        this.base.on(form, 'click', this.handleFormClick.bind(this));

        // Add save buton
        save.className = 'medium-editor-toolbar-fontSize clearfix';
        let str =
            `<li data-font-size="1">12</li>
            <li data-font-size="2">13</li>
            <li data-font-size="3">16</li>
            <li data-font-size="4">18</li>
            <li data-font-size="5">24</li>
            <li data-font-size="6">32</li>
            <li data-font-size="7">48</li>`
        save.innerHTML = str
        form.appendChild(save);

        // Handle save button clicks (capture)
        let btn = save.children
        for (let i = 0; i < btn.length; i++) {
          this.base.on(btn[i], 'click', this.handleSaveClick.bind(this), true);
          btn[i].addEventListener('mousedown', function (e) {
            currFontSize = e.target.getAttribute('data-font-size')
          })
        }
        return form;
    }
})
