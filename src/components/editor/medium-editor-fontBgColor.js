var currFontBgColor = null
window.ExtensionFontBgColor = window.MediumEditor.extensions.anchor.extend({
    name: 'fontBgColor',
    action: 'createFontBgColor',
    aria: 'fontBgColor',
    tagNames: ['fontBgColor'],
    contentDefault: '<b>Aa</b>',
    contentFA: '<i class="fa fa-font" style="background:#ff2323;color: #fff;"></i>',

    doFormSave: function (i) {
        // Restore Medium Editor's selection before pasting HTML
        this.base.restoreSelection();

        // Paste newly created table.
        document.execCommand('foreColor',false, '#ffffff')
        document.execCommand('BackColor', false, currFontBgColor)
        // this.base.pasteHTML(resultHtml.innerHTML);

        // Update toolbar -> hide this form
        this.base.checkSelection();
    },

    // Called when the button the toolbar is clicked
    // Overrides DefaultButton.handleClick
    handleClick: function (evt) {
        evt.preventDefault();
        evt.stopPropagation();

        let currentTarget = this.base
        let dataActive = currentTarget.isBgColorActive

        if (!dataActive) {
            this.showForm()
           currentTarget.isBgColorActive = true
           currentTarget.isFontActive = false
           currentTarget.isColorActive = false
            let fontSize = document.getElementById(`medium-editor-toolbar-form-fontSize-${currentTarget.id}`)
            if (fontSize) {
              fontSize.style.display = 'none'
            }
            let fontColor = document.getElementById(`medium-editor-toolbar-form-fontColor-${currentTarget.id}`)
            if (fontColor) {
              fontColor.style.display = 'none'
            }
        } else {
            this.hideForm()
           currentTarget.isBgColorActive = false
        }

        return false;
    },

    hideForm: function () {
        this.getForm().style.display = 'none';
    },

    showForm: function () {
        this.base.saveSelection();
        // this.hideToolbarDefaultActions();
        let obj = this.getForm()
        obj.style.left = this.button.offsetLeft + 'px'
        obj.style.display = 'block';
        this.setToolbarPosition();
    },

    createForm: function () {
        var doc = this.base.options.ownerDocument,
            form = doc.createElement('div'),
            save = doc.createElement('ul')

        form.className = 'medium-editor-toolbar-fontBgColor';
        form.id = 'medium-editor-toolbar-form-fontBgColor-' + this.base.id;

        // Handle clicks on the form itself
        this.base.on(form, 'click', this.handleFormClick.bind(this));

        // Add save buton
        save.className = 'medium-editor-toolbar-fontbgColor clearfix';
        let str =
            `<li data-font-bg-color="#ff2323"><i data-font-bg-color="#ff2323" class="fa fa-font" style="background:#ff2323;color: #fff;"></i></li>
            <li data-font-bg-color="#ffa659"><i data-font-bg-color="#ffa659" class="fa fa-font" style="background:#ffa659;color: #fff;"></i></li>
            <li data-font-bg-color="#3366cc"><i data-font-bg-color="#3366cc" class="fa fa-font" style="background:#3366cc;color: #fff;"></i></li>
            <li data-font-bg-color="#1abc65"><i data-font-bg-color="#1abc65" class="fa fa-font" style="background:#1abc65;color: #fff;"></i></li>
            <li data-font-bg-color="#666"><i data-font-bg-color="#666" class="fa fa-font" style="background:#666666;color: #fff;"></i></li>`
        save.innerHTML = str
        form.appendChild(save);

        // Handle save button clicks (capture)
        let btn = save.children
        for (let i = 0; i < btn.length; i++) {
          this.base.on(btn[i], 'click', this.handleSaveClick.bind(this), true);
          btn[i].addEventListener('mousedown', function (e) {
            currFontBgColor = e.target.getAttribute('data-font-bg-color')
          })
        }
        return form;
    }
})
