import Vue from 'vue'
import Router from 'vue-router'
import routes from './routes'
// import {MessageBox} from 'element-ui'

Vue.use(Router)

const router = new Router({
  base: process.env.BASE_PATH, // 基础路径用于项目是非根路径下
  routes,
  mode: 'history',
  linkActiveClass: 'active',
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})
router.beforeEach((to, from, next) => {
  if (to.path === '/') {
    let siteInfo = JSON.parse(localStorage.getItem('siteInfo'))
    if (siteInfo && siteInfo.isPlatform === 1) {
      next({path: '/useInfo'})
      return
    }
    if (siteInfo && siteInfo.isPlatform !== 1) {
      next({path: '/management/managementAnnouncement'})
      return
    }
  }
  if (to.meta.title) {
    document.title = to.meta.title
  }
  // if (!window.navigator.onLine) {
  //   return MessageBox.confirm('当前网络不可用，请检查网络后重试', '提示', {
  //     confirmButtonText: '确定',
  //     type: 'warning',
  //     center: true
  //   })
  // }
  if (!localStorage.getItem('WebsiteClosed')) {
    if (!localStorage.getItem('maintainData')) {
      let isLogin = parseInt(localStorage.getItem('isLogin'))
      if (to.name !== 'login' && !isLogin) next({path: '/login'})
      else next()
    } else next()
  } else next()
})
export default router
