// import IndexAdmin from '@/views/index/Index'

const router = [
  {
    name: '500',
    path: '/500',
    component: () => import('@/views/error/500'),
    meta: {
      title: '500错误-后台管理系统'
    }
  },
  {
    name: 'maintain',
    path: '/maintain',
    component: () => import('@/views/Maintain'),
    meta: {
      title: '系统维护中...'
    }
  },
  {
    name: 'websiteClosed',
    path: '/websiteClosed',
    component: () => import('@/views/WebsiteClosed'),
    meta: {
      title: '网站已关闭'
    }
  },
  {
    path: '/login',
    component: () => import(/* webpackChunkName: "login" */'../views/login/Index'),
    children: [{
      name: 'login',
      path: '/',
      component: () => import(/* webpackChunkName: "login" */'../views/login/Login'),
      meta: {
        title: '登录-后台管理系统'
      }
    }],
    meta: {
      title: '后台-登录'
    }
  },
  {
    name: 'kitchen',
    path: '/kitchen',
    component: () => import('../Kitchen'),
    meta: {
      title: '头像设置-后台管理系统'
    }
  },
  {
    name: 'kitchenMt',
    path: '/kitchenMt',
    component: () => import('../KitchenMt'),
    meta: {
      title: '手机版模版'
    }
  },
  {
    path: '',
    component: resolve => import('@/views/Index'),
    children: [
      {
        name: '404',
        path: '/404',
        component: () => import('@/views/error/404'),
        meta: {
          title: '404错误-后台管理系统'
        }
      },
      {
        path: 'platform',
        name: 'platform',
        component: () => import('../views/index/SidebarIndex'),
        children: [
          {
            path: 'platformSwitchTenant',
            name: 'platformSwitchTenant',
            component: () => import('../views/platform/PlatformSwitchTenant'),
            meta: {
              title: '站点切换-后台管理系统'
            }
          },
          {
            path: 'tenantIP',
            name: 'tenantIP',
            component: () => import('../views/platform/TenantIP'),
            meta: {
              title: 'IP管理-后台管理系统'
            }
          },
          {
            path: 'platformTenantAnnouncement',
            name: 'platformTenantAnnouncement',
            component: () => import('../views/platform/PlatformAnnouncement'),
            meta: {
              title: '平台消息-后台管理系统'
            }
          },
          {
            path: 'platformTenantDomain',
            name: 'platformTenantDomain',
            component: () => import('../views/platform/PlatformTenantDomain'),
            meta: {
              title: '域名管理-后台管理系统'
            }
          },
          {
            path: 'platformTenantDetail',
            name: 'platformTenantDetail',
            component: () => import('../views/platform/PlatformTenantDetail'),
            meta: {
              title: '站长管理-后台管理系统'
            }
          },
          {
            path: 'platformTenantFeedback',
            name: 'platformTenantFeedback',
            component: () => import('../views/platform/PlatformTenantFeedback'),
            meta: {
              title: '站长反馈-后台管理系统'
            }
          },
          {
            path: 'ImgFormofForm',
            name: 'ImgFormofForm',
            component: () => import('../views/platform/ImgFormofForm'),
            meta: {
              title: '图片规格-后台管理系统'
            }
          },
          {
            path: 'platformMaintainForm',
            name: 'platformMaintainForm',
            component: () => import('../views/platform/PlatformMaintainForm'),
            meta: {
              title: '维护设置-后台管理系统'
            }
          },
          {
            path: 'platformTenantManualAppeal',
            name: 'platformTenantManualAppeal',
            component: () => import('../views/platform/PlatformTenantManualAppeal'),
            meta: {
              title: '人工申诉-后台管理系统'
            }
          },
          // {
          //   path: 'platformSettingCancel',
          //   name: 'platformSettingCancel',
          //   component: () => import(/* webpackChunkName: "platform-sysSetting" */'../views/platform/PlatformSettingCancel'),
          //   meta: {
          //     title: '系统撤单-后台管理系统'
          //   }
          // },
          {
            path: 'platformSettingActivity',
            name: 'platformSettingActivity',
            component: () => import('../views/platform/PlatformSettingActivity'),
            meta: {
              title: '系统活动-后台管理系统'
            }
          },
          {
            path: 'platformSettingBroadcast',
            name: 'platformSettingBroadcast',
            component: () => import('../views/platform/PlatformSettingBroadcast'),
            meta: {
              title: '系统广播-后台管理系统'
            }
          },
          {
            path: 'platformSettingApp',
            name: 'platformSettingApp',
            component: () => import('../views/platform/PlatformSettingApp'),
            meta: {
              title: '系统APP-后台管理系统'
            }
          },
          {
            path: 'platformSettingLottery',
            name: 'platformSettingLottery',
            component: () => import('../views/platform/PlatformSettingLottery'),
            meta: {
              title: '彩种管理-后台管理系统'
            }
          },
          {
            path: 'platformSettingGame',
            name: 'platformSettingGame',
            component: () => import('../views/platform/PlatformSettingGame'),
            meta: {
              title: '棋牌管理-后台管理系统'
            }
          },
          {
            path: 'platformSettingBanner',
            name: 'platformSettingBanner',
            component: () => import('../views/platform/PlatformSettingBanner'),
            meta: {
              title: '大厅轮播-后台管理系统'
            }
          },
          {
            path: 'platformSettingNickname',
            name: 'platformSettingNickname',
            component: () => import('../views/platform/PlatformSettingNickname'),
            meta: {
              title: '昵称管理-后台管理系统'
            }
          },
          {
            path: 'platformSettingNicknameFilter',
            name: 'platformSettingNicknameFilter',
            component: () => import('../views/platform/PlatformSettingNicknameFilter'),
            meta: {
              title: '昵称管理-后台管理系统'
            }
          },
          {
            path: 'platformSettingThirdParty',
            name: 'platformSettingThirdParty',
            component: () => import('../views/platform/PlatformSettingThirdParty'),
            meta: {
              title: '第三方管理-后台管理系统'
            }
          },
          {
            path: 'platformSettingPaymentAssistance',
            name: 'platformSettingPaymentAssistance',
            component: () => import('../views/platform/PlatformSettingPaymentAssistan'),
            meta: {
              title: '充值说明-后台管理系统'
            }
          },
          {
            path: 'platformSettingProfit',
            name: 'platformSettingProfit',
            component: () => import('../views/platform/PlatformSettingProfit'),
            meta: {
              title: '盈率调节-后台管理系统'
            }
          },
          {
            path: 'platformSettingProfitGame',
            name: 'platformSettingProfitGame',
            component: () => import('../views/platform/PlatformSettingProfitGame'),
            meta: {
              title: '棋牌盈率调节-后台管理系统'
            }
          },
          {
            path: 'platformSettingProfitRecord',
            name: 'platformSettingProfitRecord',
            component: () => import('../views/platform/PlatformSettingProfitRecord'),
            meta: {
              title: '盈率调节记录-后台管理系统'
            }
          },
          {
            path: 'platformSettingProfitRecordGame',
            name: 'platformSettingProfitRecordGame',
            component: () => import('../views/platform/PlatformSettingProfitRecordGame'),
            meta: {
              title: '盈率调节记录-后台管理系统'
            }
          },
          {
            path: 'platformLiveTip',
            name: 'platformLiveTip',
            component: () => import('../views/platform/PlatformLiveTip'),
            meta: {
              title: '打赏设置-后台管理系统'
            }
          },
          {
            path: 'platformLiveBarrage',
            name: 'platformLiveBarrage',
            component: () => import('../views/platform/PlatformLiveBarrage'),
            meta: {
              title: '弹幕设置-后台管理系统'
            }
          },
          {
            path: 'platformLivePredefined',
            name: 'platformLivePredefined',
            component: () => import('../views/platform/PlatformLivePredefined'),
            meta: {
              title: '系统发言设置-后台管理系统'
            }
          },
          {
            path: 'platformLiveFreely',
            name: 'platformLiveFreely',
            component: () => import('../views/platform/PlatformLiveFreely'),
            meta: {
              title: '自由发言设置-后台管理系统'
            }
          },
          {
            path: 'platformLiveTenant',
            name: 'platformLiveTenant',
            component: () => import('../views/platform/PlatformLiveTenant'),
            meta: {
              title: '站点配置-后台管理系统'
            }
          },
          {
            path: 'platformNetworkHttps',
            name: 'platformNetworkHttps',
            component: () => import('../views/platform/PlatformNetworkHttps'),
            meta: {
              title: 'HTTPS套餐管理-后台管理系统'
            }
          },
          {
            path: 'platformNetworkDomain',
            name: 'platformNetworkDomain',
            component: () => import('../views/platform/PlatformNetworkDomain'),
            meta: {
              title: '域名管理-后台管理系统'
            }
          },
          {
            path: 'platformNetworkServer',
            name: 'platformNetworkServer',
            component: () => import('../views/platform/PlatformNetworkServer'),
            meta: {
              title: '服务器管理-后台管理系统'
            }
          },
          {
            path: 'platformNetworkIp',
            name: 'platformNetworkIp',
            component: () => import('../views/platform/PlatformNetworkIp'),
            meta: {
              title: 'ip管理-后台管理系统'
            }
          },
          {
            path: 'platformNetworkDispatch',
            name: 'platformNetworkDispatch',
            component: () => import('../views/platform/PlatformNetworkDispatch'),
            meta: {
              title: '网络运维-后台管理系统'
            }
          },
          {
            path: 'platformNetworkDispatchBackup',
            name: 'platformNetworkDispatchBackup',
            component: () => import('../views/platform/PlatformNetworkDispatchBackup'),
            meta: {
              title: '网路运维备用-后台管理系统'
            }
          },
          {
            path: 'platformNetworkGatewayLimit',
            name: 'platformNetworkGatewayLimit',
            component: () => import('../views/platform/PlatformNetworkGatewayLimit'),
            meta: {
              title: '网关访问限制-后台管理系统'
            }
          },
          {
            path: 'platformNetworkLog',
            name: 'platformNetworkLog',
            component: () => import('../views/platform/PlatformNetworkLog'),
            meta: {
              title: '运维日志-后台管理系统'
            }
          },
          {
            path: 'platformNetworkSetting',
            name: 'platformNetworkSetting',
            component: () => import('../views/platform/PlatformNetworkSetting'),
            meta: {
              title: '运维设置-后台管理系统'
            }
          },
          {
            path: 'platformReportCompound',
            name: 'platformReportCompound',
            component: () => import('../views/platform/PlatformReportCompound'),
            meta: {
              title: '综合报表-后台管理系统'
            }
          },
          {
            path: 'platformReportTenant',
            name: 'platformReportTenant',
            component: () => import('../views/platform/PlatformReportTenant'),
            meta: {
              title: '站长报表-后台管理系统'
            }
          },
          {
            path: 'platformReportMonthly',
            name: 'platformReportMonthly',
            component: () => import('../views/platform/PlatformReportMonthly'),
            meta: {
              title: '月度报表-后台管理系统'
            }
          },
          {
            path: 'platformReportLottery',
            name: 'platformReportLottery',
            component: () => import('../views/platform/PlatformReportLottery'),
            meta: {
              title: '彩种报表-后台管理系统'
            }
          },
          {
            path: 'platformReportGame',
            name: 'platformReportGame',
            component: () => import('../views/platform/PlatformReportGame'),
            meta: {
              title: '棋牌报表-后台管理系统'
            }
          },
          {
            path: 'platformTechnicalDeleteReport',
            name: 'platformTechnicalDeleteReport',
            component: () => import('../views/platform/PlatformTechnicalDeleteReport'),
            meta: {
              title: '报表清除-后台管理系统'
            }
          },
          {
            path: 'platformNetworkIpAddress',
            name: 'platformNetworkIpAddress',
            component: () => import('../views/platform/PlatformNetworkIpAddress'),
            meta: {
              title: '报表清除-后台管理系统'
            }
          },
          {
            path: 'platformNetworkApp',
            name: 'platformNetworkApp',
            component: () => import('../views/platform/PlatformNetworkApp'),
            meta: {
              title: '报表清除-后台管理系统'
            }
          },
          {
            path: 'platformNetworkLive',
            name: 'platformNetworkLive',
            component: () => import('../views/platform/PlatformNetworkLive'),
            meta: {
              title: '报表清除-后台管理系统'
            }
          },
          {
            path: 'platformTechnicalResetUser',
            name: 'platformTechnicalResetUser',
            component: () => import('../views/platform/PlatformTechnicalResetUser'),
            meta: {
              title: '会员重置-后台管理系统'
            }
          },
          {
            path: 'platformTechnicalCorrectOpenning',
            name: 'platformTechnicalCorrectOpenning',
            component: () => import('../views/platform/PlatformTechnicalCorrectOpenni'),
            meta: {
              title: '开奖纠错-后台管理系统'
            }
          },
          {
            path: 'platformTechnicalMannuallyOpen',
            name: 'platformTechnicalMannuallyOpen',
            component: () => import('../views/platform/PlatformTechnicalMannuallyOpen'),
            meta: {
              title: '手动开奖-后台管理系统'
            }
          },
          {
            path: 'platformTechnicalCancelBetting',
            name: 'platformTechnicalCancelBetting',
            component: () => import('../views/platform/PlatformTechnicalCancelBetting'),
            meta: {
              title: '系统撤单-后台管理系统'
            }
          },
          {
            path: 'platformTechnicalSettlementCheck',
            name: 'platformTechnicalSettlementCheck',
            component: () => import('../views/platform/PlatformTechnicalSettlementCheck'),
            meta: {
              title: '结算检查-后台管理系统'
            }
          },
          {
            path: 'platformTechnicalCancelBettingGame',
            name: 'platformTechnicalCancelBettingGame',
            component: () => import('../views/platform/PlatformTechnicalCancelBettingGame'),
            meta: {
              title: '棋牌系统撤单-后台管理系统'
            }
          },
          {
            path: 'platformAuthorityProle',
            name: 'platformAuthorityProle',
            component: () => import('../views/platform/PlatformAuthorityProle'),
            meta: {
              title: '平台角色-后台管理系统'
            }
          },
          {
            path: 'platformAuthorityTrole',
            name: 'platformAuthorityTrole',
            component: () => import('../views/platform/PlatformAuthorityTrole'),
            meta: {
              title: '站点角色-后台管理系统'
            }
          },
          {
            path: 'platformAuthorityManager',
            name: 'platformAuthorityManager',
            component: () => import('../views/platform/PlatformAuthorityManager'),
            meta: {
              title: '管理员-后台管理系统'
            }
          },
          {
            path: 'platformAuthorityLog',
            name: 'platformAuthorityLog',
            component: () => import('../views/platform/PlatformAuthorityLog'),
            meta: {
              title: '管理日志-后台管理系统'
            }
          },
          {
            path: 'platformSettingRecharge',
            name: 'platformSettingRecharge',
            component: () => import('../views/platform/platformSettingRecharge'),
            meta: {
              title: '充值管理-后台管理系统'
            }
          }
        ]
      },
      {
        path: 'tenant',
        name: 'tenant',
        component: () => import('../views/index/SidebarIndex'),
        children: [
          {
            path: 'tenantWebsiteHotLottery',
            name: 'tenantWebsiteHotLottery',
            component: () => import('../views/site/TenantWebsiteHotLottery'),
            meta: {
              title: '热门彩种-后台管理系统'
            }
          },
          {
            path: 'tenantWebsiteTenantLottery',
            name: 'tenantWebsiteTenantLottery',
            component: () => import('../views/site/TenantWebsiteTenantLottery'),
            meta: {
              title: '站长彩种-后台管理系统'
            }
          },
          {
            path: 'tenantWebsiteGame',
            name: 'tenantWebsiteGame',
            component: () => import('../views/site/TenantWebsiteGame'),
            meta: {
              title: '棋牌管理-后台管理系统'
            }
          },
          {
            path: 'tenantWebsiteLogo',
            name: 'tenantWebsiteLogo',
            component: () => import('../views/site/TenantWebsiteLogo'),
            meta: {
              title: 'Logo管理-后台管理系统'
            }
          },
          {
            path: 'tenantWebsiteBanner',
            name: 'tenantWebsiteBanner',
            component: () => import('../views/site/TenantWebsiteBanner'),
            meta: {
              title: '首页轮播-后台管理系统'
            }
          },
          {
            path: 'tenantWebsiteIntroduction',
            name: 'tenantWebsiteIntroduction',
            component: () => import('../views/site/TenantWebsiteIntroduction'),
            meta: {
              title: '关于我们-后台管理系统'
            }
          },
          {
            path: 'tenantWebsiteAssistance',
            name: 'tenantWebsiteAssistance',
            component: () => import('../views/site/TenantWebsiteAssistance'),
            meta: {
              title: '帮助指南-后台管理系统'
            }
          },
          {
            path: 'tenantWebsiteSupport',
            name: 'tenantWebsiteSupport',
            component: () => import('../views/site/TenantWebsiteSupport'),
            meta: {
              title: '客服管理-后台管理系统'
            }
          },
          {
            path: 'tenantWebsiteFeedback',
            name: 'tenantWebsiteFeedback',
            component: () => import('../views/site/TenantWebsiteFeedback'),
            meta: {
              title: '意见反馈-后台管理系统'
            }
          },
          {
            path: 'tenantWebsiteDomain',
            name: 'tenantWebsiteDomain',
            component: () => import('../views/site/TenantWebsiteDomain'),
            meta: {
              title: '域名管理-后台管理系统'
            }
          },
          {
            path: 'tenantWebsiteHttps',
            name: 'tenantWebsiteHttps',
            component: () => import('../views/site/TenantWebsiteHttps'),
            meta: {
              title: 'HTTPS管理-后台管理系统'
            }
          },
          {
            path: 'tenantWebsiteApp',
            name: 'tenantWebsiteApp',
            component: () => import('../views/site/TenantWebsiteApp'),
            meta: {
              title: 'APP管理-后台管理系统'
            }
          },
          {
            path: 'tenantWebsiteIp',
            name: 'tenantWebsiteIp',
            component: () => import('../views/site/TenantWebsiteIp'),
            meta: {
              title: 'IP管理-后台管理系统'
            }
          },
          {
            path: 'tenantWebsiteRingtone',
            name: 'tenantWebsiteRingtone',
            component: () => import('../views/site/TenantWebsiteRingtone'),
            meta: {
              title: '提示音设置-后台管理系统'
            }
          },
          {
            path: 'tenantPaymentBankTransfer',
            name: 'tenantPaymentBankTransfer',
            component: () => import('../views/site/TenantPaymentBankTransfer'),
            meta: {
              title: '银行转帐-后台管理系统'
            }
          },
          {
            path: 'tenantPaymentOnlineBank',
            name: 'tenantPaymentOnlineBank',
            component: () => import('../views/site/TenantPaymentOnlineBank'),
            meta: {
              title: '网银支付-后台管理系统'
            }
          },
          {
            path: 'tenantPaymentWeChat',
            name: 'tenantPaymentWeChat',
            component: () => import('../views/site/TenantPaymentWeChat'),
            meta: {
              title: '微信支付-后台管理系统'
            }
          },
          {
            path: 'tenantPaymentQuickpass',
            name: 'tenantPaymentQuickpass',
            component: () => import('../views/site/TenantPaymentQuickpass'),
            meta: {
              title: '云闪付-后台管理系统'
            }
          },
          {
            path: 'tenantPaymentJdpay',
            name: 'tenantPaymentJdpay',
            component: () => import('../views/site/TenantPaymentJdpay'),
            meta: {
              title: '京东钱包-后台管理系统'
            }
          },
          {
            path: 'tenantPaymentAlipay',
            name: 'tenantPaymentAlipay',
            component: () => import('../views/site/TenantPaymentAlipay'),
            meta: {
              title: '支付宝-后台管理系统'
            }
          },
          {
            path: 'tenantPaymentQqWallet',
            name: 'tenantPaymentQqWallet',
            component: () => import('../views/site/TenantPaymentQqWallet'),
            meta: {
              title: 'QQ钱包-后台管理系统'
            }
          },
          {
            path: 'tenantPaymentUnionpay',
            name: 'tenantPaymentUnionpay',
            component: () => import('../views/site/TenantPaymentUnionpay'),
            meta: {
              title: '银联支付-后台管理系统'
            }
          },
          {
            path: 'tenantPaymentFourth',
            name: 'tenantPaymentFourth',
            component: () => import('../views/site/TenantPaymentFourth'),
            meta: {
              title: '第四方-后台管理系统'
            }
          },
          {
            path: 'tenantPaymentAmount',
            name: 'tenantPaymentAmount',
            component: () => import('../views/site/TenantPaymentAmount'),
            meta: {
              title: '限额设置-后台管理系统'
            }
          },
          {
            path: 'tenantPaymentSort',
            name: 'tenantPaymentSort',
            component: () => import('../views/site/TenantPaymentSort'),
            meta: {
              title: '充值排序-后台管理系统'
            }
          },
          {
            path: 'tenantPaymentAssistance',
            name: 'tenantPaymentAssistance',
            component: () => import('../views/site/TenantPaymentAssistance'),
            meta: {
              title: '充值说明-后台管理系统'
            }
          },
          {
            path: 'tenantChatRoom',
            name: 'tenantChatRoom',
            component: () => import('../views/site/TenantChatRoom'),
            meta: {
              title: '聊天室管理-房间管理'
            }
          },
          {
            path: 'tenantChatRole',
            name: 'tenantChatRole',
            component: () => import('../views/site/TenantChatRole'),
            meta: {
              title: '聊天室管理-角色管理'
            }
          },
          {
            path: 'tenantChatFilter',
            name: 'tenantChatFilter',
            component: () => import('../views/site/TenantChatFilter'),
            meta: {
              title: '聊天室管理-过滤设置'
            }
          },
          {
            path: 'tenantChatBan',
            name: 'tenantChatBan',
            component: () => import('../views/site/TenantChatBan'),
            meta: {
              title: '聊天室管理-禁言管理'
            }
          },
          {
            path: 'tenantChatOnline',
            name: 'tenantChatOnline',
            component: () => import('../views/site/TennatChatOnline'),
            meta: {
              title: '聊天室管理-在线用户'
            }
          },
          {
            path: 'tenantChatMessage',
            name: 'tenantChatMessage',
            component: () => import('../views/site/TenantChatMessage'),
            meta: {
              title: '聊天室管理-聊天记录'
            }
          },
          {
            path: 'tenantChatBlock',
            name: 'tenantChatBlock',
            component: () => import('../views/site/TenantChatBlock'),
            meta: {
              title: '聊天室管理-屏蔽记录'
            }
          },
          {
            path: 'tenantChatManual',
            name: 'tenantChatManual',
            component: () => import('../views/site/TenantChatManual'),
            meta: {
              title: '聊天室管理-人工审核'
            }
          },
          {
            path: 'tenantPlayPush',
            name: 'tenantPlayPush',
            component: () => import('../views/site/TenantPlayPush'),
            meta: {
              title: '计划管理-推送计划'
            }
          },
          {
            path: 'TenantPlayPushDetail',
            name: 'TenantPlayPushDetail',
            component: () => import('../views/site/TenantPlayPushDetail'),
            meta: {
              title: '推送计划-历史记录'
            }
          },
          {
            path: 'tenantPlayCreate',
            name: 'tenantPlayCreate',
            component: () => import('../views/site/TenantPlayCreate'),
            meta: {
              title: '计划管理-计划方案'
            }
          },
          {
            path: 'tenantAuthorityToken',
            name: 'tenantAuthorityToken',
            component: () => import('../views/site/TenantAuthorityToken'),
            meta: {
              title: '令牌管理-后台管理系统'
            }
          },
          {
            path: 'tenantAuthorityRole',
            name: 'tenantAuthorityRole',
            component: () => import('../views/site/TenantAuthorityRol'),
            meta: {
              title: '角色管理-后台管理系统'
            }
          },
          {
            path: 'tenantAuthorityManager',
            name: 'tenantAuthorityManager',
            component: () => import('../views/site/TenantAuthorityManager'),
            meta: {
              title: '管理员-后台管理系统'
            }
          },
          {
            path: 'tenantAuthorityLog',
            name: 'tenantAuthorityLog',
            component: () => import('../views/site/TenantAuthorityLog'),
            meta: {
              title: '管理日志-后台管理系统'
            }
          }
        ]
      },
      { // 管理首页
        path: 'management',
        name: 'management',
        component: () => import('../views/index/SidebarIndex'),
        children: [
          {
            path: 'managementAnnouncement',
            name: 'managementAnnouncement',
            component: () => import('../views/managementHome/ManagementAnnouncement'),
            meta: {
              title: '平台消息-后台管理系统'
            }
          },
          {
            path: 'managementBroadcast',
            name: 'managementBroadcast',
            component: () => import('../views/managementHome/ManagementBroadcast'),
            meta: {
              title: '平台广播-后台管理系统'
            }
          },
          {
            path: 'managementFeedback',
            name: 'managementFeedback',
            component: () => import('../views/managementHome/ManagementFeedback'),
            meta: {
              title: '意见反馈-后台管理系统'
            }
          }
        ]
      },
      {
        path: 'member',
        name: 'member',
        component: () => import('../views/index/SidebarIndex'),
        children: [
          {
            path: 'memberUsersMessage',
            name: 'memberUsersMessage',
            component: () => import('../views/users/MemberUsersMessage'),
            meta: {
              title: '站内信-后台管理系统'
            }
          },
          {
            path: 'memberUsersAnnouncement',
            name: 'memberUsersAnnouncement',
            component: () => import('../views/users/MemberUsersAnnouncement'),
            meta: {
              title: '网站公告-后台管理系统'
            }
          },
          {
            path: 'memberUsersManage',
            name: 'memberUsersManage',
            component: () => import('../views/users/MemberManage'),
            meta: {
              title: '会员管理-后台管理系统'
            }
          },
          {
            path: 'memberUsersGrade',
            name: 'memberUsersGrade',
            component: () => import('../views/users/MemberUsersGrade'),
            meta: {
              title: '等级管理-后台管理系统'
            }
          },
          {
            path: 'memberUsersOnline',
            name: 'memberUsersOnline',
            component: () => import('../views/users/MemberUsersOnline'),
            meta: {
              title: '在线会员-后台管理系统'
            }
          },
          {
            path: 'memberUsersTest',
            name: 'memberUsersTest',
            component: () => import('../views/users/MemberUsersTest'),
            meta: {
              title: '测试账号-后台管理系统'
            }
          },
          {
            path: 'memberUsersBank',
            name: 'memberUsersBank',
            component: () => import('../views/users/MemberUsersBank'),
            meta: {
              title: '银行管理-后台管理系统'
            }
          },
          {
            path: 'memberAgentRebateForbidden',
            name: 'memberAgentRebateForbidden',
            component: () => import('../views/users/MemberAgentRebateForbidden'),
            meta: {
              title: '返点禁止-后台管理系统'
            }
          },
          {
            path: 'memeberAgentInvitation',
            name: 'memeberAgentInvitation',
            component: () => import('../views/users/MemeberAgentInvitation'),
            meta: {
              title: '邀请码管理-后台管理系统'
            }
          },
          {
            path: 'memberUsersLog',
            name: 'memberUsersLog',
            component: () => import('../views/users/MemberUsersLog'),
            meta: {
              title: '会员日志-后台管理系统'
            }
          },
          {
            path: 'memberUsersLoginRecord',
            name: 'memberUsersLoginRecord',
            component: () => import('../views/users/MemberUsersLoginRecord'),
            meta: {
              title: '登录记录-后台管理系统'
            }
          },
          {
            path: 'memberOthersBlackList',
            name: 'memberOthersBlackList',
            component: () => import('../views/users/MemberOthersBlackList'),
            meta: {
              title: 'IP黑名单-后台管理系统'
            }
          },
          {
            path: 'memberOthersLoginProtect',
            name: 'memberOthersLoginProtect',
            component: () => import('../views/users/MemberOthersLoginProtect'),
            meta: {
              title: '登录保护-后台管理系统'
            }
          },
          {
            path: 'memberOthersAreaBan',
            name: 'memberOthersAreaBan',
            component: () => import('../views/users/MemberOthersAreaBan'),
            meta: {
              title: '区域访问限制'
            }
          }
        ]
      },
      {
        path: 'transaction',
        name: 'transaction',
        component: () => import('../views/index/SidebarIndex'),
        children: [
          {
            path: 'transactionWithdrawOrder',
            name: 'transactionWithdrawOrder',
            component: () => import('../views/trade/TransactionWithdrawOrder'),
            meta: {
              title: '提现订单管理-后台管理系统'
            }
          },
          {
            path: 'transactionRechargeGeneral',
            name: 'transactionRechargeGeneral',
            component: () => import('../views/trade/TransactionRechargeGeneral'),
            meta: {
              title: '一般充值订单-后台管理系统'
            }
          },
          {
            path: 'transactionRechargeFast',
            name: 'transactionRechargeFast',
            component: () => import('../views/trade/TransactionRechargeFast'),
            meta: {
              title: '第三方充值订单-后台管理系统'
            }
          },
          {
            path: 'transactionRechargeFourth',
            name: 'transactionRechargeFourth',
            component: () => import('../views/trade/TransactionRechargeFourth'),
            meta: {
              title: '第四方充值订单-后台管理系统'
            }
          },
          {
            path: 'transactionManualProcess',
            name: 'transactionManualProcess',
            component: () => import('../views/trade/TransactionManualProcess'),
            meta: {
              title: '人工存提-后台管理系统'
            }
          },
          {
            path: 'transactionManualDeposit',
            name: 'transactionManualDeposit',
            component: () => import('../views/trade/TransactionManualDeposit'),
            meta: {
              title: '人工存款记录-后台管理系统'
            }
          },
          {
            path: 'transactionManualWithdraw',
            name: 'transactionManualWithdraw',
            component: () => import('../views/trade/TransactionManualWithdraw'),
            meta: {
              title: '人工提出记录-后台管理系统'
            }
          },
          {
            path: 'transactionBettingRecord',
            name: 'transactionBettingRecord',
            component: () => import('../views/trade/TransactionBettingRecord'),
            meta: {
              title: '投注记录-后台管理系统'
            }
          },
          {
            path: 'transactionBettingChaseRecord',
            name: 'transactionBettingChaseRecord',
            component: () => import('../views/trade/TransactionBettingChaseRecord'),
            meta: {
              title: '追号记录-后台管理系统'
            }
          },
          {
            path: 'transactionGameRecord',
            name: 'transactionGameRecord',
            component: () => import('../views/trade/TransactionGameRecord'),
            meta: {
              title: '棋牌投注记录-后台管理系统'
            }
          },
          {
            path: 'SeekOrderDetail',
            name: 'SeekOrderDetail',
            component: () => import('../views/trade/SeekOrderDetail'),
            meta: {
              title: '订单详情-后台管理系统'
            }
          },
          {
            path: 'SeekOrderInfo',
            name: 'SeekOrderInfo',
            component: () => import('../views/trade/SeekOrderInfo'),
            meta: {
              title: '订单详细-后台管理系统'
            }
          },
          {
            path: 'transactionRecordDetail',
            name: 'transactionRecordDetail',
            component: () => import('../views/trade/TransactionRecordDetail'),
            meta: {
              title: '交易记录-后台管理系统'
            }
          },
          {
            path: 'transactionRecordWithdraw',
            name: 'transactionRecordWithdraw',
            component: () => import('../views/trade/TransactionRecordWithdraw'),
            meta: {
              title: '提现记录-后台管理系统'
            }
          },
          {
            path: 'transactionRecordRecharge',
            name: 'transactionRecordRecharge',
            component: () => import('../views/trade/TransactionRecordRecharge'),
            meta: {
              title: '充值记录-后台管理系统'
            }
          },
          {
            path: 'transactionRecordTransfer',
            name: 'transactionRecordTransfer',
            component: () => import('../views/trade/TransactionRecordTransfer'),
            meta: {
              title: '转帐纪录-后台管理系统'
            }
          },
          {
            path: 'transactionRecordReceive',
            name: 'transactionRecordReceive',
            component: () => import('../views/trade/TransactionRecordReceive'),
            meta: {
              title: '收款纪录-后台管理系统'
            }
          },
          {
            path: 'transactionRecordRedEnvelopeSend',
            name: 'transactionRecordRedEnvelopeSend',
            component: () => import('../views/trade/TransactionRecordRedEnvelopeSend'),
            meta: {
              title: '红包发送纪录-后台管理系统'
            }
          },
          {
            path: 'transactionRecordRedEnvelopeReceive',
            name: 'transactionRecordRedEnvelopeReceive',
            component: () => import('../views/trade/TransactionRecordRedEnvelopeReceive'),
            meta: {
              title: '红包领取纪录-后台管理系统'
            }
          },
          {
            path: 'transactionRecordTip',
            name: 'transactionRecordTip',
            component: () => import('../views/trade/TransactionRecordTip'),
            meta: {
              title: '打赏记录-后台管理系统'
            }
          }
        ]
      },
      {
        path: 'activity',
        name: 'activity',
        component: () => import('../views/index/SidebarIndex'),
        children: [
          {
            path: 'activityList',
            name: 'activityList',
            component: () => import('../views/activity/ActivitiesManage'),
            meta: {
              title: '活动管理-后台管理系统'
            }
          },
          {
            path: 'activityDetail',
            name: 'activityDetail',
            component: () => import('../views/activity/ActivitiesDetail'),
            meta: {
              title: '活动明细-后台管理系统'
            }
          },
          {
            path: 'activityForbidden',
            name: 'activityForbidden',
            component: () => import('../views/activity/ActivityForbidden'),
            meta: {
              title: '活动禁止-后台管理系统'
            }
          }
        ]
      },
      {
        path: 'report',
        name: 'report',
        component: () => import('../views/index/SidebarIndex'),
        children: [
          {
            path: 'reportCompound',
            name: 'reportCompound',
            component: () => import('../views/report/ReportCompound'),
            meta: {
              title: '综合报表-后台管理系统'
            }
          },
          {
            path: 'reportAgent',
            name: 'reportAgent',
            component: () => import('../views/report/ReportAgent'),
            meta: {
              title: '代理报表-后台管理系统'
            }
          },
          {
            path: 'reportMember',
            name: 'reportMember',
            component: () => import('../views/report/ReportMember'),
            meta: {
              title: '会员报表-后台管理系统'
            }
          },
          {
            path: 'reportMemberSearch',
            name: 'reportMemberSearch',
            component: () => import('../views/report/ReportMemberSearch'),
            meta: {
              title: '会员报表历史-后台管理系统'
            }
          },
          {
            path: 'reportFirstRecharge',
            name: 'reportFirstRecharge',
            component: () => import('../views/report/ReportFirstRecharge'),
            meta: {
              title: '首充报表-后台管理系统'
            }
          },
          {
            path: 'reportLottery',
            name: 'reportLottery',
            component: () => import('../views/report/ReportLottery'),
            meta: {
              title: '彩种报表-后台管理系统'
            }
          },
          {
            path: 'reportGame',
            name: 'reportGame',
            component: () => import('../views/report/ReportGame'),
            meta: {
              title: '棋牌报表-后台管理系统'
            }
          },
          {
            path: 'reportSource',
            name: 'reportSource',
            component: () => import('../views/report/ReportSource'),
            meta: {
              title: '终端报表-后台管理系统'
            }
          },
          {
            path: 'reportGrade',
            name: 'reportGrade',
            component: () => import('../views/report/ReportGrade'),
            meta: {
              title: '等级报表-后台管理系统'
            }
          },
          {
            path: 'reportPayment',
            name: 'reportPayment',
            component: () => import('../views/report/ReportPayment'),
            meta: {
              title: '支付报表-后台管理系统'
            }
          },
          // 流量报表暂时不上，先注释 2019年7月19日 21:52:53
          // 流量报表暂时不上，先注释 2019年7月24日 11:22:42
          // 流量报表上线2019年8月1日 14:59:59
          {
            path: 'reportFlow',
            name: 'reportFlow',
            component: () => import('../views/report/ReportFlow'),
            meta: {
              title: '流量报表-后台管理系统'
            }
          },
          {
            path: 'reportFlowDl',
            name: 'reportFlowDl',
            component: () => import('../views/report/ReportFlowDl'),
            meta: {
              title: '地理详情-后台管理系统'
            }
          },
          {
            path: 'reportFlowLl',
            name: 'reportFlowLl',
            component: () => import('../views/report/ReportFlowLl'),
            meta: {
              title: '流量详情-后台管理系统'
            }
          }
        ]
      },
      {
        path: '/dashBoard',
        name: 'dashboard',
        component: () => import('../views/DashBoard'),
        meta: {
          title: '管理首页-后台管理系统'
        }
      },
      {
        path: '/useInfo',
        name: 'useInfo',
        component: () => import('../views/UseInfo'),
        meta: {
          title: '修改密码-后台管理系统'
        }
      }
      // {
      //   path: 'dashboard',
      //   name: 'dashboard',
      //   component: () => import('../views/DashBoard'),
      //   meta: {
      //     title: '平台首页-后台管理系统'
      //   }
      // }
      // {
      //   path: '/',
      //   // name: 'dashboard',
      //   name: 'main',
      //   // component: () => import('../views/DashBoard'),
      //   component: () => import('../views/index/SidebarIndex'),
      //   meta: {
      //     title: '管理首页-后台管理系统'
      //   }
      // }
    ]
  }
]
export default router
