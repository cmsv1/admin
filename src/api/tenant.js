import { rest } from './index'
// 用户管理
const sysTenant = {
  urlList: '/v1/management/tenant/urlList',
  url: '/v1/management/tenant/url',
  addUrl: '/v1/management/tenant/addUrl',
  updateUrl: '/v1/management/tenant/updateUrl',
  deleteUrl: '/v1/management/tenant/deleteUrl',
  getTenantAppSystem: '/v1/management/tenant/getTenantAppSystem',
  updateTenantAppImgSystem: '/v1/management/tenant/updateTenantAppImgSystem',
  updateTenantAppOtherSystem: '/v1/management/tenant/updateTenantAppOtherSystem',
  tenantList: '/v1/management/tenant/tenantList',
  tenant: '/v1/management/tenant/tenant',
  addTenant: '/v1/management/tenant/addTenant',
  updateTenant: '/v1/management/tenant/updateTenant',
  updateTenantState: '/v1/management/tenant/updateTenantState',
  updateTenantToken: '/v1/management/tenant/updateTenantToken',
  maintenance: '/v1/management/tenant/maintenance',
  updateWebMaintenance: '/v1/management/tenant/updateWebMaintenance',
  updateCmsMaintenance: '/v1/management/tenant/updateCmsMaintenance',
  ipList: '/v1/management/tenant/ipList',
  ip: '/v1/management/tenant/ip',
  addIp: '/v1/management/tenant/addIp',
  updateIp: '/v1/management/tenant/updateIp',
  deleteIp: '/v1/management/tenant/deleteIp',
  platformProleList: '/v1/management/manager/platformProleList',
  platformProle: '/v1/management/manager/platformProle',
  addPlatformProle: '/v1/management/manager/addPlatformProle',
  updatePlatformProle: '/v1/management/manager/updatePlatformProle',
  deletePlatformProle: '/v1/management/manager/deletePlatformProle',
  platformTroleList: '/v1/management/manager/platformTroleList',
  platformTrole: '/v1/management/manager/platformTrole',
  addPlatformTrole: '/v1/management/manager/addPlatformTrole',
  updatePlatformTrole: '/v1/management/manager/updatePlatformTrole',
  deletePlatformTrole: '/v1/management/manager/deletePlatformTrole',
  platformManagerList: '/v1/management/manager/platformManagerList',
  platformManager: '/v1/management/manager/platformManager',
  addPlatformManager: '/v1/management/manager/addPlatformManager',
  updatePlatformManager: '/v1/management/manager/updatePlatformManager',
  deletePlatformManager: '/v1/management/manager/deletePlatformManager',
  manageableList: '/v1/management/manager/manageableList',
  switchTenant: '/v1/management/manager/switchTenant',
  announcementList: '/v1/management/manager/announcementList',
  announcement: '/v1/management/manager/announcement',
  addAnnouncement: '/v1/management/manager/addAnnouncement',
  updateAnnouncement: '/v1/management/manager/updateAnnouncement',
  deleteAnnouncement: '/v1/management/manager/deleteAnnouncement',
  getSystemLiveTip: '/v1/live/getSystemLiveTip',
  setSystemLiveTip: '/v1/live/setSystemLiveTip',
  getSystemLiveBarrage: '/v1/live/getSystemLiveBarrage',
  setSystemLiveBarrage: '/v1/live/setSystemLiveBarrage',
  getSystemPredefinedLiveComment: '/v1/live/getSystemPredefinedLiveComment',
  setSystemPredefinedLiveComment: '/v1/live/setSystemPredefinedLiveComment',
  getSystemFreelyLiveComment: '/v1/live/getSystemFreelyLiveComment',
  setSystemFreelyLiveComment: '/v1/live/setSystemFreelyLiveComment',
  addLiveState: '/v1/live/addLiveState',
  deleteLiveState: '/v1/live/deleteLiveState',
  queryLiveState: '/v1/live/queryLiveState',
  updatePlatformManagerState: '/v1/management/manager/updatePlatformManagerState',
  getImageConfigList: '/v1/management/content/getImageConfigList',
  setImageConfig: '/v1/management/content/setImageConfig',
  platformPpermissionList: '/v1/management/manager/platformPpermissionList',
  platformTpermissionList: '/v1/management/manager/platformTpermissionList',
  logList: '/v1/management/manager/logList',
  certificationList: '/v1/users/certificationList',
  setCertificationState: '/v1/users/setCertificationState',
  systemPaymentAssistanceSettingsList: '/v1/transaction/systemPaymentAssistanceSettingsList',
  updateSystemPaymentAssistanceSettings: '/v1/transaction/updateSystemPaymentAssistanceSettings',
  lotteryList: '/v1/lottery/lotteryList',
  howToPlay: '/v1/lottery/howToPlay',
  setHowToPlay: '/v1/lottery/howToPlay',
  updateSaleStatus: '/v1/lottery/updateSaleStatus',
  manageUserNickName: '/v1/users/manageUserNickName',
  manageUpdateUserNickName: '/v1/users/manageUpdateUserNickName',
  getManualOpen: '/v1/lottery/getManualOpen',
  manualOpen: '/v1/lottery/manualOpen',
  setClearing: '/v1/report/clearing',
  getClearing: '/v1/report/clearing',
  resetUserList: '/v1/users/resetUserList',
  resetUser: '/v1/users/resetUser',
  updateOpenNumber: '/v1/lottery/updateOpenNumber',
  getCorrectExcel: '/v1/betting/getCorrectExcel',
  saveOrUpdateThirdPartySettings: '/v1/transaction/saveOrUpdateThirdPartySettings',
  queryThirdPartyPaySettingsList: '/v1/transaction/queryThirdPartyPaySettingsList',
  queryConfigurationInformation: '/v1/transaction/queryConfigurationInformation',
  officeLottery: '/v1/lottery/officeLottery',
  updateThirdPartyPaySettingsState: '/v1/transaction/updateThirdPartyPaySettingsState',
  updateTenantAppSystem: '/v1/management/tenant/updateTenantAppSystem',
  styleList: '/v1/management/content/styleList',
  selectKillControl: '/v1/lottery/killControl/selectKillControl',
  selectKillProcessByOpenTime: '/v1/lottery/killControl/selectKillProcessByOpenTime',
  updatePurposesProfitFromCms: '/v1/lottery/killControl/updatePurposesProfitFromCms',
  updateKillOpenFromCms: '/v1/lottery/killControl/updateKillOpenFromCms',
  selectKillControlGame: '/v1/game/killControl/selectKillControl',
  updatePurposesProfitFromCmsGame: '/v1/game/killControl/updatePurposesProfitFromCms',
  updateKillOpenFromCmsGame: '/v1/game/killControl/updateKillOpenFromCms',
  selectKillProcessByOpenTimeGame: '/v1/game/killControl/selectKillProcessByOpenTime',
  gameCmsList: '/v1/game/gameCmsList',
  gamehowToPlay: '/v1/game/howToPlay',
  updateHowToPlay: '/v1/game/updateHowToPlay',
  gameUpdateSaleStatus: '/v1/game/updateSaleStatus',
  gameCancel: '/v1/game/gameCancel',
  getGameCancelList: '/v1/game/getGameCancelList',
  getSettings: '/v1/security/getSettings',
  updateSettings: '/v1/security/updateSettings',
  getHostList: '/v1/security/getHostList',
  updateHost: '/v1/security/updateHost',
  updateHostMonitored: '/v1/security/updateHostMonitored',
  updateGf: '/v1/security/updateGf',
  getPackageList: '/v1/security/getPackageList',
  getDomainList: '/v1/security/getDomainList',
  getServerList: '/v1/security/getServerList',
  getIpList: '/v1/security/getIpList',
  getLogList: '/v1/security/getLogList',
  addDomain: '/v1/security/addDomain',
  addServer: '/v1/security/addServer',
  updatePackage: '/v1/security/updatePackage',
  networkAddIp: '/v1/security/addIp',
  updateServer: '/v1/security/updateServer',
  ＮetworkUpdateIp: '/v1/security/updateIp',
  nicknameFilter: '/v1/users/nicknameFilter',
  setNicknameFilter: '/v1/users/setNicknameFilter',
  updateNicknameFilter: '/v1/users/updateNicknameFilter',
  gameEnumBackPlatform: '/v1/game/gameEnumBackPlatform',
  deleteNicknameFilter: '/v1/users/deleteNicknameFilter',
  getIpRegion: '/v1/security/getIpRegion',
  getAppUrlList: '/v1/security/getAppUrlList',
  setAppUrl: '/v1/security/setAppUrl',
  findLiveNodes: '/v1/live/findLiveNodes',
  updateLiveNodeShutdown: '/v1/live/updateLiveNodeShutdown',
  getHostBackupList: '/v1/security/getHostBackupList',
  addHostBackup: '/v1/security/addHostBackup',
  updateHostBackup: '/v1/security/updateHostBackup',
  deleteHostBackup: '/v1/security/deleteHostBackup',
  // 新增站长彩种相关接口
  findTenantSystemLottery: '/v1/lottery/tenantLottery/findTenantSystemLottery', // 站长彩种下拉
  updateTenantSaleStatus: '/v1/lottery/tenantLottery/updateTenantSaleStatus', // 站长彩种-修改销售状态
  addTenantLottery: '/v1/lottery/tenantLottery/addTenantLottery', // 站长彩种-新增站长彩种
  updateTenantLotteryName: '/v1/lottery/tenantLottery/updateTenantLotteryName', // 站长彩种-修改站长彩种名称
  updateTenantKillFromCms: '/v1/lottery/tenantLottery/updateTenantKillFromCms', // 站长彩种-修改赢率控制
  findTenantLotteryFromCms: '/v1/lottery/tenantLottery/findTenantLotteryFromCms', // 站长彩种-查询列表
  checkCalculate: '/v1/betting/checkCalculate',
  getDafaLottery: '/v1/betting/getDafaLottery',
  reCalculate: '/v1/betting/reCalculate',
  gameCheckCalculate: '/v1/game/checkCalculate',
  gameReCalculate: '/v1/game/reCalculate',
  setPlayInfo: '/v1/lottery/tenantLottery/howToPlay', // 修改站长彩种玩法说明
  saveTenantBroadcast: '/v1/management/tenant/saveTenantBroadcast', // 系统广播
  updateTenantBroadcast: '/v1/management/tenant/updateTenantBroadcast',
  listTenantBroadcast: '/v1/management/tenant/listTenantBroadcast',
  deleteTenantBroadcast: '/v1/management/tenant/deleteTenantBroadcast',
  getTenantBroadcastConfig: '/v1/management/tenant/getTenantBroadcastConfig',
  setTenantBroadcastConfig: '/v1/management/tenant/setTenantBroadcastConfig',
  gatewayLimitAdd: '/v1/security/gatewayLimit/add', // 新增网关访问限制
  gatewayLimitDelete: '/v1/security/gatewayLimit/delete', // 删除网关访问限制
  gatewayLimitList: '/v1/security/gatewayLimit/list', // 网关访问限制    列表
  updateRegisterVerify: '/v1/management/tenant/updateRegisterVerify', // 网关访问限制列表
  addFeedback: '/v1/management/content/addFeedback', // 新增反馈内容
  deleteFeedback: '/v1/management/content/deleteFeedback', // 删除反馈
  tenantFeedbackList: '/v1/management/content/tenantFeedbackList', // 站长反馈查询列表
  tenantShowFeedback: '/v1/management/content/tenantShowFeedback', // 站长反馈查看
  platformFeedbackList: '/v1/management/content/platformFeedbackList', // 站长反馈列表
  platformShowFeedback: '/v1/management/content/platformShowFeedback',
  feedbackTenant: '/v1/management/content/feedbackTenant',
  nextFeedback: '/v1/management/content/nextFeedback',
  feedbackMarkType: '/v1/management/content/feedbackMarkType', // 盖章
  getRenewalLog: '/v1/security/getRenewalLog', // 續費
  getTenantSslUrlCount: '/v1/security/getTenantSslUrlCount'
}
export default {
  // 平台网址列表
  urlList (param, callbackFunc) {
    return rest.get(sysTenant.urlList, param, callbackFunc)
  },
  // 平台网址编辑页获取数据
  url (param, callbackFunc) {
    return rest.get(sysTenant.url, param, callbackFunc)
  },
  // 平台网址新增数据
  addUrl (param, callbackFunc) {
    return rest.post(sysTenant.addUrl, param, callbackFunc)
  },
  // 平台网址修改数据
  updateUrl (param, callbackFunc) {
    return rest.post(sysTenant.updateUrl, param, callbackFunc)
  },
  // 平台网址删除数据
  deleteUrl (param, callbackFunc) {
    return rest.post(sysTenant.deleteUrl, param, callbackFunc)
  },
  // 系统APP获取数据
  getTenantAppSystem (param, callbackFunc) {
    return rest.get(sysTenant.getTenantAppSystem, param, callbackFunc)
  },
  // 系统APP上传图片
  updateTenantAppImgSystem (param, callbackFunc) {
    return rest.create(sysTenant.updateTenantAppImgSystem, param, callbackFunc)
  },
  // 系统APP更新数据
  updateTenantAppOtherSystem (param, callbackFunc) {
    return rest.post(sysTenant.updateTenantAppOtherSystem, param, callbackFunc)
  },
  // 站长管理列表
  tenantList (param, callbackFunc) {
    return rest.get(sysTenant.tenantList, param, callbackFunc)
  },
  // 站长管理编辑页获取数据
  tenant (param, callbackFunc) {
    return rest.get(sysTenant.tenant, param, callbackFunc)
  },
  // 站长管理开户
  addTenant (param, callbackFunc) {
    return rest.post(sysTenant.addTenant, param, callbackFunc)
  },
  // 站长管理修改
  updateTenant (param, callbackFunc) {
    return rest.post(sysTenant.updateTenant, param, callbackFunc)
  },
  // 站长管理开站或关站
  updateTenantState (param, callbackFunc) {
    return rest.post(sysTenant.updateTenantState, param, callbackFunc)
  },
  // 站长管理开启令牌或禁用令牌
  updateTenantToken (param, callbackFunc) {
    return rest.post(sysTenant.updateTenantToken, param, callbackFunc)
  },
  // 站长管理维护设置获取数据
  maintenance (param, callbackFunc) {
    return rest.get(sysTenant.maintenance, param, callbackFunc)
  },
  // 站长管理维护设置前台更新数据
  updateWebMaintenance (param, callbackFunc) {
    return rest.post(sysTenant.updateWebMaintenance, param, callbackFunc)
  },
  // 站长管理维护设置后台更新数据
  updateCmsMaintenance (param, callbackFunc) {
    return rest.post(sysTenant.updateCmsMaintenance, param, callbackFunc)
  },
  // 站长管理的IP管理列表
  ipList (param, callbackFunc) {
    return rest.get(sysTenant.ipList, param, callbackFunc)
  },
  // 站长管理的IP管理编辑页获取数据
  ip (param, callbackFunc) {
    return rest.get(sysTenant.ip, param, callbackFunc)
  },
  // 站长管理的IP管理添加数据
  addIp (param, callbackFunc) {
    return rest.post(sysTenant.addIp, param, callbackFunc)
  },
  // 站长管理的IP管理修改数据
  updateIp (param, callbackFunc) {
    return rest.post(sysTenant.updateIp, param, callbackFunc)
  },
  // 站长管理的IP管理删除数据
  deleteIp (param, callbackFunc) {
    return rest.post(sysTenant.deleteIp, param, callbackFunc)
  },
  // 平台角色列表
  platformProleList (param, callbackFunc) {
    return rest.get(sysTenant.platformProleList, param, callbackFunc)
  },
  // 平台角色编辑页获取数据
  platformProle (param, callbackFunc) {
    return rest.get(sysTenant.platformProle, param, callbackFunc)
  },
  // 平台角色添加数据
  addPlatformProle (param, callbackFunc) {
    return rest.post(sysTenant.addPlatformProle, param, callbackFunc)
  },
  // 平台角色更新数据
  updatePlatformProle (param, callbackFunc) {
    return rest.post(sysTenant.updatePlatformProle, param, callbackFunc)
  },
  // 平台角色删除数据
  deletePlatformProle (param, callbackFunc) {
    return rest.post(sysTenant.deletePlatformProle, param, callbackFunc)
  },
  // 站长角色列表
  platformTroleList (param, callbackFunc) {
    return rest.get(sysTenant.platformTroleList, param, callbackFunc)
  },
  // 站长角色编辑页获取数据
  platformTrole (param, callbackFunc) {
    return rest.get(sysTenant.platformTrole, param, callbackFunc)
  },
  // 站长角色添加数据
  addPlatformTrole (param, callbackFunc) {
    return rest.post(sysTenant.addPlatformTrole, param, callbackFunc)
  },
  // 站长角色更新数据
  updatePlatformTrole (param, callbackFunc) {
    return rest.post(sysTenant.updatePlatformTrole, param, callbackFunc)
  },
  // 平台角色删除数据
  deletePlatformTrole (param, callbackFunc) {
    return rest.post(sysTenant.deletePlatformTrole, param, callbackFunc)
  },
  // 平台管理员列表
  platformManagerList (param, callbackFunc) {
    return rest.get(sysTenant.platformManagerList, param, callbackFunc)
  },
  // 平台管理员编辑页获取数据
  platformManager (param, callbackFunc) {
    return rest.get(sysTenant.platformManager, param, callbackFunc)
  },
  // 平台管理员添加
  addPlatformManager (param, callbackFunc) {
    return rest.post(sysTenant.addPlatformManager, param, callbackFunc)
  },
  // 平台管理员修改
  updatePlatformManager (param, callbackFunc) {
    return rest.post(sysTenant.updatePlatformManager, param, callbackFunc)
  },
  // 平台管理员删除
  deletePlatformManager (param, callbackFunc) {
    return rest.post(sysTenant.deletePlatformManager, param, callbackFunc)
  },
  // 切换站的站长列表
  manageableList (param, callbackFunc) {
    return rest.get(sysTenant.manageableList, param, callbackFunc)
  },
  // 切换站操作
  switchTenant (param, callbackFunc) {
    return rest.post(sysTenant.switchTenant, param, callbackFunc)
  },
  // 平台站长公告列表
  announcementList (param, callbackFunc) {
    return rest.get(sysTenant.announcementList, param, callbackFunc)
  },
  // 平台站长公告编辑页获取数据
  announcement (param, callbackFunc) {
    return rest.get(sysTenant.announcement, param, callbackFunc)
  },
  // 平台站长公告添加
  addAnnouncement (param, callbackFunc) {
    return rest.post(sysTenant.addAnnouncement, param, callbackFunc)
  },
  // 平台站长公告修改
  updateAnnouncement (param, callbackFunc) {
    return rest.post(sysTenant.updateAnnouncement, param, callbackFunc)
  },
  // 平台站长公告删除
  deleteAnnouncement (param, callbackFunc) {
    return rest.post(sysTenant.deleteAnnouncement, param, callbackFunc)
  },
  // 直播管理-打赏设置获取数据
  getSystemLiveTip (param, callbackFunc) {
    return rest.get(sysTenant.getSystemLiveTip, param, callbackFunc)
  },
  // 直播管理-打赏设置保存
  setSystemLiveTip (param, callbackFunc) {
    return rest.post(sysTenant.setSystemLiveTip, param, callbackFunc)
  },
  // 直播管理-弹幕设置获取数据
  getSystemLiveBarrage (param, callbackFunc) {
    return rest.get(sysTenant.getSystemLiveBarrage, param, callbackFunc)
  },
  // 直播管理-弹幕设置保存
  setSystemLiveBarrage (param, callbackFunc) {
    return rest.post(sysTenant.setSystemLiveBarrage, param, callbackFunc)
  },
  // 直播管理-系统发言设置获取数据
  getSystemPredefinedLiveComment (param, callbackFunc) {
    return rest.get(sysTenant.getSystemPredefinedLiveComment, param, callbackFunc)
  },
  // 直播管理-系统发言设置保存
  setSystemPredefinedLiveComment (param, callbackFunc) {
    return rest.post(sysTenant.setSystemPredefinedLiveComment, param, callbackFunc)
  },
  // 直播管理-自由发言设置获取数据
  getSystemFreelyLiveComment (param, callbackFunc) {
    return rest.get(sysTenant.getSystemFreelyLiveComment, param, callbackFunc)
  },
  // 直播管理-自由发言设置保存
  setSystemFreelyLiveComment (param, callbackFunc) {
    return rest.post(sysTenant.setSystemFreelyLiveComment, param, callbackFunc)
  },
  // 直播管理-站点配置添加
  addLiveState (param, callbackFunc) {
    return rest.post(sysTenant.addLiveState, param, callbackFunc)
  },
  // 直播管理-站点配置删除
  deleteLiveState (param, callbackFunc) {
    return rest.post(sysTenant.deleteLiveState, param, callbackFunc)
  },
  // 直播管理-站点配置列表
  queryLiveState (param, callbackFunc) {
    return rest.get(sysTenant.queryLiveState, param, callbackFunc)
  },
  // 平台管理员-更新状态
  updatePlatformManagerState (param, callbackFunc) {
    return rest.post(sysTenant.updatePlatformManagerState, param, callbackFunc)
  },
  // 站长管理-图片规格获取数据
  getImageConfigList (param, callbackFunc) {
    return rest.get(sysTenant.getImageConfigList, param, callbackFunc)
  },
  // 站长管理-图片规格设置
  setImageConfig (param, callbackFunc) {
    return rest.post(sysTenant.setImageConfig, param, callbackFunc)
  },
  // 平台管理-平台权限清单
  platformPpermissionList (param, callbackFunc) {
    return rest.get(sysTenant.platformPpermissionList, param, callbackFunc)
  },
  // 平台管理-站长权限清单
  platformTpermissionList (param, callbackFunc) {
    return rest.get(sysTenant.platformTpermissionList, param, callbackFunc)
  },
  // 平台管理-管理员日志
  logList (param, callbackFunc) {
    return rest.get(sysTenant.logList, param, callbackFunc)
  },
  // 平台管理-人工申诉列表
  certificationList (param, callbackFunc) {
    return rest.get(sysTenant.certificationList, param, callbackFunc)
  },
  // 平台管理-人工申诉审核操作
  setCertificationState (param, callbackFunc) {
    return rest.post(sysTenant.setCertificationState, param, callbackFunc)
  },
  // 平台管理-充值说明列表
  systemPaymentAssistanceSettingsList (param, callbackFunc) {
    return rest.get(sysTenant.systemPaymentAssistanceSettingsList, param, callbackFunc)
  },
  // 平台管理-充值说明修改
  updateSystemPaymentAssistanceSettings (param, callbackFunc) {
    return rest.post(sysTenant.updateSystemPaymentAssistanceSettings, param, callbackFunc)
  },
  // 平台管理-彩种管理列表
  lotteryList (param, callbackFunc) {
    return rest.get(sysTenant.lotteryList, param, callbackFunc)
  },
  // 平台管理-彩种管理编辑页获取数据
  howToPlay (param, callbackFunc) {
    return rest.get(sysTenant.howToPlay, param, callbackFunc)
  },
  // 平台管理-彩种管理编辑页修改数据
  setHowToPlay (param, callbackFunc) {
    return rest.post(sysTenant.setHowToPlay, param, callbackFunc)
  },
  // 平台管理-暂停或启用操作
  updateSaleStatus (param, callbackFunc) {
    return rest.get(sysTenant.updateSaleStatus, param, callbackFunc)
  },
  // 昵称管理列表
  manageUserNickName (param, callbackFunc) {
    return rest.get(sysTenant.manageUserNickName, param, callbackFunc)
  },
  // 昵称管理-修改操作
  manageUpdateUserNickName (param, callbackFunc) {
    return rest.post(sysTenant.manageUpdateUserNickName, param, callbackFunc)
  },
  // 手动开奖列表
  getManualOpen (param, callbackFunc) {
    return rest.get(sysTenant.getManualOpen, param, callbackFunc)
  },
  // 手动开奖-新增操作
  manualOpen (param, callbackFunc) {
    return rest.post(sysTenant.manualOpen, param, callbackFunc)
  },
  // 报表清除
  setClearing (param, callbackFunc) {
    return rest.post(sysTenant.setClearing, param, callbackFunc)
  },
  // 报表清除-列表获取数据
  getClearing (param, callbackFunc) {
    return rest.get(sysTenant.getClearing, param, callbackFunc)
  },
  // 会员重置-列表
  resetUserList (param, callbackFunc) {
    return rest.get(sysTenant.resetUserList, param, callbackFunc)
  },
  // 会员重置操作
  resetUser (param, callbackFunc) {
    return rest.post(sysTenant.resetUser, param, callbackFunc)
  },
  // 第三方管理保存操作
  saveOrUpdateThirdPartySettings (param, callbackFunc) {
    return rest.post(sysTenant.saveOrUpdateThirdPartySettings, param, callbackFunc)
  },
  // 第三方管理列表获取数据
  queryThirdPartyPaySettingsList (param, callbackFunc) {
    return rest.get(sysTenant.queryThirdPartyPaySettingsList, param, callbackFunc)
  },
  // 第三方管理列编辑页获取数据
  queryConfigurationInformation (param, callbackFunc) {
    return rest.get(sysTenant.queryConfigurationInformation, param, callbackFunc)
  },
  // 获取官方彩种
  officeLottery (param, callbackFunc) {
    return rest.get(sysTenant.officeLottery, param, callbackFunc)
  },
  // 第三方启用或禁用操作
  updateThirdPartyPaySettingsState (param, callbackFunc) {
    return rest.post(sysTenant.updateThirdPartyPaySettingsState, param, callbackFunc)
  },
  // 开奖纠错
  updateOpenNumber (param, callbackFunc) {
    return rest.post(sysTenant.updateOpenNumber, param, callbackFunc)
  },
  // 平台管理-系统APP保存
  updateTenantAppSystem (param, callbackFunc) {
    return rest.create(sysTenant.updateTenantAppSystem, param, callbackFunc)
  },
  // 平台管理-开奖纠正导出Excel
  getCorrectExcel (param, callbackFunc) {
    return rest.get(sysTenant.getCorrectExcel, param, callbackFunc)
  },
  // 平台管理-盈率调节
  selectKillControl (param, callbackFunc) {
    return rest.get(sysTenant.selectKillControl, param, callbackFunc)
  },
  selectKillControlGame (param, callbackFunc) {
    return rest.get(sysTenant.selectKillControlGame, param, callbackFunc)
  },
  // 盈率调节记录
  selectKillProcessByOpenTime (param, callbackFunc) {
    return rest.get(sysTenant.selectKillProcessByOpenTime, param, callbackFunc)
  },
  // 棋牌盈率调节记录
  selectKillProcessByOpenTimeGame (param, callbackFunc) {
    return rest.get(sysTenant.selectKillProcessByOpenTimeGame, param, callbackFunc)
  },
  // 盈率调节彈窗
  updatePurposesProfitFromCms (param, callbackFunc) {
    return rest.post(sysTenant.updatePurposesProfitFromCms, param, callbackFunc)
  },
  // 棋牌盈率调节彈窗
  updatePurposesProfitFromCmsGame (param, callbackFunc) {
    return rest.post(sysTenant.updatePurposesProfitFromCmsGame, param, callbackFunc)
  },
  // 盈率调节按鈕
  updateKillOpenFromCms (param, callbackFunc) {
    return rest.post(sysTenant.updateKillOpenFromCms, param, callbackFunc)
  },
  // 棋牌盈率调节按鈕
  updateKillOpenFromCmsGame (param, callbackFunc) {
    return rest.post(sysTenant.updateKillOpenFromCmsGame, param, callbackFunc)
  },
  // 平台管理－棋牌管理 (获取采种配置列表)
  gameCmsList (param, callbackFunc) {
    return rest.get(sysTenant.gameCmsList, param, callbackFunc)
  },
  // 平台管理－棋牌管理 (玩法查看)
  gamehowToPlay (param, callbackFunc) {
    return rest.get(sysTenant.gamehowToPlay, param, callbackFunc)
  },
  // 平台管理－棋牌管理 (玩法更新)
  updateHowToPlay (param, callbackFunc) {
    return rest.post(sysTenant.updateHowToPlay, param, callbackFunc)
  },
  // 平台管理－棋牌管理 (禁止販售)
  gameUpdateSaleStatus (param, callbackFunc) {
    return rest.post(sysTenant.gameUpdateSaleStatus, param, callbackFunc)
  },
  // 平台管理－撤單更新
  gameCancel (param, callbackFunc) {
    return rest.post(sysTenant.gameCancel, param, callbackFunc)
  },
  getGameCancelList (param, callbackFunc) {
    return rest.get(sysTenant.getGameCancelList, param, callbackFunc)
  },
  // 獲取運維設置
  getSettings (param, callbackFunc) {
    return rest.get(sysTenant.getSettings, param, callbackFunc)
  },
  // 網絡運維 更新運維設置
  updateSettings (param, callbackFunc) {
    return rest.post(sysTenant.updateSettings, param, callbackFunc)
  },
  // 網絡運維 host列表
  getHostList (param, callbackFunc) {
    return rest.get(sysTenant.getHostList, param, callbackFunc)
  },
  // 網絡運維 運維遷移host
  updateHost (param, callbackFunc) {
    return rest.post(sysTenant.updateHost, param, callbackFunc)
  },
  // 網絡運維 更新host自動切防回切
  updateHostMonitored (param, callbackFunc) {
    return rest.post(sysTenant.updateHostMonitored, param, callbackFunc)
  },
  // 網絡運維 更新高防源ip
  updateGf (param, callbackFunc) {
    return rest.post(sysTenant.updateGf, param, callbackFunc)
  },
  // 網絡運維 站長套餐列表
  getPackageList (param, callbackFunc) {
    return rest.get(sysTenant.getPackageList, param, callbackFunc)
  },
  // 網絡運維 域名列表
  getDomainList (param, callbackFunc) {
    return rest.get(sysTenant.getDomainList, param, callbackFunc)
  },
  // 網絡運維 服务器管理列表
  getServerList (param, callbackFunc) {
    return rest.get(sysTenant.getServerList, param, callbackFunc)
  },
  // 網絡運維 服务器管理列表
  getIpList (param, callbackFunc) {
    return rest.get(sysTenant.getIpList, param, callbackFunc)
  },
  // 網絡運維 日誌列表
  getLogList (param, callbackFunc) {
    return rest.get(sysTenant.getLogList, param, callbackFunc)
  },
  // 網絡運維 新增域名
  addDomain (param, callbackFunc) {
    return rest.post(sysTenant.addDomain, param, callbackFunc)
  },
  // 網絡運維 新增服務器
  addServer (param, callbackFunc) {
    return rest.post(sysTenant.addServer, param, callbackFunc)
  },
  // 更新站長套餐
  updatePackage (param, callbackFunc) {
    return rest.post(sysTenant.updatePackage, param, callbackFunc)
  },
  // 更新站長套餐
  networkAddIp (param, callbackFunc) {
    return rest.create(sysTenant.networkAddIp, param, callbackFunc)
  },
  // 網路運維修改服務器
  updateServer (param, callbackFunc) {
    return rest.post(sysTenant.updateServer, param, callbackFunc)
  },
  // 網路運維修改服務器
  ＮetworkUpdateIp (param, callbackFunc) {
    return rest.post(sysTenant.ＮetworkUpdateIp, param, callbackFunc)
  },
  // 獲取昵称过滤列表
  nicknameFilter (param, callbackFunc) {
    return rest.get(sysTenant.nicknameFilter, param, callbackFunc)
  },
  // 新增昵称过滤
  setNicknameFilter (param, callbackFunc) {
    return rest.post(sysTenant.setNicknameFilter, param, callbackFunc)
  },
  // 修改昵称过滤
  updateNicknameFilter (param, callbackFunc) {
    return rest.post(sysTenant.updateNicknameFilter, param, callbackFunc)
  },
  gameEnumBackPlatform (param, callbackFunc) {
    return rest.get(sysTenant.gameEnumBackPlatform, param, callbackFunc)
  },
  deleteNicknameFilter (param, callbackFunc) {
    return rest.post(sysTenant.deleteNicknameFilter, param, callbackFunc)
  },
  // 获取IP列表
  getIpRegion (param, callbackFunc) {
    return rest.get(sysTenant.getIpRegion, param, callbackFunc)
  },
  // 获取app域名调度列表
  getAppUrlList (param, callbackFunc) {
    return rest.get(sysTenant.getAppUrlList, param, callbackFunc)
  },
  // 修改app域名
  setAppUrl (param, callbackFunc) {
    return rest.post(sysTenant.setAppUrl, param, callbackFunc)
  },
  // 获取直播调度列表
  findLiveNodes (param, callbackFunc) {
    return rest.get(sysTenant.findLiveNodes, param, callbackFunc)
  },
  // 更新状态
  updateLiveNodeShutdown (param, callbackFunc) {
    return rest.get(sysTenant.updateLiveNodeShutdown, param, callbackFunc)
  },
  // 網路運為備用列表
  getHostBackupList (param, callbackFunc) {
    return rest.get(sysTenant.getHostBackupList, param, callbackFunc)
  },
  // 新增
  addHostBackup (param, callbackFunc) {
    return rest.post(sysTenant.addHostBackup, param, callbackFunc)
  },
  // 手動更新
  updateHostBackup (param, callbackFunc) {
    return rest.post(sysTenant.updateHostBackup, param, callbackFunc)
  },
  // 刪除
  deleteHostBackup (param, callbackFunc) {
    return rest.post(sysTenant.deleteHostBackup, param, callbackFunc)
  },
  // 站长彩种接口-下拉
  findTenantSystemLottery (param, callbackFunc) {
    return rest.get(sysTenant.findTenantSystemLottery, param, callbackFunc)
  },
  // 站长彩种-销售状态
  updateTenantSaleStatus (param, callbackFunc) {
    return rest.post(sysTenant.updateTenantSaleStatus, param, callbackFunc)
  },
  // 站长彩种-添加站长彩种
  addTenantLottery (param, callbackFunc) {
    return rest.post(sysTenant.addTenantLottery, param, callbackFunc)
  },
  // 站长彩种-修改站长彩种名称
  updateTenantLotteryName (param, callbackFunc) {
    return rest.post(sysTenant.updateTenantLotteryName, param, callbackFunc)
  },
  // 站长彩种-查询站长彩种列表
  findTenantLotteryFromCms (param, callbackFunc) {
    return rest.get(sysTenant.findTenantLotteryFromCms, param, callbackFunc)
  },
  // 站长彩种-更新赢率控制
  updateTenantKillFromCms (param, callbackFunc) {
    return rest.get(sysTenant.updateTenantKillFromCms, param, callbackFunc)
  },
  checkCalculate (param, callbackFunc) {
    return rest.get(sysTenant.checkCalculate, param, callbackFunc)
  },
  getDafaLottery (param, callbackFunc) {
    return rest.get(sysTenant.getDafaLottery, param, callbackFunc)
  },
  reCalculate (param, callbackFunc) {
    return rest.post(sysTenant.reCalculate, param, callbackFunc)
  },
  gameCheckCalculate (param, callbackFunc) {
    return rest.get(sysTenant.gameCheckCalculate, param, callbackFunc)
  },
  gameReCalculate (param, callbackFunc) {
    return rest.post(sysTenant.gameReCalculate, param, callbackFunc)
  },
  // 设置站长彩种玩法说明
  setPlayInfo (param, callbackFunc) {
    return rest.post(sysTenant.setPlayInfo, param, callbackFunc)
  },
  // 新增系统广播
  saveTenantBroadcast (param, callbackFunc) {
    return rest.post(sysTenant.saveTenantBroadcast, param, callbackFunc)
  },
  // 修改系统广播
  updateTenantBroadcast (param, callbackFunc) {
    return rest.post(sysTenant.updateTenantBroadcast, param, callbackFunc)
  },
  // 查询系统广播列表
  listTenantBroadcast (param, callbackFunc) {
    return rest.get(sysTenant.listTenantBroadcast, param, callbackFunc)
  },
  // 删除系统广播
  deleteTenantBroadcast (param, callbackFunc) {
    return rest.post(sysTenant.deleteTenantBroadcast, param, callbackFunc)
  },
  // 111自动广播设置
  getTenantBroadcastConfig (param, callbackFunc) {
    return rest.get(sysTenant.getTenantBroadcastConfig, param, callbackFunc)
  },
  // 设置自动广播
  setTenantBroadcastConfig (param, callbackFunc) {
    return rest.post(sysTenant.setTenantBroadcastConfig, param, callbackFunc)
  },
  // 新增网关访问限制
  gatewayLimitAdd (param, callbackFunc) {
    return rest.post(sysTenant.gatewayLimitAdd, param, callbackFunc)
  },
  // 删除网关访问限制
  gatewayLimitDelete (param, callbackFunc) {
    return rest.post(sysTenant.gatewayLimitDelete, param, callbackFunc)
  },
  // 网关访问限制列表
  gatewayLimitList (param, callbackFunc) {
    return rest.get(sysTenant.gatewayLimitList, param, callbackFunc)
  },
  // 修改开启或者关闭验证码
  updateRegisterVerify (param, callbackFunc) {
    return rest.post(sysTenant.updateRegisterVerify, param, callbackFunc)
  },
  // 新增反馈
  addFeedback (param, callbackFunc) {
    return rest.post(sysTenant.addFeedback, param, callbackFunc)
  },
  // 删除反馈内容
  deleteFeedback (param, callbackFunc) {
    return rest.post(sysTenant.deleteFeedback, param, callbackFunc)
  },
  // 站长反馈查询列表
  tenantFeedbackList (param, callbackFunc) {
    return rest.get(sysTenant.tenantFeedbackList, param, callbackFunc)
  },
  // 站长反馈列表
  platformFeedbackList (param, callbackFunc) {
    return rest.get(sysTenant.platformFeedbackList, param, callbackFunc)
  },
  // 站长反馈查看
  platformShowFeedback (param, callbackFunc) {
    return rest.get(sysTenant.platformShowFeedback, param, callbackFunc)
  },
  feedbackTenant (param, callbackFunc) {
    return rest.post(sysTenant.feedbackTenant, param, callbackFunc)
  },
  nextFeedback (param, callbackFunc) {
    return rest.get(sysTenant.nextFeedback, param, callbackFunc)
  },
  feedbackMarkType: function (param, callbackFunc) {
    return rest.post(sysTenant.feedbackMarkType, param, callbackFunc)
  },
  getRenewalLog (param, callbackFunc) {
    return rest.get(sysTenant.getRenewalLog, param, callbackFunc)
  },
  getTenantSslUrlCount (param, callbackFunc) {
    return rest.get(sysTenant.getTenantSslUrlCount, param, callbackFunc)
  }
}
