import { rest } from './index'
// 用户管理
const sysManage = {
  xAnnouncementList: '/v1/management/manager/xAnnouncementList',
  xUnreadAnnouncement: '/v1/management/manager/xUnreadAnnouncement',
  xAnnouncement: '/v1/management/manager/xAnnouncement',
  listTenantTBroadcast: '/v1/management/tenant/listTenantTBroadcast', // 获取广播消息
  tenantDeleteBroadcast: '/v1/management/tenant/tenantDeleteBroadcast', // 删除广播消息
  tenantFeedbackList: '/v1/management/content/tenantFeedbackList', // 站长反馈查询列表
  deleteFeedback: '/v1/management/content/deleteFeedback' // 删除反馈
}
export default {
  // 后台首页公告列表
  xAnnouncementList (param, callbackFunc) {
    return rest.get(sysManage.xAnnouncementList, param, callbackFunc)
  },
  // 未读公告数量
  xUnreadAnnouncement (param, callbackFunc) {
    return rest.get(sysManage.xUnreadAnnouncement, param, callbackFunc)
  },
  // 后台首页-公告信息
  xAnnouncement (param, callbackFunc) {
    return rest.get(sysManage.xAnnouncement, param, callbackFunc)
  },
  // 获取广播消息
  listTenantTBroadcast (param, callbackFunc) {
    return rest.get(sysManage.listTenantTBroadcast, param, callbackFunc)
  },
  // 删除广播消息
  tenantDeleteBroadcast (param, callbackFunc) {
    return rest.post(sysManage.tenantDeleteBroadcast, param, callbackFunc)
  },
  // 站长反馈查询列表
  tenantFeedbackList (param, callbackFunc) {
    return rest.get(sysManage.tenantFeedbackList, param, callbackFunc)
  },
  // 删除反馈内容
  deleteFeedback (param, callbackFunc) {
    return rest.post(sysManage.deleteFeedback, param, callbackFunc)
  }
}
