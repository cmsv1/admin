import { rest } from './index'
// 用户服务
const Users = {
  manageUser: '/v1/users/manageUser',
  getUserInfo: '/v1/users/getUserInfo',
  editUser: '/v1/users/editUser',
  editUserRemark: '/v1/users/editUserRemark',
  freezeUser: '/v1/users/freezeUser',
  updateUserLoginPassword: '/v1/users/updateUserLoginPassword',
  updateUserDrawLimit: '/v1/users/updateUserDrawLimit',
  manageTestUser: '/v1/users/manageTestUser',
  disableBankCard: '/v1/users/disableBankCard',
  updateBankCardCms: '/v1/users/updateBankCardCms',
  getBankCardListCms: '/v1/users/getBankCardListCms',
  manageMessageContent: '/v1/users/manageMessageContent',
  manageMessageList: '/v1/users/manageMessageList',
  manageSendMessage: '/v1/users/manageSendMessage',
  manageSendGradeMessage: '/v1/users/manageSendGradeMessage',
  manageDeleteMessage: '/v1/users/manageDeleteMessage',
  manageAnnouncement: '/v1/users/manageAnnouncement',
  manageAnnouncementContent: '/v1/users/manageAnnouncementContent',
  setManageAnnouncementOpen: '/v1/users/setManageAnnouncementOpen',
  editManageAnnouncementContent: '/v1/users/editManageAnnouncementContent',
  setManageAnnouncementContent: '/v1/users/setManageAnnouncementContent',
  delManageAnnouncement: '/v1/users/delManageAnnouncement',
  gradeManage: '/v1/users/gradeManage',
  banRebate: '/v1/betting/banRebate',
  getBanRebateList: '/v1/betting/getBanRebateList',
  delBanRebate: '/v1/betting/delBanRebate',
  onlineUser: '/v1/users/onlineUser',
  loginBlackIp: '/v1/users/loginBlackIp',
  setLoginBlackIp: '/v1/users/setLoginBlackIp',
  deleteLoginBlackIp: '/v1/users/deleteLoginBlackIp',
  updateLoginBlackIp: '/v1/users/updateLoginBlackIp',
  loginLog: '/v1/users/loginLog',
  inviteCodeList: '/v1/users/inviteCodeList',
  updateInviteCodeRebate: '/v1/users/updateInviteCodeRebate',
  delInviteCode: '/v1/users/delInviteCode',
  updateUserConfig: '/v1/users/updateUserConfig',
  queryFrontConfigurationInfo: '/v1/transfer/queryFrontConfigurationInfo',
  queryUsersLoginWhiteipResultList: '/v1/users/queryUsersLoginWhiteipResultList',
  addUsersLoginWhiteip: '/v1/users/addUsersLoginWhiteip',
  deleteUsersLoginWhiteip: '/v1/users/deleteUsersLoginWhiteip',
  getTestInitAmountCms: '/v1/management/tenant/getTestInitAmountCms',
  setTestInitAmount: '/v1/management/tenant/setTestInitAmount',
  queryUsersLogResultList: '/v1/users/queryUsersLogResultList', // 会员日志
  // 区域限制接口
  getTenantAreaBan: '/v1/management/tenant/getTenantAreaBan', // 获取限制访问区域
  setTenantAreaBan: '/v1/management/tenant/setTenantAreaBan', // 设置限制访问区域
  addTenantAreaWhiteip: '/v1/management/tenant/addTenantAreaWhiteip', // 添加ip白名单
  deleteTenantAreaWhiteip: '/v1/management/tenant/deleteTenantAreaWhiteip', // 删除ip白名单
  tenantAreaWhiteipList: '/v1/management/tenant/tenantAreaWhiteipList', // ip白名单列表
  queryTestAccountInfo: '/v1/users/queryTestAccountInfo', // 測試帳號 初始金額
  updateTestAccountInfo: '/v1/users/updateTestAccountInfo',
  // 可绑定银行卡设置
  queryBankInfo: '/v1/users/queryBankInfo',
  updateBatchBankInfo: '/v1/users/updateBatchBankInfo',
  exportManageUser: '/v1/users/exportManageUser', // 会员管理导出
  queryTenantConfigMultiLoginList: '/v1/users/queryTenantConfigMultiLoginList', // 获取多终端登录状态
  updateUsersTenantConfig: '/v1/users/updateUsersTenantConfig', // 设置多终端或单终端
  manageSendAgentMessage: '/v1/users/manageSendAgentMessage', // 站内信新增直属与全部
  freezeUserBatch: '/v1/users/freezeUserBatch', // 後台會員管理-會員管理-凍結/解凍會員   批量
  updateUserConfigBatch: '/v1/users/updateUserConfigBatch', // 後台會員管理-會員管理-  修改會員配置(提現次數、紅包次數、轉帳次數)
  editUserBatch: '/v1/users/editUserBatch', // 批量返点设置
  exportLoginLog: '/v1/users/exportLoginLog'
}
export default {
  // 会员管理列表
  manageUser (param, callbackFunc) {
    return rest.get(Users.manageUser, param, callbackFunc)
  },
  // 会员管理-编辑页获取数据
  getUserInfo (param, callbackFunc) {
    return rest.get(Users.getUserInfo, param, callbackFunc)
  },
  // 会员管理-修改
  editUser (param, callbackFunc) {
    return rest.post(Users.editUser, param, callbackFunc)
  },
  // 会员返点-修改
  editUserRemark (param, callbackFunc) {
    return rest.post(Users.editUserRemark, param, callbackFunc)
  },
  // 会员管理-冻结
  freezeUser (param, callbackFunc) {
    return rest.post(Users.freezeUser, param, callbackFunc)
  },
  // 会员管理-修改登录密码
  updateUserLoginPassword (param, callbackFunc) {
    return rest.post(Users.updateUserLoginPassword, param, callbackFunc)
  },
  // 会员管理-修改提现次数
  updateUserDrawLimit (param, callbackFunc) {
    return rest.post(Users.updateUserDrawLimit, param, callbackFunc)
  },
  // 测试账号管理列表
  manageTestUser (param, callbackFunc) {
    return rest.get(Users.manageTestUser, param, callbackFunc)
  },
  // 银行卡禁用或解锁
  disableBankCard (param, callbackFunc) {
    return rest.post(Users.disableBankCard, param, callbackFunc)
  },
  // 银行卡-修改
  updateBankCardCms (param, callbackFunc) {
    return rest.post(Users.updateBankCardCms, param, callbackFunc)
  },
  // 银行卡-列表获取数据
  getBankCardListCms (param, callbackFunc) {
    return rest.get(Users.getBankCardListCms, param, callbackFunc)
  },
  // 站内信-获取内容
  manageMessageContent (param, callbackFunc) {
    return rest.get(Users.manageMessageContent, param, callbackFunc)
  },
  // 站内信-列表获取数据
  manageMessageList (param, callbackFunc) {
    return rest.get(Users.manageMessageList, param, callbackFunc)
  },
  // 站内信-按会员发送站内信
  manageSendMessage (param, callbackFunc) {
    return rest.post(Users.manageSendMessage, param, callbackFunc)
  },
  // 站内信-按等级发送站内信
  manageSendGradeMessage (param, callbackFunc) {
    return rest.post(Users.manageSendGradeMessage, param, callbackFunc)
  },
  // 站内信-删除
  manageDeleteMessage (param, callbackFunc) {
    return rest.post(Users.manageDeleteMessage, param, callbackFunc)
  },
  // 公告-列表获取数据
  manageAnnouncement (param, callbackFunc) {
    return rest.get(Users.manageAnnouncement, param, callbackFunc)
  },
  // 公告-编辑页获取数据
  manageAnnouncementContent (param, callbackFunc) {
    return rest.get(Users.manageAnnouncementContent, param, callbackFunc)
  },
  // 公告-修改開放終端等級
  setManageAnnouncementOpen (param, callbackFunc) {
    return rest.post(Users.setManageAnnouncementOpen, param, callbackFunc)
  },
  // 公告-修改操作
  editManageAnnouncementContent (param, callbackFunc) {
    return rest.post(Users.editManageAnnouncementContent, param, callbackFunc)
  },
  // 公告-发布操作
  setManageAnnouncementContent (param, callbackFunc) {
    return rest.post(Users.setManageAnnouncementContent, param, callbackFunc)
  },
  // 公告-删除操作
  delManageAnnouncement (param, callbackFunc) {
    return rest.post(Users.delManageAnnouncement, param, callbackFunc)
  },
  // 等级管理
  gradeManage (param, callbackFunc) {
    return rest.get(Users.gradeManage, param, callbackFunc)
  },
  // 返点禁止-新增操作
  banRebate (param, callbackFunc) {
    return rest.post(Users.banRebate, param, callbackFunc)
  },
  // 返点禁止-列表获取数据
  getBanRebateList (param, callbackFunc) {
    return rest.get(Users.getBanRebateList, param, callbackFunc)
  },
  // 返点禁止-删除操作
  delBanRebate (param, callbackFunc) {
    return rest.post(Users.delBanRebate, param, callbackFunc)
  },
  // 在线会员
  onlineUser (param, callbackFunc) {
    return rest.get(Users.onlineUser, param, callbackFunc)
  },
  // 登录IP黑名单-列表获取数据
  loginBlackIp (param, callbackFunc) {
    return rest.get(Users.loginBlackIp, param, callbackFunc)
  },
  // 登录IP黑名单-新增
  setLoginBlackIp (param, callbackFunc) {
    return rest.post(Users.setLoginBlackIp, param, callbackFunc)
  },
  // 登录IP黑名单-删除
  deleteLoginBlackIp (param, callbackFunc) {
    return rest.post(Users.deleteLoginBlackIp, param, callbackFunc)
  },
  // 登录IP黑名单-修改
  updateLoginBlackIp (param, callbackFunc) {
    return rest.post(Users.updateLoginBlackIp, param, callbackFunc)
  },
  // 登录记录列表
  loginLog (param, callbackFunc) {
    return rest.get(Users.loginLog, param, callbackFunc)
  },
  // 邀请码管理-列表获取数据
  inviteCodeList (param, callbackFunc) {
    return rest.get(Users.inviteCodeList, param, callbackFunc)
  },
  // 邀请码管理-修改操作
  updateInviteCodeRebate (param, callbackFunc) {
    return rest.post(Users.updateInviteCodeRebate, param, callbackFunc)
  },
  // 邀请码管理-删除操作
  delInviteCode (param, callbackFunc) {
    return rest.post(Users.delInviteCode, param, callbackFunc)
  },
  updateUserConfig (param, callbackFunc) {
    return rest.post(Users.updateUserConfig, param, callbackFunc)
  },
  queryFrontConfigurationInfo (param, callbackFunc) {
    return rest.get(Users.queryFrontConfigurationInfo, param, callbackFunc)
  },
  queryUsersLoginWhiteipResultList (param, callbackFunc) {
    return rest.get(Users.queryUsersLoginWhiteipResultList, param, callbackFunc)
  },
  addUsersLoginWhiteip (param, callbackFunc) {
    return rest.post(Users.addUsersLoginWhiteip, param, callbackFunc)
  },
  deleteUsersLoginWhiteip (param, callbackFunc) {
    return rest.post(Users.deleteUsersLoginWhiteip, param, callbackFunc)
  },
  getTestInitAmountCms (param, callbackFunc) {
    return rest.get(Users.getTestInitAmountCms, param, callbackFunc)
  },
  setTestInitAmount (param, callbackFunc) {
    return rest.post(Users.setTestInitAmount, param, callbackFunc)
  },
  // 会员日志
  queryUsersLogResultList (param, callbackFunc) {
    return rest.get(Users.queryUsersLogResultList, param, callbackFunc)
  },
  // 获取限制访问区域
  getTenantAreaBan (param, callbackFunc) {
    return rest.get(Users.getTenantAreaBan, param, callbackFunc)
  },
  // 设置限制访问区域
  setTenantAreaBan (param, callbackFunc) {
    return rest.post(Users.setTenantAreaBan, param, callbackFunc)
  },
  // 添加ip白名单
  addTenantAreaWhiteip (param, callbackFunc) {
    return rest.post(Users.addTenantAreaWhiteip, param, callbackFunc)
  },
  // 删除ip白名单
  deleteTenantAreaWhiteip (param, callbackFunc) {
    return rest.post(Users.deleteTenantAreaWhiteip, param, callbackFunc)
  },
  // ip白名单列表
  tenantAreaWhiteipList (param, callbackFunc) {
    return rest.get(Users.tenantAreaWhiteipList, param, callbackFunc)
  },
  // 測試帳號初始金額 查詢
  queryTestAccountInfo (param, callbackFunc) {
    return rest.get(Users.queryTestAccountInfo, param, callbackFunc)
  },
  updateTestAccountInfo (param, callbackFunc) {
    return rest.post(Users.updateTestAccountInfo, param, callbackFunc)
  },
  // 查询可绑定银行卡设置信息
  queryBankInfo (param, callbackFunc) {
    return rest.get(Users.queryBankInfo, param, callbackFunc)
  },
  // 可绑定银行卡设置
  updateBatchBankInfo (param, callbackFunc) {
    return rest.post(Users.updateBatchBankInfo, param, callbackFunc)
  },
  exportManageUser (param, callbackFunc) {
    return rest.get(Users.exportManageUser, param, callbackFunc)
  },
  queryTenantConfigMultiLoginList (param, callbackFunc) {
    return rest.get(Users.queryTenantConfigMultiLoginList, param, callbackFunc)
  },
  updateUsersTenantConfig (param, callbackFunc) {
    return rest.post(Users.updateUsersTenantConfig, param, callbackFunc)
  },
  manageSendAgentMessage (param, callbackFunc) {
    return rest.post(Users.manageSendAgentMessage, param, callbackFunc)
  },
  freezeUserBatch (param, callbackFunc) {
    return rest.post(Users.freezeUserBatch, param, callbackFunc)
  },
  editUserBatch (param, callbackFunc) {
    return rest.post(Users.editUserBatch, param, callbackFunc)
  },
  updateUserConfigBatch (param, callbackFunc) {
    return rest.post(Users.updateUserConfigBatch, param, callbackFunc)
  },
  exportLoginLog (param, callbackFunc) {
    return rest.post(Users.exportLoginLog, param, callbackFunc)
  }
}
