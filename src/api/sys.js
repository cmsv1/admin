import { rest } from './index'
// 交易服务
const sysManage = {
  getCmsToken: '/v1/broadCast/getCmsToken',
  lottery: '/v1/management/content/getAllLotteryDataCms',
  getAllLotteryDataFront: '/v1/management/content/getAllLotteryDataFront',
  setHotLotteryData: '/v1/management/content/setHotLotteryData',
  getIntroductionListPaging: '/v1/management/content/getIntroductionListPaging',
  updateIntroduction: '/v1/management/content/updateIntroduction',
  addIntroduction: '/v1/management/content/addIntroduction',
  deleteIntroduction: '/v1/management/content/deleteIntroduction',
  findByIdIntroduction: '/v1/management/content/findByIdIntroduction',
  getAssistanceListPaging: '/v1/management/content/getAssistanceListPaging',
  deleteAssistance: '/v1/management/content/deleteAssistance',
  addAssistance: '/v1/management/content/addAssistance',
  updateAssistance: '/v1/management/content/updateAssistance',
  findByIdAssistance: '/v1/management/content/findByIdAssistance',
  getTenantSupportData: '/v1/management/content/getTenantSupportData',
  setTenantSupport: '/v1/management/content/setTenantSupport',
  getTenantApp: '/v1/management/tenant/getTenantApp',
  updateTenantApp: '/v1/management/tenant/updateTenantApp',
  imageBannerHomePcCms: '/v1/management/content/imageBannerHomePcCms',
  deleteImageBannerHomePc: '/v1/management/content/deleteImageBannerHomePc',
  updateImageBannerImgHomePc: '/v1/management/content/updateImageBannerImgHomePc',
  findLogoList: '/v1/management/content/findLogoList',
  getImageConfig: '/v1/management/content/getImageConfig',
  imageBannerHomePcMobileCms: '/v1/management/content/imageBannerHomePcMobileCms',
  deleteImageBannerHomePcMobile: '/v1/management/content/deleteImageBannerHomePcMobile',
  saveImageBannerImgHomePcMobile: '/v1/management/content/saveImageBannerImgHomePcMobile',
  updateImageBannerLinkHomePcMobile: '/v1/management/content/updateImageBannerLinkHomePcMobile',
  tenantUrlList: '/v1/management/tenant/tenantUrlList',
  tenantUrl: '/v1/management/tenant/tenantUrl',
  tenantAddUrl: '/v1/management/tenant/tenantAddUrl',
  tenantUpdateUrl: '/v1/management/tenant/tenantUpdateUrl',
  tenantDeleteUrl: '/v1/management/tenant/tenantDeleteUrl',
  tenantIpList: '/v1/management/tenant/tenantIpList',
  tenantIp: '/v1/management/tenant/tenantIp',
  addTenantIp: '/v1/management/tenant/tenantAddIp',
  updateIp: '/v1/management/tenant/tenantUpdateIp',
  deleteIp: '/v1/management/tenant/tenantDeleteIp',
  tenantLogList: '/v1/management/manager/tenantLogList',
  xUnreadAnnouncement: '/v1/management/manager/xUnreadAnnouncement',
  imageBannerLotteryCms: '/v1/management/content/imageBannerLotteryCms',
  deleteImageBannerLottery: '/v1/management/content/deleteImageBannerLottery',
  saveImageBannerImgLottery: '/v1/management/content/saveImageBannerImgLottery',
  updateImageBannerLinkLottery: '/v1/management/content/updateImageBannerLinkLottery',
  tokenList: '/v1/management/tenant/tokenList',
  addToken: '/v1/management/tenant/addToken',
  importToken: '/v1/management/tenant/importToken',
  deleteToken: '/v1/management/tenant/deleteToken',
  removeToken: '/v1/management/tenant/removeToken',
  updateTokenState: '/v1/management/tenant/updateTokenState',
  generateTokenKey: '/v1/management/tenant/generateTokenKey',
  ringtone: '/v1/management/tenant/ringtone',
  ringtoneList: '/v1/management/content/ringtoneList',
  updateRingtone: '/v1/management/tenant/updateRingtone',
  deleteRingtone: '/v1/management/tenant/deleteRingtone',
  uploadRingtone: '/v1/management/tenant/uploadRingtone',
  xAnnouncementList: '/v1/management/manager/xAnnouncementList',
  xAnnouncement: '/v1/management/manager/xAnnouncement',
  updatePersonalDetails: '/v1/management/manager/updatePersonalDetails',
  getImageConfigSystem: '/v1/management/content/getImageConfigSystem',
  logout: '/v1/management/manager/logout',
  getAvatarList: '/v1/management/content/avatarList',
  getFilterWordList: '/v1/chat/getFilterWordList',
  getFilterWord: '/v1/chat/getFilterWord',
  addFilterWord: '/v1/chat/addFilterWord',
  updateFilterWord: '/v1/chat/updateFilterWord',
  deleteFilterWord: '/v1/chat/deleteFilterWord',
  getBanList: '/v1/chat/getBanList',
  getBan: '/v1/chat/getBan',
  addBan: '/v1/chat/addBan',
  updateBan: '/v1/chat/updateBan',
  deleteBan: '/v1/chat/deleteBan',
  getOnlineMemberList: '/v1/chat/getOnlineMemberList',
  getMessageList: '/v1/chat/getMessageList',
  addMessage: '/v1/chat/addMessage',
  deleteMessage: '/v1/chat/deleteMessage',
  getFilteredMessageBlockList: '/v1/chat/getFilteredMessageBlockList',
  deleteFilteredMessageBlock: '/v1/chat/deleteFilteredMessageBlock',
  getFilteredMessageManualList: '/v1/chat/getFilteredMessageManualList',
  updateFilteredMessageManualState: '/v1/chat/updateFilteredMessageManualState',
  deleteFilteredMessageManual: '/v1/chat/deleteFilteredMessageManual',
  updateMessageState: '/v1/chat/updateMessageState',
  getRoleList: '/v1/chat/getRoleList',
  getRole: '/v1/chat/getRole',
  addRole: '/v1/chat/addRole',
  updateRole: '/v1/chat/updateRole',
  deleteRole: '/v1/chat/deleteRole',
  getRoomList: '/v1/chat/getRoomList',
  updateRoom: '/v1/chat/updateRoom',
  getRoom: '/v1/chat/getRoom',
  addRoom: '/v1/chat/addRoom',
  updateRoomBanned: '/v1/chat/updateRoomBanned',
  updateRoomList: '/v1/chat/updateRoomList',
  gameSort: '/v1/game/gameSort',
  gameEnumBack: '/v1/game/gameEnumBack',
  gameManagement: '/v1/game/gameManagement',
  getUrlList: '/v1/security/getUrlList',
  addUrl: '/v1/security/addUrl',
  deleteUrl: '/v1/security/deleteUrl',
  getPackage: '/v1/security/getPackage',
  getNonSslUrlList: '/v1/security/getNonSslUrlList',
  playSchemeList: '/v1/chat/playSchemeList',
  addPlayScheme: '/v1/chat/addPlayScheme',
  playList: '/v1/chat/playList',
  addPlay: '/v1/chat/addPlay',
  getAllChatRoomInfo: '/v1/chat/getAllChatRoomInfo',
  getAllMiliaoRoomInfo: '/v1/chat/getAllMiliaoRoomInfo',
  updatePlayStatus: '/v1/chat/updatePlayStatus',
  deletePlay: '/v1/chat/deletePlay',
  playSchemeListInPlay: '/v1/chat/playSchemeListInPlay',
  getPushPlayList: '/v1/chat/getPushPlayList',
  updatePlayScheme: '/v1/chat/updatePlayScheme',
  updatePlay: '/v1/chat/updatePlay',
  deletePlayScheme: '/v1/chat/deletePlayScheme',
  runingPlayList: '/v1/chat/runingPlayList',
  updatePlayListStatus: '/v1/chat/updatePlayListStatus',
  getChatRoomName: '/v1/chat/getChatRoomName',
  getMiliaoGroupName: '/v1/chat/getMiliaoGroupName',
  updateBannerHomeSort: '/v1/management/content/updateBannerHomeSort', // 首页轮播排序
  sysCancelBettingList: '/v1/betting/sysCancelBettingList',
  sysCancelBetting: '/v1/betting/sysCancelBetting',
  getPlatformLottery: '/v1/lottery/tenantLottery/getPlatformLottery', // 获取平台彩种列表
  deleteRoom: '/v1/chat/deleteRoom', // 刪除聊天室
  userInfo: '/v1/management/manager/userInfo' // 個人訊息
}
export default {
  // 获取后台jessionid
  getCmsToken (param, callbackFunc) {
    return rest.get(sysManage.getCmsToken, param, callbackFunc)
  },
  // 热门彩种列表
  lottery (param, callbackFunc) {
    return rest.get(sysManage.lottery, param, callbackFunc)
  },
  // 设置热门彩种
  setHotLotteryData (param, callbackFunc) {
    return rest.post(sysManage.setHotLotteryData, param, callbackFunc)
  },
  // 关于我们列表
  getIntroductionListPaging (param, callbackFunc) {
    return rest.get(sysManage.getIntroductionListPaging, param, callbackFunc)
  },
  // 关于我们修改
  updateIntroduction (param, callbackFunc) {
    return rest.post(sysManage.updateIntroduction, param, callbackFunc)
  },
  // 关于我们添加
  addIntroduction (param, callbackFunc) {
    return rest.post(sysManage.addIntroduction, param, callbackFunc)
  },
  // 关于我们删除
  deleteIntroduction (param, callbackFunc) {
    return rest.post(sysManage.deleteIntroduction, param, callbackFunc)
  },
  // 关于我们根据id获取数据
  findByIdIntroduction (param, callbackFunc) {
    return rest.get(sysManage.findByIdIntroduction, param, callbackFunc)
  },
  // 帮助指南列表
  getAssistanceListPaging (param, callbackFunc) {
    return rest.get(sysManage.getAssistanceListPaging, param, callbackFunc)
  },
  // 帮助指南删除
  deleteAssistance (param, callbackFunc) {
    return rest.post(sysManage.deleteAssistance, param, callbackFunc)
  },
  // 帮助指南添加
  addAssistance (param, callbackFunc) {
    return rest.post(sysManage.addAssistance, param, callbackFunc)
  },
  // 帮助指南修改
  updateAssistance (param, callbackFunc) {
    return rest.post(sysManage.updateAssistance, param, callbackFunc)
  },
  // 帮助指南根据id获取数据
  findByIdAssistance (param, callbackFunc) {
    return rest.get(sysManage.findByIdAssistance, param, callbackFunc)
  },
  // 在线客服获取数据
  getTenantSupportData (param, callbackFunc) {
    return rest.get(sysManage.getTenantSupportData, param, callbackFunc)
  },
  // 在线客服保存
  setTenantSupport (param, callbackFunc) {
    return rest.post(sysManage.setTenantSupport, param, callbackFunc)
  },
  // APP管理获取信息
  getTenantApp (param, callbackFunc) {
    return rest.get(sysManage.getTenantApp, param, callbackFunc)
  },
  // APP管理保存
  updateTenantApp (param, callbackFunc) {
    return rest.create(sysManage.updateTenantApp, param, callbackFunc)
  },
  // 轮播图片查询
  imageBannerHomePcCms (param, callbackFunc) {
    return rest.post(sysManage.imageBannerHomePcCms, param, callbackFunc)
  },
  // 轮播图片更新
  updateImageBannerImgHomePc (param, callbackFunc) {
    return rest.post(sysManage.updateImageBannerImgHomePc, param, callbackFunc)
  },
  // Logo管理列表
  findLogoList (param, callbackFunc) {
    return rest.get(sysManage.findLogoList, param, callbackFunc)
  },
  // Logo管理获取图片大小限制
  getImageConfig (param, callbackFunc) {
    return rest.get(sysManage.getImageConfig, param, callbackFunc)
  },
  // 轮播图片获取数据
  imageBannerHomePcMobileCms (param, callbackFunc) {
    return rest.get(sysManage.imageBannerHomePcMobileCms, param, callbackFunc)
  },
  // 轮播图片删除数据
  deleteImageBannerHomePcMobile (param, callbackFunc) {
    return rest.post(sysManage.deleteImageBannerHomePcMobile, param, callbackFunc)
  },
  // 轮播图片上传图片
  saveImageBannerImgHomePcMobile (param, callbackFunc) {
    return rest.create(sysManage.saveImageBannerImgHomePcMobile, param, callbackFunc)
  },
  // 轮播图片保存数据
  updateImageBannerLinkHomePcMobile (param, callbackFunc) {
    return rest.create(sysManage.updateImageBannerLinkHomePcMobile, param, callbackFunc)
  },
  // 域名管理列表
  tenantUrlList (param, callbackFunc) {
    return rest.get(sysManage.tenantUrlList, param, callbackFunc)
  },
  // 域名管理编辑页获取数据
  tenantUrl (param, callbackFunc) {
    return rest.get(sysManage.tenantUrl, param, callbackFunc)
  },
  // 域名管理添加数据
  tenantAddUrl (param, callbackFunc) {
    return rest.post(sysManage.tenantAddUrl, param, callbackFunc)
  },
  // 域名管理更新数据
  tenantUpdateUrl (param, callbackFunc) {
    return rest.post(sysManage.tenantUpdateUrl, param, callbackFunc)
  },
  // 域名管理删除数据
  tenantDeleteUrl (param, callbackFunc) {
    return rest.post(sysManage.tenantDeleteUrl, param, callbackFunc)
  },
  // IP管理列表
  tenantIpList (param, callbackFunc) {
    return rest.get(sysManage.tenantIpList, param, callbackFunc)
  },
  // IP管理编辑页获取数据
  tenantIp (param, callbackFunc) {
    return rest.get(sysManage.tenantIp, param, callbackFunc)
  },
  // IP管理添加数据
  addTenantIp (param, callbackFunc) {
    return rest.post(sysManage.addTenantIp, param, callbackFunc)
  },
  // IP管理修改数据
  updateIp (param, callbackFunc) {
    return rest.post(sysManage.updateIp, param, callbackFunc)
  },
  // IP管理删除数据
  deleteIp (param, callbackFunc) {
    return rest.post(sysManage.deleteIp, param, callbackFunc)
  },
  // 管理员日志
  tenantLogList (param, callbackFunc) {
    return rest.get(sysManage.tenantLogList, param, callbackFunc)
  },
  // 未读公告数量
  xUnreadAnnouncement (param, callbackFunc) {
    return rest.get(sysManage.xUnreadAnnouncement, param, callbackFunc)
  },
  // 平台管理 - 大厅轮播
  imageBannerLotteryCms (param, callbackFunc) {
    return rest.get(sysManage.imageBannerLotteryCms, param, callbackFunc)
  },
  // 平台管理 - 大厅轮播图片删除
  deleteImageBannerLottery (param, callbackFunc) {
    return rest.post(sysManage.deleteImageBannerLottery, param, callbackFunc)
  },
  // 平台管理 - 大厅轮播图片上传
  saveImageBannerImgLottery (param, callbackFunc) {
    return rest.create(sysManage.saveImageBannerImgLottery, param, callbackFunc)
  },
  // 平台管理 - 大厅轮播图片保存
  updateImageBannerLinkLottery (param, callbackFunc) {
    return rest.post(sysManage.updateImageBannerLinkLottery, param, callbackFunc)
  },
  // 共享令牌列表
  tokenList (param, callbackFunc) {
    return rest.get(sysManage.tokenList, param, callbackFunc)
  },
  // 共享令牌-添加操作
  addToken (param, callbackFunc) {
    return rest.post(sysManage.addToken, param, callbackFunc)
  },
  // 共享令牌-导入
  importToken (param, callbackFunc) {
    return rest.post(sysManage.importToken, param, callbackFunc)
  },
  // 共享令牌-删除
  deleteToken (param, callbackFunc) {
    return rest.post(sysManage.deleteToken, param, callbackFunc)
  },
  // 共享令牌-解绑
  removeToken (param, callbackFunc) {
    return rest.post(sysManage.removeToken, param, callbackFunc)
  },
  // 共享令牌-更新状态
  updateTokenState (param, callbackFunc) {
    return rest.post(sysManage.updateTokenState, param, callbackFunc)
  },
  // 共享令牌-生成密钥
  generateTokenKey (param, callbackFunc) {
    return rest.post(sysManage.generateTokenKey, param, callbackFunc)
  },
  // 提示音-获取当前选中数据
  ringtone (param, callbackFunc) {
    return rest.get(sysManage.ringtone, param, callbackFunc)
  },
  // 提示音-列表获取数据
  ringtoneList (param, callbackFunc) {
    return rest.get(sysManage.ringtoneList, param, callbackFunc)
  },
  // 提示音-更新操作
  updateRingtone (param, callbackFunc) {
    return rest.post(sysManage.updateRingtone, param, callbackFunc)
  },
  deleteRingtone (param, callbackFunc) {
    return rest.post(sysManage.deleteRingtone, param, callbackFunc)
  },
  uploadRingtone (param, callbackFunc) {
    return rest.create(sysManage.uploadRingtone, param, callbackFunc)
  },
  // 后台首页公告列表
  xAnnouncementList (param, callbackFunc) {
    return rest.get(sysManage.xAnnouncementList, param, callbackFunc)
  },
  // 后台首页-公告信息
  xAnnouncement (param, callbackFunc) {
    return rest.get(sysManage.xAnnouncement, param, callbackFunc)
  },
  // 后台账号修改个人信息
  updatePersonalDetails (param, callbackFunc) {
    return rest.create(sysManage.updatePersonalDetails, param, callbackFunc)
  },
  // 平台管理-大厅轮播获取图片配置
  getImageConfigSystem (param, callbackFunc) {
    return rest.get(sysManage.getImageConfigSystem, param, callbackFunc)
  },
  // 退出登录
  logout (param, callbackFunc) {
    return rest.post(sysManage.logout, param, callbackFunc)
  },
  // 頭像清單
  getAvatarList (param, callbackFunc) {
    return rest.get(sysManage.getAvatarList, param, callbackFunc)
  },
  // 过滤设置
  getFilterWordList (param, callbackFunc) {
    return rest.get(sysManage.getFilterWordList, param, callbackFunc)
  },
  // 獲取过滤词
  getFilterWord (param, callbackFunc) {
    return rest.get(sysManage.getFilterWord, param, callbackFunc)
  },
  // 过滤设置增加
  addFilterWord (param, callbackFunc) {
    return rest.post(sysManage.addFilterWord, param, callbackFunc)
  },
  // 过滤设置更新
  updateFilterWord (param, callbackFunc) {
    return rest.post(sysManage.updateFilterWord, param, callbackFunc)
  },
  deleteFilterWord (param, callbackFunc) {
    return rest.post(sysManage.deleteFilterWord, param, callbackFunc)
  },
  // 禁言列表
  getBanList (param, callbackFunc) {
    return rest.get(sysManage.getBanList, param, callbackFunc)
  },
  // 獲取禁言
  getBan (param, callbackFunc) {
    return rest.get(sysManage.getBan, param, callbackFunc)
  },
  // 新增禁言
  addBan (param, callbackFunc) {
    return rest.post(sysManage.addBan, param, callbackFunc)
  },
  // 刪除禁言
  deleteBan (param, callbackFunc) {
    return rest.post(sysManage.deleteBan, param, callbackFunc)
  },
  // 更新禁言
  updateBan (param, callbackFunc) {
    return rest.post(sysManage.updateBan, param, callbackFunc)
  },
  // 在线用户
  getOnlineMemberList (param, callbackFunc) {
    return rest.get(sysManage.getOnlineMemberList, param, callbackFunc)
  },
  // 聊天记录列表
  getMessageList (param, callbackFunc) {
    return rest.get(sysManage.getMessageList, param, callbackFunc)
  },
  // 前台聊天记录列表
  addMessage (param, callbackFunc) {
    return rest.post(sysManage.addMessage, param, callbackFunc)
  },
  // 新增聊天记录
  deleteMessage (param, callbackFunc) {
    return rest.post(sysManage.deleteMessage, param, callbackFunc)
  },
  // 撤回聊天记录
  updateMessageState (param, callbackFunc) {
    return rest.post(sysManage.updateMessageState, param, callbackFunc)
  },
  // 屏蔽记录列表
  getFilteredMessageBlockList (param, callbackFunc) {
    return rest.get(sysManage.getFilteredMessageBlockList, param, callbackFunc)
  },
  // 刪除屏蔽记录
  deleteFilteredMessageBlock (param, callbackFunc) {
    return rest.post(sysManage.deleteFilteredMessageBlock, param, callbackFunc)
  },
  // 人工审核列表
  getFilteredMessageManualList (param, callbackFunc) {
    return rest.get(sysManage.getFilteredMessageManualList, param, callbackFunc)
  },
  // 更新人工审核狀態
  updateFilteredMessageManualState (param, callbackFunc) {
    return rest.post(sysManage.updateFilteredMessageManualState, param, callbackFunc)
  },
  // 刪除人工审核
  deleteFilteredMessageManual (param, callbackFunc) {
    return rest.post(sysManage.deleteFilteredMessageManual, param, callbackFunc)
  },
  // 角色列表
  getRoleList (param, callbackFunc) {
    return rest.get(sysManage.getRoleList, param, callbackFunc)
  },
  // 獲取角色
  getRole (param, callbackFunc) {
    return rest.get(sysManage.getRole, param, callbackFunc)
  },
  // 新增角色
  addRole (param, callbackFunc) {
    return rest.post(sysManage.addRole, param, callbackFunc)
  },
  // 更新角色
  updateRole (param, callbackFunc) {
    return rest.post(sysManage.updateRole, param, callbackFunc)
  },
  // 刪除角色
  deleteRole (param, callbackFunc) {
    return rest.post(sysManage.deleteRole, param, callbackFunc)
  },
  // 後台獲取房間列表
  getRoomList (param, callbackFunc) {
    return rest.get(sysManage.getRoomList, param, callbackFunc)
  },
  // 更新房間
  updateRoom (param, callbackFunc) {
    return rest.create(sysManage.updateRoom, param, callbackFunc)
  },
  // 後台獲取房間设置
  getRoom (param, callbackFunc) {
    return rest.get(sysManage.getRoom, param, callbackFunc)
  },
  // 新增房間
  addRoom (param, callbackFunc) {
    return rest.create(sysManage.addRoom, param, callbackFunc)
  },
  // 更新全体禁言狀態
  updateRoomBanned (param, callbackFunc) {
    return rest.post(sysManage.updateRoomBanned, param, callbackFunc)
  },
  // 更新房间排序及开放等级
  updateRoomList (param, callbackFunc) {
    return rest.post(sysManage.updateRoomList, param, callbackFunc)
  },
  // https域名列表
  gameSort (param, callbackFunc) {
    return rest.post(sysManage.gameSort, param, callbackFunc)
  },
  gameEnumBack (param, callbackFunc) {
    return rest.get(sysManage.gameEnumBack, param, callbackFunc)
  },
  gameManagement (param, callbackFunc) {
    return rest.get(sysManage.gameManagement, param, callbackFunc)
  },
  getUrlList (param, callbackFunc) {
    return rest.get(sysManage.getUrlList, param, callbackFunc)
  },
  // 增加https域名
  addUrl (param, callbackFunc) {
    return rest.post(sysManage.addUrl, param, callbackFunc)
  },
  // 刪除https域名
  deleteUrl (param, callbackFunc) {
    return rest.post(sysManage.deleteUrl, param, callbackFunc)
  },
  // 站長套餐按鈕
  getPackage (param, callbackFunc) {
    return rest.get(sysManage.getPackage, param, callbackFunc)
  },
  // 可加https域名列表
  getNonSslUrlList (param, callbackFunc) {
    return rest.get(sysManage.getNonSslUrlList, param, callbackFunc)
  },
  // 計畫管理创建计划
  playSchemeList (param, callbackFunc) {
    return rest.get(sysManage.playSchemeList, param, callbackFunc)
  },
  // 計畫管理创建计划
  addPlayScheme (param, callbackFunc) {
    return rest.post(sysManage.addPlayScheme, param, callbackFunc)
  },
  // 計畫管理推送计划 列表
  playList (param, callbackFunc) {
    return rest.get(sysManage.playList, param, callbackFunc)
  },
  // 計畫管理推送计划 新增
  addPlay (param, callbackFunc) {
    return rest.post(sysManage.addPlay, param, callbackFunc)
  },
  // 获取当前站所有的聊天室
  getAllChatRoomInfo (param, callbackFunc) {
    return rest.get(sysManage.getAllChatRoomInfo, param, callbackFunc)
  },
  // 获取当前站所有的密聊
  getAllMiliaoRoomInfo (param, callbackFunc) {
    return rest.get(sysManage.getAllMiliaoRoomInfo, param, callbackFunc)
  },
  // 計畫管理推送计划 更換狀態
  updatePlayStatus (param, callbackFunc) {
    return rest.post(sysManage.updatePlayStatus, param, callbackFunc)
  },
  // 計畫管理推送计划 刪除
  deletePlay (param, callbackFunc) {
    return rest.post(sysManage.deletePlay, param, callbackFunc)
  },
  // 推送計畫彈窗的方案
  playSchemeListInPlay (param, callbackFunc) {
    return rest.get(sysManage.playSchemeListInPlay, param, callbackFunc)
  },
  // 推送 歷史紀錄
  getPushPlayList (param, callbackFunc) {
    return rest.get(sysManage.getPushPlayList, param, callbackFunc)
  },
  updatePlayScheme (param, callbackFunc) {
    return rest.post(sysManage.updatePlayScheme, param, callbackFunc)
  },
  // 推送計畫彈窗的修改
  updatePlay (param, callbackFunc) {
    return rest.post(sysManage.updatePlay, param, callbackFunc)
  },
  // 創建計畫彈窗的刪除
  deletePlayScheme (param, callbackFunc) {
    return rest.post(sysManage.deletePlayScheme, param, callbackFunc)
  },
  // 获取当前用户的计划推送列表
  runingPlayList (param) {
    return rest.get(sysManage.runingPlayList, param)
  },
  updatePlayListStatus (param) {
    return rest.post(sysManage.updatePlayListStatus, param)
  },
  getChatRoomName (param) {
    return rest.get(sysManage.getChatRoomName, param)
  },
  getMiliaoGroupName (param) {
    return rest.get(sysManage.getMiliaoGroupName, param)
  },
  // 首页轮播排序
  updateBannerHomeSort (param, callbackFunc) {
    return rest.post(sysManage.updateBannerHomeSort, param, callbackFunc)
  },
  // 系统撤单列表
  sysCancelBettingList (param, callbackFunc) {
    return rest.get(sysManage.sysCancelBettingList, param, callbackFunc)
  },
  // 系统撤单操作
  sysCancelBetting (param, callbackFunc) {
    return rest.post(sysManage.sysCancelBetting, param, callbackFunc)
  },
  getPlatformLottery (param) {
    return rest.get(sysManage.getPlatformLottery, param)
  },
  deleteRoom (param, callbackFunc) {
    return rest.post(sysManage.deleteRoom, param, callbackFunc)
  },
  userInfo (param, callbackFunc) {
    return rest.get(sysManage.userInfo, param, callbackFunc)
  }
}
