import { rest } from './index'
// 报表服务
const report = {
  userReport: '/v1/report/userReport',
  userHistory: '/v1/report/userReport/userHistory',
  gradeReport: '/v1/report/gradeReport',
  lotteryReport: '/v1/report/lotteryReport',
  sourceReport: '/v1/report/sourceReport',
  tenantReport: '/v1/report/tenantReport',
  firstinReport: '/v1/report/firstinReport',
  agentReport: '/v1/report/agentReport',
  setAgents: '/v1/report/agentReport/setAgents',
  getAgents: '/v1/report/agentReport/getAgents',
  platformReport: '/v1/report/platformReport',
  tenant: '/v1/report/platformReport/tenant',
  month: '/v1/report/platformReport/month',
  lottery: '/v1/report/platformReport/lottery',
  getAllLotteryDataSystemCms: '/v1/management/content/getAllLotteryDataSystemCms',
  reportPaymentList: '/v1/transaction/reportPaymentList',
  reportManualRecordList: '/v1/transaction/reportManualRecordList',
  payingReport: '/v1/report/payingReport',
  findSumByTenantCodeCms: '/v1/balance/findSumByTenantCodeCms',
  profitDetail: '/v1/report/userReport/profitDetail',
  // 流量报表
  flowReport: '/v1/report/flowReport',
  host: '/v1/report/flowReport/host',
  getSiteLottery: '/v1/lottery/tenantLottery/getSiteLottery'// 获取站长级彩种列表
}
export default {
  // 会员报表
  userReport (param, callbackFunc) {
    return rest.get(report.userReport, param, callbackFunc)
  },
  // 会员报表-历史数据
  userHistory (param, callbackFunc) {
    return rest.get(report.userHistory, param, callbackFunc)
  },
  // 等级报表
  gradeReport (param, callbackFunc) {
    return rest.get(report.gradeReport, param, callbackFunc)
  },
  // 彩种报表
  lotteryReport (param, callbackFunc) {
    return rest.get(report.lotteryReport, param, callbackFunc)
  },
  // 终端报表
  sourceReport (param, callbackFunc) {
    return rest.get(report.sourceReport, param, callbackFunc)
  },
  // 综合报表
  tenantReport (param, callbackFunc) {
    return rest.get(report.tenantReport, param, callbackFunc)
  },
  // 首充报表
  firstinReport (param, callbackFunc) {
    return rest.get(report.firstinReport, param, callbackFunc)
  },
  // 代理报表
  agentReport (param, callbackFunc) {
    return rest.get(report.agentReport, param, callbackFunc)
  },
  // 代理报表-保存代理
  setAgents (param, callbackFunc) {
    return rest.post(report.setAgents, param, callbackFunc)
  },
  // 代理报表-获取代理数据
  getAgents (param, callbackFunc) {
    return rest.get(report.getAgents, param, callbackFunc)
  },
  // 平台报表
  platformReport (param, callbackFunc) {
    return rest.get(report.platformReport, param, callbackFunc)
  },
  // 平台报表-站长报表
  tenant (param, callbackFunc) {
    return rest.get(report.tenant, param, callbackFunc)
  },
  // 平台报表-月报表
  month (param, callbackFunc) {
    return rest.get(report.month, param, callbackFunc)
  },
  // 平台报表-彩种报表
  lottery (param, callbackFunc) {
    return rest.get(report.lottery, param, callbackFunc)
  },
  // 平台管理-获取彩种数据
  getAllLotteryDataSystemCms (param, callbackFunc) {
    return rest.get(report.getAllLotteryDataSystemCms, param, callbackFunc)
  },
  // 综合报表-第三方充值明细
  reportPaymentList (param, callbackFunc) {
    return rest.get(report.reportPaymentList, param, callbackFunc)
  },
  // 综合报表-人工存款充值明细
  reportManualRecordList (param, callbackFunc) {
    return rest.get(report.reportManualRecordList, param, callbackFunc)
  },
  // 支付报表
  payingReport (param, callbackFunc) {
    return rest.get(report.payingReport, param, callbackFunc)
  },
  // 综合报表-会员余额
  findSumByTenantCodeCms (param, callbackFunc) {
    return rest.get(report.findSumByTenantCodeCms, param, callbackFunc)
  },
  profitDetail (param, callbackFunc) {
    return rest.get(report.profitDetail, param, callbackFunc)
  },
  // 流量报表
  flowReport (param, callbackFunc) {
    return rest.get(report.flowReport, param, callbackFunc)
  },
  // 域名报表
  host (param, callbackFunc) {
    return rest.get(report.host, param, callbackFunc)
  },
  // 站长级彩种列表
  getSiteLottery (param, callbackFunc) {
    return rest.get(report.getSiteLottery, param, callbackFunc)
  }
}
