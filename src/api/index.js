// import Vue from 'vue'
// import VueResource from 'vue-resource'
import axios from 'axios'
import qs from 'qs'
import md5 from 'js-md5/build/md5.min'
import {MessageBox, Message} from 'element-ui'
import router from '../router'
const api = '/v1/management/manager'
const Api = {
  login: api + '/login',
  getNavList: '/v1/management/manager/xPermission',
  getNavTimestamp: '/v1/management/manager/xPermissionTimestamp',
  postBanner: '/v1/management/content/saveImageBannerImgHomePcMobile',
  postPlatformBanner: '/v1/management/content/saveImageBannerImgLottery',
  tenantCmsConfig: '/v1/management/tenant/tenantCmsConfig',
  checkMaintain: '/v1/management/tenant/check'
}

axios.defaults.timeout = 60000
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8'
// axios.defaults.headers.baseURL = 'http://system.dfcdn39.com:8001'

// POST传参序列化
axios.interceptors.request.use((config) => {
  return config
}, (error) => {
  // console.log('错误的传参')
  return Promise.reject(error)
})

// 返回状态判断
window.tipLock = false
axios.interceptors.response.use((res) => {
  // console.log(res.config.url, res.data.data)
  if (res.status !== 200) {
    return Promise.reject(res)
  }
  let code = res.data.code
  localStorage.setItem('maintainData', '')
  localStorage.setItem('WebsiteClosed', '')
  switch (code) {
    case 0:
      if (localStorage.getItem('isLogin')) {
        if (!window.tipLock) {
          window.tipLock = true
          MessageBox.confirm('由于您长时间未操作，已自动退出，需要重新登录', '提示', {
            confirmButtonText: '确定',
            type: 'error',
            closeOnClickModal: false,
            closeOnPressEscape: false,
            showClose: false,
            showCancelButton: false
          }).then(() => {
            window.tipLock = false
            sessionStorage.clear()
            localStorage.removeItem('siteInfo')
            localStorage.removeItem('isLogin')
            router.replace('/login')
            // window.document.location.href = '/login'
          })
        }
      }
      return res
    case 1:
      return res
    case -6:
      let isLogin = document.location.pathname === '/login'
      if (!isLogin && !window.tipLock) {
        window.tipLock = true
        MessageBox.confirm('后台IP未绑定无法操作，请检查IP', '提示', {
          confirmButtonText: '确定',
          type: 'error',
          closeOnClickModal: false,
          closeOnPressEscape: false,
          showClose: false,
          showCancelButton: false
        }).then(() => {
          window.tipLock = false
        })
      }
      return res
    case -1:
      Message.error(res.data.msg)
      return res
    case -2:
      Message.error(res.data.msg)
      return res
      // if (!window.tipLock) {
      //   window.tipLock = true
      //   MessageBox.confirm(res.data.msg, '提示', {
      //     confirmButtonText: '确定',
      //     type: 'error',
      //     showClose: false,
      //     showCancelButton: false
      //   }).then(() => {
      //     window.tipLock = false
      //     router.replace('/')
      //   })
      // }
      // return res
    case -7:
      errorRes1(res)
      return res
    case -17:
    case -9:
      if (!window.tipLock) {
        window.tipLock = true
        loginOut(res, code)
      }
      return res
    case -11: //
      if (!window.tipLock) {
        window.tipLock = true
        MessageBox.confirm(res.data.msg, '提示', {
          confirmButtonText: '确定',
          type: 'error',
          closeOnClickModal: false,
          closeOnPressEscape: false,
          showClose: false,
          showCancelButton: false
        }).then(() => {
          // window.location.href = '/'
          window.tipLock = false
          window.tipLock = false
          // router.replace('/login')
        })
      }
      return res
    case -12: // 网站关闭
      localStorage.setItem('WebsiteClosed', JSON.stringify(res.data))
      if (location.pathname === '/websiteClosed') return
      location.href = '/websiteClosed'
      return res
    case -16:
      Message.error(res.data.msg)
      return res
    default:
      if (!window.tipLock) {
        window.tipLock = true
        MessageBox.confirm(res.data.msg, '提示', {
          confirmButtonText: '确定',
          type: 'error',
          closeOnClickModal: false,
          closeOnPressEscape: false,
          showClose: false,
          showCancelButton: false
        }).then(() => {
          // localStorage.setItem('isLogin', 0)
          window.tipLock = false
          // router.replace('/login')
        })
      }
      return res
  }
}, (error) => {
  if (!window.tipLock) {
    window.tipLock = true
    MessageBox.confirm('网络异常', '提示', {
      confirmButtonText: '确定',
      type: 'error',
      showClose: false,
      showCancelButton: false
    }).then(() => {
      window.tipLock = false
    })
  }
  return Promise.reject(error)
})
export const rest = {
  create (url, params, callbackFunc) {
    return new Promise((resolve, reject) => {
      axios.post(url, params, {
        method: 'post',
        headers: {'Content-Type': 'multipart/form-data'}
      })
        .then(response => {
          if (response.data.code === 1) {
            resolve(response.data)
          } else {
            if (typeof callbackFunc === 'function') {
              callbackFunc(response.data)
            }
          }
        }, err => {
          if (typeof callbackFunc === 'function') {
            callbackFunc()
          }
          reject(err)
        })
        .catch((error) => {
          if (typeof callbackFunc === 'function') {
            callbackFunc()
          }
          reject(error)
        })
    })
  },
  post: (url, params, callbackFunc) => {
    return new Promise((resolve, reject) => {
      axios.post(url, qs.stringify(params))
        .then(response => {
          if (response.data.code === 1) {
            resolve(response.data)
          } else {
            if (typeof callbackFunc === 'function') {
              callbackFunc(response.data)
            }
          }
        }, err => {
          if (typeof callbackFunc === 'function') {
            callbackFunc()
          }
          reject(err)
        })
        .catch((error) => {
          if (typeof callbackFunc === 'function') {
            callbackFunc()
          }
          reject(error)
        })
    })
  },
  get: (url, params, callbackFunc) => {
    return new Promise((resolve, reject) => {
      let param = params || {}
      let paramData = {}
      for (let item in param) {
        if (param[item] === null || param[item] === undefined) {
          continue
        }
        paramData[item] = encodeURIComponent(param[item])
      }
      let uri = getStrUrl(url, paramData)
      // console.log(uri)
      axios.get(uri)
        .then(function (res) {
          // handle success
          if (res.data.code === 1) {
            resolve(res.data)
          } else {
            if (typeof callbackFunc === 'function') {
              callbackFunc(res.data)
            }
          }
        })
        .catch(function (error) {
          if (typeof callbackFunc === 'function') {
            callbackFunc()
          }
          reject(error)
        })
    })
  }
}
export function getStrUrl (url, params) {
  if (params) {
    var uri = '?'
    for (let item in params) {
      uri = uri + item + '=' + params[item] + '&'
    }
    return url + uri
  }
  return url
}
function loginOut (res, code) {
  MessageBox.confirm(res.data.msg, '提示', {
    confirmButtonText: '确定',
    type: 'error',
    closeOnClickModal: false,
    closeOnPressEscape: false,
    showClose: false,
    showCancelButton: false
  }).then(() => {
    window.tipLock = false
    sessionStorage.clear()
    localStorage.removeItem('siteInfo')
    localStorage.removeItem('isLogin')
    router.replace('/login')
  })
}
export function errorRes1 (res) {
  localStorage.setItem('maintainData', JSON.stringify(res.data))
  if (location.pathname === '/maintain') return
  window.document.location.href = '/maintain?from=index'
}

let newVar = {
  /**
   * 网站logo
   * */
  getSiteLogo (callbackFunc) {
    return rest.get(Api.tenantCmsConfig, {}, callbackFunc)
  },
  /*
  * 用户登录
  * **/
  Login (params, callBack) {
    let name = params.username.toLowerCase()
    let password = name + md5(params.password)
    let param = {
      managerName: name,
      password: md5(password)
    }
    if (params.token) {
      param.token = params.token
    }
    return rest.post(Api.login, param, callBack)
  },
  /**
   * 站点切换
   * */
  swichSite (siteID) {
    let param = {
      Action: 'GetListTenantManagerIdentity',
      IdentityId: siteID
    }
    return fetch(Api, param)
  },
  /**
   * 上传图片
   * */
  uploadBanner (fd, callbackFunc) {
    return rest.create(Api.postBanner, fd, callbackFunc)
  },
  /**
   * 上传轮播图片
   * */
  uploadPlatformBanner (fd, callbackFunc) {
    return rest.create(Api.postPlatformBanner, fd, callbackFunc)
  },
  /**
   * 获取菜单权限
   * */
  getNavList () {
    return rest.get(Api.getNavList, {})
  },
  /**
   * 获取菜单权限时间戳
   * */
  getNavTimestamp () {
    return rest.get(Api.getNavTimestamp, {})
  },
  /**
   * 检查是否在维护
   * */
  checkMaintain (callbackFunc) {
    return rest.get(Api.checkMaintain, {}, callbackFunc)
  }
}
export default newVar
