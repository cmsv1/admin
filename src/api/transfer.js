import { rest } from './index'
// 交易服务
const Transfer = {
  bankTransferList: '/v1/transaction/bankTransferList',
  saveOrUpdateBankTransfer: '/v1/transaction/saveOrUpdateBankTransfer',
  deleteBankTransfer: '/v1/transaction/deleteBankTransfer',
  updateBankTransferSort: '/v1/transaction/updateBankTransferSort',
  internetBankList: '/v1/transaction/internetBankList',
  saveOrUpdateInternetBank: '/v1/transaction/saveOrUpdateInternetBank',
  deleteInternetBank: '/v1/transaction/deleteInternetBank',
  wechatList: '/v1/transaction/wechatList',
  saveOrUpdateWechat: '/v1/transaction/saveOrUpdateWechat',
  deleteWechat: '/v1/transaction/deleteWechat',
  alipayList: '/v1/transaction/alipayList',
  saveOrUpdateAlipay: '/v1/transaction/saveOrUpdateAlipay',
  deleteAlipay: '/v1/transaction/deleteAlipay',
  qqList: '/v1/transaction/qqList',
  saveOrUpdateQQ: '/v1/transaction/saveOrUpdateQQ',
  deleteQQ: '/v1/transaction/deleteQQ',
  unionpayList: '/v1/transaction/unionpayList',
  saveOrUpdateUnionpay: '/v1/transaction/saveOrUpdateUnionpay',
  deleteUnionpay: '/v1/transaction/deleteUnionpay',
  fourthPartyList: '/v1/transaction/fourthPartyList',
  saveOrUpdateFourthParty: '/v1/transaction/saveOrUpdateFourthParty',
  deleteFourthParty: '/v1/transaction/deleteFourthParty',
  merchantsPaymentList: '/v1/transaction/merchantsPaymentList',
  updateMerchantsPayment: '/v1/transaction/updateMerchantsPayment',
  paySortList: '/v1/transaction/paySortList',
  updatePaySort: '/v1/transaction/updatePaySort',
  withdrawRecordList: '/v1/transaction/withdrawRecordList',
  updateWithdrawRecordStatus: '/v1/transaction/updateWithdrawRecordStatus',
  updateInternetBankPaymentListSort: '/v1/transaction/updateInternetBankPaymentListSort',
  updateWechatPaymentListSort: '/v1/transaction/updateWechatPaymentListSort',
  updateAlipayPaymentListSort: '/v1/transaction/updateAlipayPaymentListSort',
  updateQQPaymentListSort: '/v1/transaction/updateQQPaymentListSort',
  updateUnionpayPaymentSort: '/v1/transaction/updateUnionpayPaymentSort',
  updateFourthPartySettingsListSort: '/v1/transaction/updateFourthPartySettingsListSort',
  getThirdPartyPaySettingsVo: '/v1/transaction/getThirdPartyPaySettingsVo',
  getStationAgentAndKey: '/v1/transaction/getStationAgentAndKey',
  queryWithdrawOrderDetails: '/v1/transaction/queryWithdrawOrderDetails',
  paymentRecordList: '/v1/transaction/paymentRecordList',
  updateSummaryPaymentRecordState: '/v1/transaction/updateSummaryPaymentRecordState',
  fourthPartyPaymentList: '/v1/transaction/fourthPartyPaymentList',
  saveBatchManualRecord: '/v1/transaction/saveBatchManualRecord',
  manualRecordList: '/v1/transaction/manualRecordList',
  getAccountNameCondition: '/v1/transaction/getAccountNameCondition',
  findDictionAll: '/v1/balance/findDictionAll',
  getTransactionRecordsCms: '/v1/balance/getTransactionRecordsCms',
  tradingWithdrawRecordList: '/v1/transaction/tradingWithdrawRecordList',
  summaryPaymentRecordList: '/v1/transaction/summaryPaymentRecordList',
  getBetDataList: '/v1/betting/getBetDataList',
  getChaseBetDataList: '/v1/betting/getChaseBetDataList',
  getBettingChaseOrder: '/v1/betting/getBettingChaseOrder',
  getBettingChaseOrderInfo: '/v1/betting/getBettingChaseOrderInfo',
  getLiveTipRecordList: '/v1/live/getLiveTipRecordList',
  updatePaySortListSort: '/v1/transaction/updatePaySortListSort',
  importManualRecord: '/v1/transaction/importManualRecord',
  paymentAssistanceSettingsList: '/v1/transaction/paymentAssistanceSettingsList',
  updatePaymentAssistanceSettings: '/v1/transaction/updatePaymentAssistanceSettings',
  updateThirdPartyPaymentState: '/v1/transaction/updateThirdPartyPaymentState',
  userProfit: '/v1/report/userReport/userProfitCMS',
  excelExportSummaryPaymentRecord: '/v1/transaction/excelExportSummaryPaymentRecord',
  getAccountNameToThirdParty: '/v1/transaction/getAccountNameToThirdParty',
  queryTransferList: '/v1/transfer/queryTransferList',
  querySendRedEnvelopeList: '/v1/transfer/querySendRedEnvelopeList',
  queryReceiveRedEnvelopeList: '/v1/transfer/queryReceiveRedEnvelopeList',
  queryReceiveTransferList: '/v1/transfer/queryReceiveTransferList',
  queryCashflowInfo: '/v1/transaction/queryCashflowInfo',
  updateCashflowInfo: '/v1/transaction/updateCashflowInfo',
  getCmsGameBetDataList: '/v1/game/getCmsGameBetDataList',
  getCmsGameBetDataInfo: '/v1/game/getCmsGameBetDataInfo',
  // 云闪付
  quickpassList: '/v1/transaction/quickpassList', // 查询列表
  saveOrUpdateQuickpass: '/v1/transaction/saveOrUpdateQuickpass', // 更新-新增
  deleteQuickpass: '/v1/transaction/deleteQuickpass', // 删除
  updateQuickpassPaymentListSort: '/v1/transaction/updateQuickpassPaymentListSort', // 排序
  // 京东钱包
  jdpayPaymentList: '/v1/transaction/jdpayPaymentList', // 查询列表
  saveOrUpdateJdpayPayment: '/v1/transaction/saveOrUpdateJdpayPayment', // 更新-新增
  deleteJdpayPayment: '/v1/transaction/deleteJdpayPayment', // 删除
  updateJdpayPaymenttListSort: '/v1/transaction/updateJdpayPaymenttListSort', // 排序
  getSetting: '/v1/transaction/rechargeSetting/getSetting',
  updateSetting: '/v1/transaction/rechargeSetting/updateSetting',
  exportBettingRecords: '/v1/betting/exportBettingRecords'
}
export default {
  // 银行转账列表
  bankTransferList (param, callbackFunc) {
    return rest.get(Transfer.bankTransferList, param, callbackFunc)
  },
  // 银行转账新增修改
  saveOrUpdateBankTransfer (param, callbackFunc) {
    return rest.post(Transfer.saveOrUpdateBankTransfer, param, callbackFunc)
  },
  // 银行转账删除
  deleteBankTransfer (param, callbackFunc) {
    return rest.post(Transfer.deleteBankTransfer, param, callbackFunc)
  },
  // 银行转账排序
  updateBankTransferSort (param, callbackFunc) {
    // console.log(12312, param)
    return rest.post(Transfer.updateBankTransferSort, param, callbackFunc)
  },
  // 网银支付列表
  internetBankList (param, callbackFunc) {
    return rest.get(Transfer.internetBankList, param, callbackFunc)
  },
  // 网银支付新增修改
  saveOrUpdateInternetBank (param, callbackFunc) {
    return rest.post(Transfer.saveOrUpdateInternetBank, param, callbackFunc)
  },
  // 网银支付删除
  deleteInternetBank (param, callbackFunc) {
    return rest.post(Transfer.deleteInternetBank, param, callbackFunc)
  },
  // 网银支付排序
  updateInternetBankPaymentListSort (param, callbackFunc) {
    return rest.post(Transfer.updateInternetBankPaymentListSort, param, callbackFunc)
  },
  // 微信支付列表
  wechatList (param, callbackFunc) {
    return rest.get(Transfer.wechatList, param, callbackFunc)
  },
  // 微信支付新增修改
  saveOrUpdateWechat (param, callbackFunc) {
    return rest.post(Transfer.saveOrUpdateWechat, param, callbackFunc)
  },
  // 微信支付删除
  deleteWechat (param, callbackFunc) {
    return rest.post(Transfer.deleteWechat, param, callbackFunc)
  },
  // 微信支付排序
  updateWechatPaymentListSort (param, callbackFunc) {
    return rest.post(Transfer.updateWechatPaymentListSort, param, callbackFunc)
  },
  // 云闪付列表
  quickpassList (param, callbackFunc) {
    return rest.get(Transfer.quickpassList, param, callbackFunc)
  },
  // 云闪付新增修改
  saveOrUpdateQuickpass (param, callbackFunc) {
    return rest.post(Transfer.saveOrUpdateQuickpass, param, callbackFunc)
  },
  // 云闪付删除
  deleteQuickpass (param, callbackFunc) {
    return rest.post(Transfer.deleteQuickpass, param, callbackFunc)
  },
  // 云闪付排序
  updateQuickpassPaymentListSort (param, callbackFunc) {
    return rest.post(Transfer.updateQuickpassPaymentListSort, param, callbackFunc)
  },
  // 京东钱包列表
  jdpayPaymentList (param, callbackFunc) {
    return rest.get(Transfer.jdpayPaymentList, param, callbackFunc)
  },
  // 京东钱包新增修改
  saveOrUpdateJdpayPayment (param, callbackFunc) {
    return rest.post(Transfer.saveOrUpdateJdpayPayment, param, callbackFunc)
  },
  // 京东钱包删除
  deleteJdpayPayment (param, callbackFunc) {
    return rest.post(Transfer.deleteJdpayPayment, param, callbackFunc)
  },
  // 京东钱包排序
  updateJdpayPaymenttListSort (param, callbackFunc) {
    return rest.post(Transfer.updateJdpayPaymenttListSort, param, callbackFunc)
  },
  // 支付宝支付列表
  alipayList (param, callbackFunc) {
    return rest.get(Transfer.alipayList, param, callbackFunc)
  },
  // 支付宝支付新增修改
  saveOrUpdateAlipay (param, callbackFunc) {
    return rest.post(Transfer.saveOrUpdateAlipay, param, callbackFunc)
  },
  // 支付宝支付删除
  deleteAlipay (param, callbackFunc) {
    return rest.post(Transfer.deleteAlipay, param, callbackFunc)
  },
  // 支付宝支付排序
  updateAlipayPaymentListSort (param, callbackFunc) {
    return rest.post(Transfer.updateAlipayPaymentListSort, param, callbackFunc)
  },
  // QQ钱包列表
  qqList (param, callbackFunc) {
    return rest.get(Transfer.qqList, param, callbackFunc)
  },
  // QQ钱包新增修改
  saveOrUpdateQQ (param, callbackFunc) {
    return rest.post(Transfer.saveOrUpdateQQ, param, callbackFunc)
  },
  // QQ钱包删除
  deleteQQ (param, callbackFunc) {
    return rest.post(Transfer.deleteQQ, param, callbackFunc)
  },
  // QQ钱包排序
  updateQQPaymentListSort (param, callbackFunc) {
    return rest.post(Transfer.updateQQPaymentListSort, param, callbackFunc)
  },
  // 银联支付列表
  unionpayList (param, callbackFunc) {
    return rest.get(Transfer.unionpayList, param, callbackFunc)
  },
  // 银联支付新增修改
  saveOrUpdateUnionpay (param, callbackFunc) {
    return rest.post(Transfer.saveOrUpdateUnionpay, param, callbackFunc)
  },
  // 银联支付删除
  deleteUnionpay (param, callbackFunc) {
    return rest.post(Transfer.deleteUnionpay, param, callbackFunc)
  },
  // 银联支付排序
  updateUnionpayPaymentSort (param, callbackFunc) {
    return rest.post(Transfer.updateUnionpayPaymentSort, param, callbackFunc)
  },
  // 第四方列表
  fourthPartyList (param, callbackFunc) {
    return rest.get(Transfer.fourthPartyList, param, callbackFunc)
  },
  // 第四方新增修改
  saveOrUpdateFourthParty (param, callbackFunc) {
    return rest.post(Transfer.saveOrUpdateFourthParty, param, callbackFunc)
  },
  // 第四方删除
  deleteFourthParty (param, callbackFunc) {
    return rest.post(Transfer.deleteFourthParty, param, callbackFunc)
  },
  // 第四方排序
  updateFourthPartySettingsListSort (param, callbackFunc) {
    return rest.post(Transfer.updateFourthPartySettingsListSort, param, callbackFunc)
  },
  // 金额设置列表
  merchantsPaymentList (param, callbackFunc) {
    return rest.get(Transfer.merchantsPaymentList, param, callbackFunc)
  },
  // 金额设置修改
  updateMerchantsPayment (param, callbackFunc) {
    return rest.post(Transfer.updateMerchantsPayment, param, callbackFunc)
  },
  // 支付排序列表
  paySortList (param, callbackFunc) {
    return rest.get(Transfer.paySortList, param, callbackFunc)
  },
  // 支付排序修改
  updatePaySort (param, callbackFunc) {
    return rest.post(Transfer.updatePaySort, param, callbackFunc)
  },
  // 提现订单管理列表查询
  withdrawRecordList (param, callbackFunc) {
    return rest.get(Transfer.withdrawRecordList, param, callbackFunc)
  },
  // 提现订单管理提现操作
  updateWithdrawRecordStatus (param, callbackFunc) {
    return rest.post(Transfer.updateWithdrawRecordStatus, param, callbackFunc)
  },
  // 获取第三方配置
  getThirdPartyPaySettingsVo (param, callbackFunc) {
    return rest.get(Transfer.getThirdPartyPaySettingsVo, param, callbackFunc)
  },
  // 新增第四方时获取站长id和秘要
  getStationAgentAndKey (param, callbackFunc) {
    return rest.get(Transfer.getStationAgentAndKey, param, callbackFunc)
  },
  // 单击提现订单管理金额获取订单信息
  queryWithdrawOrderDetails (param, callbackFunc) {
    return rest.get(Transfer.queryWithdrawOrderDetails, param, callbackFunc)
  },
  // 一般充值、快捷充值列表接口
  paymentRecordList (param, callbackFunc) {
    return rest.get(Transfer.paymentRecordList, param, callbackFunc)
  },
  // 一般充值、快捷充值操作接口
  updateSummaryPaymentRecordState (param, callbackFunc) {
    return rest.post(Transfer.updateSummaryPaymentRecordState, param, callbackFunc)
  },
  // 第四方充值列表接口
  fourthPartyPaymentList (param, callbackFunc) {
    return rest.get(Transfer.fourthPartyPaymentList, param, callbackFunc)
  },
  // 人工存提入款接口
  saveBatchManualRecord (param, callbackFunc) {
    return rest.post(Transfer.saveBatchManualRecord, param, callbackFunc)
  },
  // 人工存款、人工提出列表接口
  manualRecordList (param, callbackFunc) {
    return rest.get(Transfer.manualRecordList, param, callbackFunc)
  },
  // 一般充值列表的账户下拉框数据
  getAccountNameCondition (param, callbackFunc) {
    return rest.get(Transfer.getAccountNameCondition, param, callbackFunc)
  },
  // 交易记录的摘要下拉框数据
  findDictionAll (param, callbackFunc) {
    return rest.get(Transfer.findDictionAll, param, callbackFunc)
  },
  // 交易记录列表
  getTransactionRecordsCms (param, callbackFunc) {
    return rest.get(Transfer.getTransactionRecordsCms, param, callbackFunc)
  },
  // 交易管理-提现记录列表
  tradingWithdrawRecordList (param, callbackFunc) {
    return rest.get(Transfer.tradingWithdrawRecordList, param, callbackFunc)
  },
  // 交易管理-充值记录列表
  summaryPaymentRecordList (param, callbackFunc) {
    return rest.get(Transfer.summaryPaymentRecordList, param, callbackFunc)
  },
  // 交易管理-投注记录列表
  getBetDataList (param, callbackFunc) {
    return rest.get(Transfer.getBetDataList, param, callbackFunc)
  },
  // 交易管理-追号记录列表
  getChaseBetDataList (param, callbackFunc) {
    return rest.get(Transfer.getChaseBetDataList, param, callbackFunc)
  },
  // 交易管理-追号详情
  getBettingChaseOrder (param, callbackFunc) {
    return rest.get(Transfer.getBettingChaseOrder, param, callbackFunc)
  },
  // 交易管理-追号信息
  getBettingChaseOrderInfo (param, callbackFunc) {
    return rest.get(Transfer.getBettingChaseOrderInfo, param, callbackFunc)
  },
  // 直播打赏记录列表
  getLiveTipRecordList (param, callbackFunc) {
    return rest.get(Transfer.getLiveTipRecordList, param, callbackFunc)
  },
  // 充值排序-排序操作
  updatePaySortListSort (param, callbackFunc) {
    return rest.post(Transfer.updatePaySortListSort, param, callbackFunc)
  },
  // 人工存款-导入接口
  importManualRecord (param, callbackFunc) {
    return rest.create(Transfer.importManualRecord, param, callbackFunc)
  },
  // 充值说明-列表获取数据
  paymentAssistanceSettingsList (param, callbackFunc) {
    return rest.get(Transfer.paymentAssistanceSettingsList, param, callbackFunc)
  },
  // 充值说明-修改操作
  updatePaymentAssistanceSettings (param, callbackFunc) {
    return rest.post(Transfer.updatePaymentAssistanceSettings, param, callbackFunc)
  },
  // 第三方强制入款
  updateThirdPartyPaymentState (param, callbackFunc) {
    return rest.post(Transfer.updateThirdPartyPaymentState, param, callbackFunc)
  },
  // 交易管理-提现订单管理-查看会员信息-获取盈利、盈率信息
  userProfit (param, callbackFunc) {
    return rest.get(Transfer.userProfit, param, callbackFunc)
  },
  // 一般充值、第三方充值订单导出
  excelExportSummaryPaymentRecord (param, callbackFunc) {
    return rest.get(Transfer.excelExportSummaryPaymentRecord, param, callbackFunc)
  },
  // 快捷充值账户信息
  getAccountNameToThirdParty (param, callbackFunc) {
    return rest.get(Transfer.getAccountNameToThirdParty, param, callbackFunc)
  },
  // 转账记录接口
  queryTransferList (param, callbackFunc) {
    return rest.get(Transfer.queryTransferList, param, callbackFunc)
  },
  querySendRedEnvelopeList (param, callbackFunc) {
    return rest.get(Transfer.querySendRedEnvelopeList, param, callbackFunc)
  },
  queryReceiveRedEnvelopeList (param, callbackFunc) {
    return rest.get(Transfer.queryReceiveRedEnvelopeList, param, callbackFunc)
  },
  queryReceiveTransferList (param, callbackFunc) {
    return rest.get(Transfer.queryReceiveTransferList, param, callbackFunc)
  },
  queryCashflowInfo (param, callbackFunc) {
    return rest.get(Transfer.queryCashflowInfo, param, callbackFunc)
  },
  updateCashflowInfo (param, callbackFunc) {
    return rest.post(Transfer.updateCashflowInfo, param, callbackFunc)
  },
  // 投注記錄查詢－棋牌投注记录 投注紀錄接口
  getCmsGameBetDataList (param, callbackFunc) {
    return rest.get(Transfer.getCmsGameBetDataList, param, callbackFunc)
  },
  getCmsGameBetDataInfo (param, callbackFunc) {
    return rest.get(Transfer.getCmsGameBetDataInfo, param, callbackFunc)
  },
  // 站点充值时间间隔设置
  getSetting (param, callbackFunc) {
    return rest.get(Transfer.getSetting, param, callbackFunc)
  },
  updateSetting (param, callbackFunc) {
    return rest.post(Transfer.updateSetting, param, callbackFunc)
  },
  exportBettingRecords (param, callbackFunc) {
    return rest.get(Transfer.exportBettingRecords, param, callbackFunc)
  }
}
