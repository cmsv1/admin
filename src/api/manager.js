import { rest } from './index'
// 用户管理
const sysManager = {
  tenantTroleList: '/v1/management/manager/tenantTroleList',
  tenantTrole: '/v1/management/manager/tenantTrole',
  addTenantTrole: '/v1/management/manager/addTenantTrole',
  updateTenantTrole: '/v1/management/manager/updateTenantTrole',
  deleteTenantTrole: '/v1/management/manager/deleteTenantTrole',
  tenantManagerList: '/v1/management/manager/tenantManagerList',
  tenantManager: '/v1/management/manager/tenantManager',
  addTenantManager: '/v1/management/manager/addTenantManager',
  updateTenantManager: '/v1/management/manager/updateTenantManager',
  deleteTenantManager: '/v1/management/manager/deleteTenantManager',
  updateTenantManagerState: '/v1/management/manager/updateTenantManagerState',
  tenantTpermissionList: '/v1/management/manager/tenantTpermissionList'
}
export default {
  // 角色管理列表
  tenantTroleList (param, callbackFunc) {
    return rest.get(sysManager.tenantTroleList, param, callbackFunc)
  },
  // 角色管理详细信息
  tenantTrole (param, callbackFunc) {
    return rest.get(sysManager.tenantTrole, param, callbackFunc)
  },
  // 角色管理新增
  addTenantTrole (param, callbackFunc) {
    return rest.post(sysManager.addTenantTrole, param, callbackFunc)
  },
  // 角色管理修改
  updateTenantTrole (param, callbackFunc) {
    return rest.post(sysManager.updateTenantTrole, param, callbackFunc)
  },
  // 角色管理删除
  deleteTenantTrole (param, callbackFunc) {
    return rest.post(sysManager.deleteTenantTrole, param, callbackFunc)
  },
  // 管理员列表
  tenantManagerList (param, callbackFunc) {
    return rest.get(sysManager.tenantManagerList, param, callbackFunc)
  },
  // 管理员编辑页获取数据
  tenantManager (param, callbackFunc) {
    return rest.get(sysManager.tenantManager, param, callbackFunc)
  },
  // 管理员添加数据
  addTenantManager (param, callbackFunc) {
    return rest.post(sysManager.addTenantManager, param, callbackFunc)
  },
  // 管理员修改数据
  updateTenantManager (param, callbackFunc) {
    return rest.post(sysManager.updateTenantManager, param, callbackFunc)
  },
  // 管理员删除数据
  deleteTenantManager (param, callbackFunc) {
    return rest.post(sysManager.deleteTenantManager, param, callbackFunc)
  },
  // 管理员冻结或取消冻结
  updateTenantManagerState (param, callbackFunc) {
    return rest.post(sysManager.updateTenantManagerState, param, callbackFunc)
  },
  // 获取站长权限清单
  tenantTpermissionList (param, callbackFunc) {
    return rest.get(sysManager.tenantTpermissionList, param, callbackFunc)
  }
}
