import { rest } from './index'
// 活动服务
const Activity = {
  setSysActivity: '/v1/activity/setSysActivity',
  getSysActivityList: '/v1/activity/getSysActivityList',
  setSysActivitySort: '/v1/activity/setSysActivitySort',
  setSysActivityStatus: '/v1/activity/setSysActivityStatus',
  getSysActivity: '/v1/activity/getSysActivity',
  delSysActivity: '/v1/activity/delSysActivity',
  setActivity: '/v1/activity/setActivity',
  getTenantActivityList: '/v1/activity/getTenantActivityList',
  setActivitySort: '/v1/activity/setActivitySort',
  setActivityStatus: '/v1/activity/setActivityStatus',
  delActivity: '/v1/activity/delActivity',
  getActivity: '/v1/activity/getActivity',
  getActivityLogList: '/v1/activity/getActivityLogList',
  getActivityBanList: '/v1/activity/getActivityBanList',
  setActivityBan: '/v1/activity/setActivityBan',
  getRechargeBonusConfig: '/v1/activity/getRechargeBonusConfig',
  setRechargeBonusConfig: '/v1/activity/setRechargeBonusConfig',
  delActivityBan: '/v1/activity/delActivityBan',
  getActivityImageLimitConfig: '/v1/activity/getActivityImageLimitConfig',
  getSysActivityImageLimitConfig: '/v1/activity/getSysActivityImageLimitConfig',
  uploadActivityImage: '/v1/activity/uploadActivityImage',
  uploadSysActivityImage: '/v1/activity/uploadSysActivityImage',
  setUpgradeConfig: '/v1/activity/setUpgradeConfig',
  setRewardRate: '/v1/activity/setRewardRate',
  getCmsUpgradeConfig: '/v1/activity/getCmsUpgradeConfig',
  getCmsRewardRate: '/v1/activity/getCmsRewardRate'
}
export default {
  // 系统活动列表添加修改
  setSysActivity (param, callbackFunc) {
    return rest.create(Activity.setSysActivity, param, callbackFunc)
  },
  // 系统活动列表
  getSysActivityList (param, callbackFunc) {
    return rest.get(Activity.getSysActivityList, param, callbackFunc)
  },
  // 系统活动列表-排序操作
  setSysActivitySort (param, callbackFunc) {
    return rest.post(Activity.setSysActivitySort, param, callbackFunc)
  },
  // 系统活动列表-启用或禁用操作
  setSysActivityStatus (param, callbackFunc) {
    return rest.post(Activity.setSysActivityStatus, param, callbackFunc)
  },
  // 系统活动编辑页面获取数据
  getSysActivity (param, callbackFunc) {
    return rest.get(Activity.getSysActivity, param, callbackFunc)
  },
  // 系统活动列表-删除
  delSysActivity (param, callbackFunc) {
    return rest.post(Activity.delSysActivity, param, callbackFunc)
  },
  // 站长活动列表添加修改
  setActivity (param, callbackFunc) {
    return rest.create(Activity.setActivity, param, callbackFunc)
  },
  // 站长活动列表
  getTenantActivityList (param, callbackFunc) {
    return rest.get(Activity.getTenantActivityList, param, callbackFunc)
  },
  // 站长活动-排序操作
  setActivitySort (param, callbackFunc) {
    return rest.post(Activity.setActivitySort, param, callbackFunc)
  },
  // 站长活动-启用或禁用操作
  setActivityStatus (param, callbackFunc) {
    return rest.post(Activity.setActivityStatus, param, callbackFunc)
  },
  // 站长活动-删除操作
  delActivity (param, callbackFunc) {
    return rest.post(Activity.delActivity, param, callbackFunc)
  },
  // 站长活动-编辑页面获取数据
  getActivity (param, callbackFunc) {
    return rest.get(Activity.getActivity, param, callbackFunc)
  },
  // 活动明细列表获取数据
  getActivityLogList (param, callbackFunc) {
    return rest.get(Activity.getActivityLogList, param, callbackFunc)
  },
  // 活动禁止列表
  getActivityBanList (param, callbackFunc) {
    return rest.get(Activity.getActivityBanList, param, callbackFunc)
  },
  // 活动禁止新增修改
  setActivityBan (param, callbackFunc) {
    return rest.post(Activity.setActivityBan, param, callbackFunc)
  },
  // 活动优惠设置获取数据
  getRechargeBonusConfig (param, callbackFunc) {
    return rest.get(Activity.getRechargeBonusConfig, param, callbackFunc)
  },
  // 活动优惠设置保存操作
  setRechargeBonusConfig (param, callbackFunc) {
    return rest.post(Activity.setRechargeBonusConfig, param, callbackFunc)
  },
  // 活动优惠设置删除操作
  delActivityBan (param, callbackFunc) {
    return rest.post(Activity.delActivityBan, param, callbackFunc)
  },
  // 站长活动图片限制配置
  getActivityImageLimitConfig (param, callbackFunc) {
    return rest.get(Activity.getActivityImageLimitConfig, param, callbackFunc)
  },
  // 平台系统活动图片限制配置
  getSysActivityImageLimitConfig (param, callbackFunc) {
    return rest.get(Activity.getSysActivityImageLimitConfig, param, callbackFunc)
  },
  // 站长活动上传内容图片
  uploadActivityImage (param, callbackFunc) {
    return rest.create(Activity.uploadActivityImage, param, callbackFunc)
  },
  // 系统活动上传内容图片
  uploadSysActivityImage (param, callbackFunc) {
    return rest.create(Activity.uploadSysActivityImage, param, callbackFunc)
  },
  // 后台取得晉級獎勵設置
  getCmsUpgradeConfig (param, callbackFunc) {
    return rest.get(Activity.getCmsUpgradeConfig, param, callbackFunc)
  },
  // 后台每日加獎設置
  getCmsRewardRate (param, callbackFunc) {
    return rest.get(Activity.getCmsRewardRate, param, callbackFunc)
  },
  // 设置晋级奖励
  setUpgradeConfig (param, callbackFunc) {
    return rest.post(Activity.setUpgradeConfig, param, callbackFunc)
  },
  // 设置每日加獎
  setRewardRate (param, callbackFunc) {
    return rest.post(Activity.setRewardRate, param, callbackFunc)
  }
}
