// import MediumEditor from 'medium-editor'
// import MediumButton from 'medium-button'

// export function mediumEditor (element, options) {
//   return new MediumEditor(element, options)
// }
// export function mediumButton (options) {
//   return new MediumButton(options)
// }

let me, mb

export function createMediumEditor (clb) {
  if (me && mb) {
    clb(me, mb)
  } else {
    Promise.all([
      import(/* webpackChunkName: "mediumeditor" */ 'medium-editor'),
      import(/* webpackChunkName: "mediumeditor" */ 'medium-button')
    ])
      .then(([e, b]) => {
        if (e && b) {
          me = e
          mb = b
          clb(me, mb)
        }
      })
  }
}
