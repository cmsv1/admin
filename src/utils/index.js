// 开发环境 和 测试环境
import {formatMoney} from "../filters";

export function nodeEnv () {
  return process.env.NODE_ENV === 'production'
}

export const messagePrompt = {
  msgError (msg) {
    this.$message({
      showClose: true,
      message: msg,
      type: 'error',
      duration: 3000
    })
  },
  msgWarning (msg) {
    this.$message({
      message: msg,
      type: 'warning',
      duration: 3000
    })
  },
  msgSuccess (msg) {
    this.$message({
      dangerouslyUseHTMLString: true,
      message: msg,
      type: 'success'
    })
  },
  msgInfo (msg) {
    this.$message({
      message: msg,
      type: 'info'
    })
  },
  msgConfirm (msg, object) {
    return this.$msgbox.confirm(msg, '温馨提示', object)
  },
  msgAlert (msg, title, object) {
    return this.$msgbox.alert(msg, title, object)
  }
}
export function camelToKebab (str) {
  return str.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase()
}
export default {
  install (Vue, options) {
    // 添加消息功能方法
    Object.assign(Vue.prototype, messagePrompt)

    let uid = 0
    Vue.mixin({
      beforeCreate () {
        // 添加组件唯一标识
        this.$uid = uid.toString()
        uid += 1
      }
    })
  }
}
// 桌面通知，参考 https://developer.mozilla.org/zh-CN/docs/Web/API/notification/Using_Web_Notifications
export function getNotificationPermission (cb) {
  if (window.Notification && window.Notification.permission !== 'granted') {
    window.Notification.requestPermission(function (status) {
      if (window.Notification.permission !== status) {
        window.Notification.permission = status
      }
      if (cb && typeof cb === 'function') {
        cb(status)
      }
    })
  }
}
export function dateFormat (t, format) { // t 为毫秒， format 为格式
  var tf = function (i) {
    return (i < 10 ? '0' : '') + i
  };
  return format.replace(/yyyy|MM|dd|HH|mm|ss/g, function (a) {
    switch (a) {
      case 'yyyy':
        return tf(t.getFullYear())
        break
      case 'MM':
        return tf(t.getMonth() + 1)
        break
      case 'mm':
        return tf(t.getMinutes())
        break
      case 'dd':
        return tf(t.getDate())
        break
      case 'HH':
        return tf(t.getHours())
        break
      case 'ss':
        return tf(t.getSeconds())
        break
    }
  })
}
export function getJsonDiff (o, n) {
  if (!o) {
    return { ...n }
  }
  let ret = {}
  Object.keys(n).forEach(p => {
    if (o[p] !== n[p]) {
      if (typeof o[p] === 'object' &&
        typeof n[p] === 'object' &&
        JSON.stringify(o[p]) === JSON.stringify(n[p])) {
        return
      }
      ret[p] = n[p]
    }
  })
  return ret
}
export function touchEvent (flag) {
  if (flag) {
    document.addEventListener('touchmove', preventDefaultEvent, isPassive() ? {
      passive: false
    } : false)
  } else {
    document.removeEventListener('touchmove', preventDefaultEvent, isPassive() ? {
      passive: false
    } : false)
  }
}
function isPassive () {
  var supportsPassiveOption = false
  try {
    addEventListener("test", null, Object.defineProperty({}, 'passive', {
      get: function () {
        supportsPassiveOption = true;
      }
    }))
  } catch(e) {}
  return supportsPassiveOption
}
function preventDefaultEvent (e) {
  e.preventDefault()
}

export function changeTextHtml (v, w) {
  let strDiv = document.querySelector('#str-length')
  var str = '' // 存放截断字符串
  for (let j = 0; j < v.length; j++) {
    str = str + v[j]
    // console.log(str)
    strDiv.innerText = str
    if (strDiv.offsetWidth > w) {
      return {
        flag: 1,
        str: `${str}<span class="color-blue">...</span>`
      }
    }
  }
  return {
    flag: 0,
    str: str
  }
}
export function getEllipsisHtml (columns, data) {
  let strDiv = document.querySelector('#str-length')
  for (let i = 0; i < data.length; i++) {
    for (let j = 0; j < columns.length; j++) {
      let content = data[i][columns[j].name]
      // console.log(content)
      let width = columns[j].width
      let formatterFunc = columns[j].formatterFunc
      if (content === undefined || content === null) {
        data[i][columns[j].name+'Flag'] = 0
        data[i][columns[j].name+'Content'] = ''
        continue
      }
      if (typeof formatterFunc === 'function') {
        content = formatterFunc(data[i])
        // console.log(content)
      }
      let tempContent = ''
      let isEllipsis = false
      for (let k = 0; k < content.length; k++) {
        tempContent = tempContent + content[k]
        // console.log(str)
        strDiv.innerText = tempContent
        if (strDiv.offsetWidth > width) {
          data[i][columns[j].name+'Flag'] = 1
          data[i][columns[j].name+'Content'] = `${tempContent}<span class="color-blue">...</span>`
          // console.log(content, tempContent + ' ' + data[i][columns[j].name+'Content'])
          isEllipsis = true
          break
        }
      }
      if (!isEllipsis) {
        data[i][columns[j].name+'Flag'] = 0
        data[i][columns[j].name+'Content'] = content
      }
    }
  }
}
export function getEllipsisFlag (columnDomId, contentDomId) {
  // let strDiv = document.querySelector('#str-length')
  // let content = data[column.name]
  // // console.log(content)
  // let width = column.width
  // let formatterFunc = column.formatterFunc
  // if (content === undefined || content === null) {
  //   return false
  // }
  // if (typeof formatterFunc === 'function') {
  //   content = formatterFunc(data)
  //   // console.log(content)
  // }
  // let tempContent = ''
  // for (let k = 0; k < content.length; k++) {
  //   tempContent = tempContent + content[k]
  //   // console.log(str)
  //   strDiv.innerText = tempContent
  //   if (strDiv.offsetWidth > width) {
  //     data[column.name+'Flag'] = 1
  //     data[column.name+'Content'] = `${tempContent}<span>...</span>`
  //     // console.log(content, tempContent + ' ' + data[i][columns[j].name+'Content'])
  //     return true
  //   }
  // }
  let columnDom = document.querySelector('#' + columnDomId)
  let contentDom = document.querySelector('#' + contentDomId)
  if (contentDom.offsetWidth + 32 >= columnDom.offsetWidth) {
    return true
  }
  return false
}
export function testAutoPlay (successCallBackFunc, errCallBackFunc) {
  // 返回一个promise以告诉调用者检测结果
  return new Promise(resolve => {
    let audio = document.getElementById('bgMusic')
    // play返回的是一个promise
    audio.play().then(() => {
      // 支持自动播放
      if(typeof successCallBackFunc === 'function') {
        successCallBackFunc()
      }
      return true
    }).catch(errl => {
      // 不支持自动播放
      if(typeof errCallBackFunc === 'function') {
        errCallBackFunc()
      }
      return false
    })
  })
}
