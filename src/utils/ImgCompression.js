export function imgCompression (file, quality, resultFun) {
  // let ready = new FileReader()
  // /*开始读取指定的Blob对象或File对象中的内容. 当读取操作完成时,readyState属性的值会成为DONE,如果设置了onloadend事件处理程序,则调用之.同时,result属性中将包含一个data: URL格式的字符串以表示所读取文件的内容.*/
  // ready.readAsDataURL(file)
  // ready.onload = function () {
  //
  // }
  canvasDataURL(file, quality, resultFun)
}
function canvasDataURL (file, obj, callback) {
  if(!file) {
    return null
  }
  //读取图片数据
  let reader = new FileReader()
  reader.onload = function (e) {
    let data = e.target.result
    //加载图片获取图片真实宽度和高度
    let image = new Image()
    image.src= data
    image.onload = function () {
      let that = this
      // 默认按比例压缩
      let w = that.width, h = that.height, scale = w / h
      w = obj.width || w
      h = obj.height || (w / scale)
      let quality = 0.7 // 默认图片质量为0.7
      //生成canvas
      let canvas = document.createElement('canvas')
      let ctx = canvas.getContext('2d')
      // 创建属性节点
      let anw = document.createAttribute('width')
      anw.nodeValue = w
      let anh = document.createAttribute('height')
      anh.nodeValue = h
      canvas.setAttributeNode(anw)
      canvas.setAttributeNode(anh)
      ctx.drawImage(that, 0, 0, w, h)
      // 图像质量
      if(obj.quality && obj.quality <= 1 && obj.quality > 0){
        quality = obj.quality
      }
      // quality值越小，所绘制出的图像越模糊
      let base64 = canvas.toDataURL('image/jpeg', quality)
      // 回调函数返回base64的值
      let blob = convertBase64UrlToBlob(base64)
      callback(blob)
    }
  }
  reader.readAsDataURL(file)
}
/**
 * 将以base64的图片url数据转换为Blob
 * @param urlData
 *            用url方式表示的base64图片数据
 */
function convertBase64UrlToBlob (urlData) {
  var arr = urlData.split(','),
    type = arr[0].match(/:(.*?);/)[1], // 生成的图片类型
    bstr = atob(arr[1]),
    n = bstr.length,
    u8arr = new Uint8Array(n)
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n)
  }
  return new Blob([u8arr], {type: type})
}
