let bank=[{'Name': '工商银行', 'Short': '工行', 'En': 'ICBC'}, {'Name': '建设银行', 'Short': '建行', 'En': 'CCB'}, {'Name': '农业银行', 'Short': '农行', 'En': 'ABC'}, {'Name': '民生银行', 'Short': '民生', 'En': 'CMBC'}, {'Name': '招商银行', 'Short': '招商', 'En': 'CMB'}, {'Name': '交通银行', 'Short': '交通', 'En': 'BCM'}, {'Name': '中国银行', 'Short': '中行', 'En': 'BOC'}, {'Name': '邮政储蓄', 'Short': '邮政', 'En': 'PSBC'}, {'Name': '中信银行', 'Short': '中信', 'En': 'CITIC'}, {'Name': '兴业银行', 'Short': '兴业', 'En': 'CIB'}, {'Name': '华夏银行', 'Short': '华夏', 'En': 'HXBANK'}, {'Name': '浦发银行', 'Short': '浦发', 'En': 'SPDB'}, {'Name': '广发银行', 'Short': '广发', 'En': 'GDB'}, {'Name': '平安银行', 'Short': '平安', 'En': 'SPABANK'}, {'Name': '光大银行', 'Short': '光大', 'En': 'CEB'}, {'Name': '银座银行', 'Short': '银座', 'En': 'YZB'}, {'Name': '农商银行', 'Short': '农商', 'En': 'CRCC'},{'Name':'其它银行','Short':'','En':''}]
let navlist = [
  {
    'id': '01',
    'name': '样式一',
    'url': '/system/common/menu/menu_01.jpg'
  },
  {
    'id': '02',
    'name': '样式二',
    'url': '/system/common/menu/menu_02.jpg'
  },
  {
    'id': '03',
    'name': '样式三',
    'url': '/system/common/menu/menu_03.jpg'
  },
  {
    'id': '04',
    'name': '样式四',
    'url': '/system/common/menu/menu_04.jpg'
  },
  {
    'id': '05',
    'name': '样式五',
    'url': '/system/common/menu/menu_05.jpg'
  },
  {
    'id': '06',
    'name': '样式六',
    'url': '/system/common/menu/menu_06.jpg'
  },
  {
    'id': '07',
    'name': '样式七',
    'url': '/system/common/menu/menu_07.jpg'
  },
  {
    'id': '08',
    'name': '样式八',
    'url': '/system/common/menu/menu_08.jpg'
  },
  {
    'id': '09',
    'name': '样式九',
    'url': '/system/common/menu/menu_09.jpg'
  },
  {
    'id': '10',
    'name': '样式十',
    'url': '/system/common/menu/menu_10.jpg'
  }
]
let gradeData = {
  '-1': '测试组',
  '0': '黑名单',
  '1': 'VIP1',
  '2': 'VIP2',
  '3': 'VIP3',
  '4': 'VIP4',
  '5': 'VIP5',
  '6': 'VIP6',
  '7': 'VIP7',
  '8': 'VIP8',
  '9': 'VIP9',
  '10': 'VIP10'
}
let gradeVIPData = {
  '-2': '访客',
  '-1': '测试组',
  '0': '黑名单',
  '1': 'VIP1',
  '2': 'VIP2',
  '3': 'VIP3',
  '4': 'VIP4',
  '5': 'VIP5',
  '6': 'VIP6',
  '7': 'VIP7',
  '8': 'VIP8',
  '9': 'VIP9',
  '10': 'VIP10'
}
let gradeChatRoom = {
  '-2': '访客',
  '-1': '测试组',
  '0': '黑名单',
  '1': 'VIP1',
  '2': 'VIP2',
  '3': 'VIP3',
  '4': 'VIP4',
  '5': 'VIP5',
  '6': 'VIP6',
  '7': 'VIP7',
  '8': 'VIP8',
  '9': 'VIP9',
  '10': 'VIP10',
  '100': '管理员'
}
let sourceListData = [
  {
    key: 1,
    value: 'PC'
  }, {
    key: 2,
    value: 'Mobile'
  }]
let sourceData = [
  {
    key: 1,
    value: 'PC'
  }, {
    key: 2,
    value: 'Mobile'
  }, {
    key: 3,
    value: 'APP'
  }]
let gradeListData = [
    {
      key: -1,
      value: '测试组'
    }, {
      key: 0,
      value: '黑名单'
    }, {
      key: 1,
      value: 'VIP1'
    }, {
      key: 2,
      value: 'VIP2'
    }, {
      key: 3,
      value: 'VIP3'
    }, {
      key: 4,
      value: 'VIP4'
    }, {
      key: 5,
      value: 'VIP5'
    }, {
      key: 6,
      value: 'VIP6'
    }, {
      key: 7,
      value: 'VIP7'
    }, {
      key: 8,
      value: 'VIP8'
    }, {
      key: 9,
      value: 'VIP9'
    }, {
      key: 10,
      value: 'VIP10'
    }
  ]
let gradeList = [
  {
    key: -1,
    value: '测试组'
  }, {
    key: 1,
    value: 'VIP1'
  }, {
    key: 2,
    value: 'VIP2'
  }, {
    key: 3,
    value: 'VIP3'
  }, {
    key: 4,
    value: 'VIP4'
  }, {
    key: 5,
    value: 'VIP5'
  }, {
    key: 6,
    value: 'VIP6'
  }, {
    key: 7,
    value: 'VIP7'
  }, {
    key: 8,
    value: 'VIP8'
  }, {
    key: 9,
    value: 'VIP9'
  }, {
    key: 10,
    value: 'VIP10'
  }
]
let gradeVIPList = [
  {
    key: -2,
    value: '访客'
  },
  {
    key: -1,
    value: '测试组'
  },
  {
    key: 1,
    value: 'VIP1'
  }, {
    key: 2,
    value: 'VIP2'
  }, {
    key: 3,
    value: 'VIP3'
  }, {
    key: 4,
    value: 'VIP4'
  }, {
    key: 5,
    value: 'VIP5'
  }, {
    key: 6,
    value: 'VIP6'
  }, {
    key: 7,
    value: 'VIP7'
  }, {
    key: 8,
    value: 'VIP8'
  }, {
    key: 9,
    value: 'VIP9'
  }, {
    key: 10,
    value: 'VIP10'
  }
]
//运维日志 狀態
let  NetworkStateList = [
  {
    key: 1,
    value: '主线'
  }, {
    key: 2,
    value: '备线'
  }, {
    key: 3,
    value: '高防'
  },
  {
    key: 4,
    value: '其它'
  }
  ]
// 安全服務的終端
let NetworksourceData = [
  {
    key: 1,
    value: '电脑'
  }, {
    key: 2,
    value: '手机'
  }, {
    key: 4,
    value: '密聊'
  }]
export { bank, navlist, gradeData, sourceListData, sourceData, gradeListData, gradeList, gradeVIPList, gradeVIPData, gradeChatRoom, NetworksourceData, NetworkStateList}
