// vdt.js
import { Message } from 'element-ui'
let validateRules = {
  messages: {
    required: '这是必填字段',
    remote: '请修正此字段',
    email: '请输入有效的电子邮件地址',
    url: '请输入有效的网址',
    date: '请输入有效的日期',
    dateISO: '请输入有效的日期 (YYYY-MM-DD)',
    number: '请输入有效的数字',
    digits: '只能输入整数',
    creditcard: '请输入有效的信用卡号码',
    equalTo: '你的输入不相同',
    extension: '请输入有效的后缀',
    minlength: '输入字数过短',
    maxlength: '输入字数过长',
    mphone: '请输入正确的手机号格式',
    tphone: '请输入正确的电话格式',
    ip: '请输入有效的IP地址',
    postal: '请输入正确的邮编格式',
    isChineseChar: '请输入中文格式',
    isEnglishNumber: '格式错误，只能输入英文或数字',
    password: '密码格式错误，密码6-16位，请使用字母、数字'
  },
  required: function (value, param) {
    return value != undefined ? (value.toString().length > 0) : false
  },
  email: function (value) {
    return /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(value)
  },
  url: function (value) {
    return /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(value)
  },
  date: function (value) {
    return !/Invalid|NaN/.test(new Date(value).toString())
  },
  dateISO: function (value) {
    return /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/.test(value)
  },
  number: function (value) {
    return /^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(value)
  },
  digits: function (value) {
    return /^\d+$/.test(value)
  },
  isarr: function (o) {
    return Object.prototype.toString.call(o) == '[object Array]'
  },
  minlength: function (value, param) {
    return value.length >= param
  },
  maxlength: function (value, param) {
    return value.length <= param
  },
  rangelength: function (value, param) {
    var length = $.isArray(value) ? value.length : this.getLength(value)
    return (length >= param[0] && length <= param[1])
  },
  min: function (value, param) {
    return value >= param
  },
  max: function (value, param) {
    return value <= param
  },
  range: function (value, param) {
    return (value >= param[0] && value <= param[1])
  },
  equalTo: function (value, param) {
    return value === param
  },
  mphone: function (value) {
    return /^1[3|4|5|8][0-9]\d{4,8}$/.test(value)
  },
  tphone: function (value) {
    return /^[+]{0,1}(\d){1,3}[ ]?([-]?((\d)|[ ]){1,12})+$/.test(value)
  },
  ip: function (value) {
    let ipv4 = /^((\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\.){4}$/
    let ipv6 = /^(([\da-fA-F]{1,4}):){8}$/
    return ipv4.test(value + '.') || ipv6.test(value + ':')
  },
  postal: function (value) {
    return /^[a-zA-Z0-9 ]{3,12}$/g.test(value)
  },
  isChineseChar: function (value) {
    return !/[^\u4e00-\u9fa5]/.test(value)
  },
  isEnglishNumber: function (value) {
    return /^[0-9a-zA-Z]*$/.test(value)
  },
  otherName: function (value) {
    return /^[\u4e00-\u9fa5|·|.]{2,15}$|^[a-zA-Z|\s]{2,30}$/.test(value)
  },
  password: function (value) {
    return /^[\w!@#$%^&*.]{6,16}$/.test(value)
  },
  vdata: function (value, config) { // 返回正确错误对象 提示 与结果
    for (var fun in config) {
      if (typeof this[fun] == 'function' && (!(config[fun].param == undefined ? this[fun](value) : this[fun](value, config[fun].param)))) {
        if (typeof config[fun] == 'object') {
          return { msg: config[fun].msg ? config[fun].msg : this.messages[fun], result: false }
        } else {
          return { msg: typeof config[fun] == 'string' ? config[fun] : this.messages[fun], result: false }
        }
      } else if (typeof config[fun] == 'function') {
        var tmpr = config[fun](value)
        if (tmpr != '' && tmpr != undefined && tmpr != false) {
          return { msg: tmpr, result: false }
        }
      }
    }
    return { msg: '', result: true }
  }
}
let DateFormat = (str, format) => {
  let time = new Date(str)
  let date = {
    'M+': time.getMonth() + 1,
    'd+': time.getDate(),
    'h+': time.getHours(),
    'm+': time.getMinutes(),
    's+': time.getSeconds(),
    'q+': Math.floor((time.getMonth() + 3) / 3),
    'S+': time.getMilliseconds()
  }
  if (/(y+)/i.test(format)) {
    format = format.replace(RegExp.$1, (time.getFullYear() + '').substr(4 - RegExp.$1.length))
  }
  for (var k in date) {
    if (new RegExp('(' + k + ')').test(format)) {``
      format = format.replace(RegExp.$1, RegExp.$1.length === 1 ? date[k] : ('00' + date[k]).substr(('' + date[k]).length))
    }
  }
  return format
}
let validateQueryDate = {
    validatehour : function(startTime, endTime) {
      if (!startTime) {
        Message.error('请选择开始时间')
        return false
      }
      if (!endTime) {
        Message.error('请选择结束时间')
        return false
      }
      if (new Date(DateFormat(startTime, 'yyyy-MM-dd hh:mm:ss')) >= new Date(DateFormat(endTime, 'yyyy-MM-dd hh:mm:ss'))) {
        Message.error('开始时间不能大于等于结束时间')
        return false
      }
      if (new Date() - new Date(startTime) > 1000 * 60 * 60 * 24 * 60) {
        Message.error('只能查询最近60天记录')
        return false
      }
      if (new Date(endTime) - new Date(startTime) > 1000 * 60 * 60 * 24 * 60) {
        Message.error('只能查询60天记录')
        return false
      }
      return true
    },
    validateDate : function(startTime, endTime) {
      if (!startTime) {
        Message.error('请选择开始时间')
        return false
      }
      if (!endTime) {
        Message.error('请选择结束时间')
        return false
      }
      if (new Date(DateFormat(startTime, 'yyyy-MM-dd')) > new Date(DateFormat(endTime, 'yyyy-MM-dd'))) {
        Message.error('开始时间不能大于结束时间')
        return false
      }
      if (new Date() - new Date(startTime) > 1000 * 60 * 60 * 24 * 60) {
        Message.error('只能查询最近60天记录')
        return false
      }
      if (new Date(endTime) - new Date(startTime) > 1000 * 60 * 60 * 24 * 60) {
        Message.error('只能查询60天记录')
        return false
      }
      return true
    },
    validateDateOne: function (startTime) {
      if (!startTime) {
        Message.error('请选择日期')
        return false
      }
      if (new Date(startTime) - new Date() > 0) {
        Message.error('查询时间不能大于当前时间')
        return false
      }
      if (new Date() - new Date(startTime) > 1000 * 60 * 60 * 24 * 30) {
        Message.error('只能查询最近30天记录')
        return false
      }
      return true
    },
    validateDateTwoHour : function(startTime, endTime, days) {
      if (!startTime) {
        Message.error('请选择开始时间')
        return false
      }
      if (!endTime) {
        Message.error('请选择结束时间')
        return false
      }
      if (new Date(DateFormat(startTime, 'yyyy-MM-dd hh:mm:ss')) >= new Date(DateFormat(endTime, 'yyyy-MM-dd hh:mm:ss'))) {
        Message.error('开始时间不能大于等于结束时间')
        return false
      }
      if (new Date() - new Date(startTime) > 1000 * 60 * 60 * 24 * Number(days)) {
        Message.error('只能查询最近'+days+'天记录')
        return false
      }
      if (new Date(endTime) - new Date(startTime) > 1000 * 60 * 60 * 24 * Number(days)) {
        Message.error('只能查询'+days+'天记录')
        return false
      }
      return true
    },
    validateDateTwo : function(startTime, endTime, days) {
    if (!startTime) {
      Message.error('请选择开始时间')
      return false
    }
    if (!endTime) {
      Message.error('请选择结束时间')
      return false
    }
    if (new Date(DateFormat(startTime, 'yyyy-MM-dd')) > new Date(DateFormat(endTime, 'yyyy-MM-dd'))) {
      Message.error('开始时间不能大于结束时间')
      return false
    }
    if (new Date() - new Date(startTime) > 1000 * 60 * 60 * 24 * Number(days)) {
      Message.error('只能查询最近' + days + '天记录')
      return false
    }
    if (new Date(endTime) - new Date(startTime) > 1000 * 60 * 60 * 24 * Number(days)) {
      Message.error('只能查询' + days + '天记录')
      return false
    }
    return true
  }
}
let validateQueryDateTwo = {
  validateHour: function (startTime, endTime) {
    if (startTime && !endTime) {
      Message.error('请输入结束时间')
      return false
    }
    if (!startTime && endTime) {
      Message.error('请输入开始时间')
      return false
    }
    if (startTime && endTime && new Date(DateFormat(startTime, 'yyyy-MM-dd hh:mm:ss')) >= new Date(DateFormat(endTime, 'yyyy-MM-dd hh:mm:ss'))) {
      Message.error('开始时间不能大于等于结束时间')
      return false
    }
    return true
  },
  validateDate: function (startTime, endTime) {
    if (startTime && !endTime) {
      Message.error('请输入结束时间')
      return false
    }
    if (!startTime && endTime) {
      Message.error('请输入开始时间')
      return false
    }
    if (startTime && endTime && new Date(DateFormat(startTime, 'yyyy-MM-dd')) > new Date(DateFormat(endTime, 'yyyy-MM-dd'))) {
      Message.error('开始时间不能大于结束时间')
      return false
    }
    return true
  }
}
let validateQueryDateThree = {
  validateDate: function (startTime, endTime) {
    if (!startTime && endTime) {
      Message.error('请选择开始时间')
      return false
    }
    if (startTime && !endTime) {
      Message.error('请选择结束时间')
      return false
    }
    if (new Date(DateFormat(startTime, 'yyyy-MM-dd')) > new Date(DateFormat(endTime, 'yyyy-MM-dd'))) {
      Message.error('开始时间不能大于结束时间')
      return false
    }
    return true
  }
}
export { validateRules, validateQueryDate, validateQueryDateTwo, validateQueryDateThree }
