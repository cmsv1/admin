// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import './components'
import '../theme/index.css'
import '../src/assets/css/master.css'
import '../src/assets/css/main-table.css'

import Vue from 'vue'
import App from './App'
import router from './router'
import Util from '@/utils'
import * as directives from './directives'
import * as filters from './filters'
import {Loading} from 'element-ui'
import BusPlugin from '@/utils/busPlugin'
import store from './store'
import 'babel-polyfill'
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})

// 添加工具类
Vue.use(Util)
let screeWidth = document.documentElement.clientWidth
if (screeWidth <= 768) {
  import(/* webpackChunkName: "vant" */'vant/lib/datetime-picker').then((DatetimePicker) => {
    Vue.use(DatetimePicker.default)
  })
  require('vant/lib/datetime-picker/style')
}

Vue.use(BusPlugin)
// register global utility directives.
Object.keys(directives).forEach(key => {
  Vue.directive(key, directives[key])
})

// register global utility filters.
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

Vue.use(Loading.directive)
Vue.prototype.$loading = Loading.service

export default Vue
