module.exports = {
  // localIP请根据本地ip进行设置
  // localIP:"192.168.0.100",
  localIP: "0.0.0.0",
  // localIP: "localhost",
  // port设置的监听端口   端口注意不要和别的再使用的冲突
  port: 3000,
  // APIhost设置提供接口的域名及端口(下面是ip对应的电脑)
  // 253->王志伟
  // 252->冯伟杰
  //APIhost: "http://54.169.151.170:8001", // 线上测试后台
  APIhost: "http://pt.dafacloud-test.com"//站长彩种测试
  // APIhost: "http://system.dfcdn39.com:8001"
  //APIhost: "http://cms.ceykjxzcfx99.com:8001/", // 线上测试后台 http://system.dfcdn39.com:8001 http://54.169.151.170:8001
  // APIhost: "", // 本地测试后台
  // Origin: "http://192.168.3.252:8888"
  // Origin:"http://localhost:3001"
}
