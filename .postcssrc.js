// https://github.com/michael-ciniawsky/postcss-load-config

module.exports = {
  plugins: {
    "postcss-import": {},
    "postcss-url": {},
    "postcss-nesting": {}
    // 'postcss-cssnext': {
    //   browsers: ['last 2 versions', '> 5%'],
    // }
  }
}
