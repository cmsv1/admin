# admin

> 改在前端后台体验

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e
Anita2018
# run all tests
npm test
```
### 自定义element主题

####命令行主题工具
首先安装「主题生成工具」，可以全局安装或者安装在当前项目下，推荐安装在项目里，方便别人 clone 项目时能直接安装依赖并启动，这里以全局安装做演示。
```
npm i element-theme -g
```
在element-variables.scss文件中修改变量,例如修改主题色为红色。
```
$--color-primary: red;
```
编译主题,如果你想启用 `watch` 模式，实时编译主题，增加 -w 参数
```aidl
et -w
```
For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
